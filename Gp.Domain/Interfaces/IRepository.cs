﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Gp.Domain.Models;

namespace Gp.Domain.Interfaces
{
    public interface IRepository<TEntity> : IDisposable where TEntity : EntityModel
    {

        void Add(TEntity obj);

        TEntity GetById(long id);

        IEnumerable<TEntity> GetAll();

        void Update(TEntity obj);

        void Remove(long id);

        TEntity Get(Expression<Func<TEntity, bool>> where);

        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
                       
        int SaveChanges();
    }
}
