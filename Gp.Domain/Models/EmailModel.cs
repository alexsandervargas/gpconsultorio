﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
    public class EmailModel: EntityModel
    {
        public EmailModel()
        {
            
        }

        public virtual PessoaModel Pessoa { get; set; }
        public string Email { get; set; }
        public string Observacao { get; set; }

    }
}
