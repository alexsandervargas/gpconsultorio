﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
    public class EstadoModel : EntityModel
    {

        public EstadoModel()
        {

        }

        public string Descricao { get; set; }
        public string Sigla { get; set; }
        public virtual PaisModel Pais { get; set; }

    }
}
