﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
    public class ProfissionalModel : PessoaFisicaModel
    {
        public ProfissionalModel()
        {

        }

        public virtual ConselhoModel Conselho { get; set; }
        public string MatriculaConselho { get; set; }
    }
}
