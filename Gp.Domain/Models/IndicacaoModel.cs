﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
   public class IndicacaoModel:EntityModel
    {
        public IndicacaoModel()
        {

        }

        
        public virtual TipoIndicacaoModel Tipo { get; set; }
        public virtual PessoaFisicaModel PessoaIndicou { get; set; }
        public string NomeIndicacao { get; set; }

    }
}
