﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
    public class PessoaFisicaModel : PessoaModel
    {
        public PessoaFisicaModel()
        {
            Indicacoes = new List<IndicacaoModel>();
        }

        public DateTime Nascimento { get; set; }
        public string RG { get; set; }
        public string CPF { get; set; }
        public virtual SexoModel Sexo { get; set; }
        public virtual EstadoCivilModel EstadoCivil { get; set; }
        public virtual ProfissaoModel Profissao { get; set; }
        public ICollection<IndicacaoModel> Indicacoes { get; set; }

    }
}
