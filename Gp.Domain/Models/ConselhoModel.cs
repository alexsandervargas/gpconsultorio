﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
    public class ConselhoModel : EntityModel
    {
        public ConselhoModel()
        {
            Profissionais = new List<ProfissionalModel>();
        }

        public string Descricao { get; set; }
        public ICollection<ProfissionalModel> Profissionais { get; set; }

    }
}
