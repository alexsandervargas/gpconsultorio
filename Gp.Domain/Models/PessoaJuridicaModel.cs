﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
    public class PessoaJuridicaModel:PessoaModel
    {

        public PessoaJuridicaModel()
        {
            Planos = new List<PlanoModel>();
                        
        }

        public string CNPJ { get; set; }
        public string NomeFantasia { get; set;}
        public string InscricaoEstadual { get; set; }
        public ICollection<PlanoModel> Planos { get; set; }

      
        
    }
}
