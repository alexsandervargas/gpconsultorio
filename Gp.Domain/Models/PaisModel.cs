﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
    public class PaisModel : EntityModel
    {
        public PaisModel()
        {
            Estados = new List<EstadoModel>();
        }

        public string Descricao { get; set; }

        public ICollection<EstadoModel> Estados { get; set; }
    }
}
