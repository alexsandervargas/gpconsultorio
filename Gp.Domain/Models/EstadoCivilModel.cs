﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
    public class EstadoCivilModel:EntityModel
    {
        public EstadoCivilModel()
        {

        }

        public string Descricao { get; set; }
    }
}
