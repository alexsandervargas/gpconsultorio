﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
    public class TelefoneModel: EntityModel 
    {
        public TelefoneModel()
        {

        }

        public virtual PessoaModel Pessoa { get; set; }
        public int CodigoArea { get; set; }
        public string Telefone { get; set; }
        public string Observacao { get; set; }

    }
}
