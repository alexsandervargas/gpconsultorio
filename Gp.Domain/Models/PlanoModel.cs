﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
    public class PlanoModel:EntityModel
    {
        public PlanoModel()
        {
                
        }

        public virtual PessoaJuridicaModel PessoaJuridica { get; set; }
        public string Descricao { get; set; }
        public bool Cooparticipacao { get; set; }
        public bool Ativo { get; set; }
        public string Observacao { get; set; }
    }
}
