﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
    public class ProfissaoModel : EntityModel
    {
        public ProfissaoModel()
        {
            Profissionais = new List<PessoaFisicaModel>();
        }

        public string Descricao { get; set; }
        public ICollection<PessoaFisicaModel> Profissionais { get; set; }

    }
}
