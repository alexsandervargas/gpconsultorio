﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
    public abstract class PessoaModel:EntityModel
    {
        public PessoaModel()
        {
          
        }

        public string Nome { get; set; }

        public int CodigoAreaResidencial { get; set; }
        public string TelefoneResidencial { get; set; }

        public int CodigoAreaCelular { get; set; }
        public string TelefoneCelular { get; set; }

        public int CodigoAreaComercial { get; set; }
        public string TelefoneComercial { get; set; }
               
        public string Email { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public virtual CidadeModel Cidade { get; set; }
        public string Bairro { get; set; }
        public string CEP { get; set; }

        public string Observacao { get; set; }




    }
}
