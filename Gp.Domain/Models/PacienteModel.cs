﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Domain.Models
{
    public class PacienteModel : PessoaFisicaModel
    {
        public PacienteModel()
        {

        }
        public virtual IndicacaoModel Indicacao { get; set; }
        public virtual PlanoModel Plano { get; set; }
        public string Empresa { get; set; }
        public string Matricula { get; set; }
        public int Ficha { get; set; }

    }
}
