﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class CidadeDTO:EntityDTO
    {
        public CidadeDTO()
        {

        }

        public string Descricao { get; set; }
        public int EstadoId { get; set; }
    }
}
