﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class PessoaFisicaDTO : PessoaDTO
    {
        public PessoaFisicaDTO()
        {

        }

        public DateTime Nascimento { get; set; }
        public string RG { get; set; }
        public string CPF { get; set; }
        public int SexoId { get; set; }
        public int EstadoCivilId { get; set; }
        public int ProfissaoId { get; set; }
        public string Profissao { get; set; }
      
    }
}
