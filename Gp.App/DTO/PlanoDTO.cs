﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class PlanoDTO: EntityDTO
    {
        public PlanoDTO()
        {

        }

        public int PessoaJuridicaId { get; set; }
        public string Descricao { get; set; }
        public bool Cooparticipacao { get; set; }
        public bool Ativo { get; set; }
        public string Observacao { get; set; }
    }
}
