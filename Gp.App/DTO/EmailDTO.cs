﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class EmailDTO : EntityDTO
    {
        public EmailDTO()
        {

        }

        public int PessoaId { get; set; }
        public string Email { get; set; }
        public string Observacao { get; set; }
    }
}
