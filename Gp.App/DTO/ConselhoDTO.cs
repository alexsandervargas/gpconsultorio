﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class ConselhoDTO :EntityDTO
    {
        public ConselhoDTO()
        {
            Profissionais = new List<ProfissionalDTO>();
        }

        public string Descricao { get; set; }
        public ICollection<ProfissionalDTO> Profissionais { get; set; }
    }
}
