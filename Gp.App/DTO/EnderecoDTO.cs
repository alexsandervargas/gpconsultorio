﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class EnderecoDTO : EntityDTO
    {
        public EnderecoDTO()
        {

        }

        public int PessoaId { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public int CidadeId { get; set; }
        public string Bairro { get; set; }
        public string CEP { get; set; }
        public string Observacao { get; set; }

    }
}
