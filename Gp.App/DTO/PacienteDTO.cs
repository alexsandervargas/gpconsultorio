﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class PacienteDTO : PessoaFisicaDTO
    {
        public PacienteDTO()
        {
            
        }

        public int PlanoId { get; set; }
        public int IndicacaoId { get; set; }
        public string Matricula { get; set; }
        public string Empresa { get; set; }
        public int Ficha { get; set; }
        
    }
}
