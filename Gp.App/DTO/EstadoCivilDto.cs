﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class EstadoCivilDTO:EntityDTO
    {
        public EstadoCivilDTO()
        {

        }

        public string Descricao { get; set; }
    }
}
