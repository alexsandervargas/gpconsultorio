﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class ProfissaoDTO : EntityDTO
    {
        public ProfissaoDTO()
        {
            Profissionais = new List<PessoaFisicaDTO>();
        }

        public string Descricao { get; set; }
        public ICollection<PessoaFisicaDTO> Profissionais { get; set; }
    }
}
