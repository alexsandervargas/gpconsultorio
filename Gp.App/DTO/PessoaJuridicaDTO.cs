﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class PessoaJuridicaDTO : PessoaDTO
    {
        public PessoaJuridicaDTO()
        {
            Planos = new List<PlanoDTO>();
        }

        public string CNPJ { get; set; }
        public string NomeFantasia { get; set; }
        public string InscricaoEstadual { get; set; }
        public ICollection<PlanoDTO> Planos { get; set; }


    }
}
