﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class IndicacaoDTO : EntityDTO
    {
        public IndicacaoDTO()
        {

        }

        public virtual TipoIndicacaoDTO Tipo { get; set; }
        public int PessoaIndicouId { get; set; }
        public string NomeIndicacao { get; set; }

    }
}
