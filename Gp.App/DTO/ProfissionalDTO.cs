﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class ProfissionalDTO : PessoaFisicaDTO
    {
        public ProfissionalDTO()
        {
        }
        public int ConselhoId { get; set; }
        public string MatriculaConselho { get; set; }
    }
}
