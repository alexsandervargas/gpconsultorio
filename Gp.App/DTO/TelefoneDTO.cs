﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class TelefoneDTO : EntityDTO
    {
        public TelefoneDTO()
        {

        }

        public int PessoaId { get; set; }
        public int CodigoArea { get; set; }
        public string Telefone { get; set; }
        public string Observacao { get; set; }

    }
}
