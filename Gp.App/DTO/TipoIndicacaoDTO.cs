﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public enum TipoIndicacaoDTO
    {
        Nenhum = 0,
        Cliente = 1,
        Médico = 2,
        Funcionário = 3,
        Outros = 4
    }
}
