﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class SexoDTO:EntityDTO
    {
        public SexoDTO()
        {

        }

        public string Descricao { get; set; }
    }
}
