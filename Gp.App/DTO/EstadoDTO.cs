﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.App.DTO
{
    public class EstadoDTO :EntityDTO
    {
        public EstadoDTO()
        {
            
        }

        public string Descricao { get; set; }
        public string Sigla { get; set; }
        public int PaisId { get; set; }
    }
}
