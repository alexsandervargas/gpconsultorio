﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;

namespace Gp.App.Services
{
    public class CepService
    {

        public List<string> GetCEP(string CEP)
        {
            String[] substrings=null;
            List<string> endereco = new List<string>();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://viacep.com.br/ws/" + CEP + "/json/");
            request.AllowAutoRedirect = false;
            if (CEP.Length < 8)
            {
                MessageBox.Show("CEP Inválido!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }
            HttpWebResponse ChecaServidor = (HttpWebResponse)request.GetResponse();

            if (ChecaServidor.StatusCode != HttpStatusCode.OK)
            {
                MessageBox.Show("Servidor Indisponível!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null; // Sai da rotina
            }

            using (Stream webStream = ChecaServidor.GetResponseStream())
            {
                if (webStream != null)
                {
                    using (StreamReader responseReader = new StreamReader(webStream))
                    {
                        string response = responseReader.ReadToEnd();
                        response = Regex.Replace(response, "[{},]", string.Empty);
                        response = response.Replace("\"", "");

                        substrings = response.Split('\n');


                        int cont = 0;
                        foreach (var substring in substrings)
                        {
                            if (cont == 1)
                            {
                                string[] valor = substring.Split(":".ToCharArray());
                                if (valor[0] == "  erro")
                                {
                                    MessageBox.Show("CEP não encontrado","GP", MessageBoxButtons.OK, MessageBoxIcon.Information );
                                    return null;
                                }
                            }

                            //Logradouro
                            if (cont == 2)
                            {
                                string[] valor = substring.Split(":".ToCharArray());
                                endereco.Add(valor[1].TrimStart());                              
                            }

                            //Complemento
                            if (cont == 3)
                            {

                                string[] valor = substring.Split(":".ToCharArray());
                                endereco.Add(valor[1].TrimStart());
                            }

                            //Bairro
                            if (cont == 4)
                            {
                                string[] valor = substring.Split(":".ToCharArray());
                                endereco.Add(valor[1].TrimStart());
                            }

                            //Localidade (Cidade)
                            if (cont == 5)
                            {
                                string[] valor = substring.Split(":".ToCharArray());
                                endereco.Add(valor[1]);                           

                            }

                            //Estado (UF)
                            if (cont == 6)
                            {
                                string[] valor = substring.Split(":".ToCharArray());
                                endereco.Add(valor[1]);

                                var cidade = new CidadeService().GetByNome(endereco[3],valor[1]);
                                if (cidade != null)
                                {
                                    endereco[3]= cidade.Id.ToString();
                                }
                                
                            }

                            cont++;
                        }
                    }
                }
            }

            return endereco;
        }

    }
}
