﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gp.App.DTO;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using Gp.Infrastructure.Repository;

namespace Gp.App.Services
{
    public class UsuarioService
    {

        private readonly UsuarioRepository _repository;
        private readonly GpContext _context;

        public UsuarioService()
        {
            this._context = new GpContext();
            this._repository = new UsuarioRepository(_context);
        }

        public IList<UsuarioDTO> GetAll()
        {
            try
            {
                var usuarios = _repository.GetAll();
                var ListaUsuarios = new List<UsuarioDTO>();

                foreach (var usuario in usuarios)
                {
                    var usuarioDTO = new UsuarioDTO
                    {
                        Ativo = usuario.Ativo,
                        ConfirmaSenha = usuario.Senha,
                        Email = usuario.Email,
                        Id = usuario.Id,
                        Login = usuario.Login,
                        Nome = usuario.Nome,
                        Senha = usuario.Senha,

                    };


                    ListaUsuarios.Add(usuarioDTO);
                }

                return ListaUsuarios;

            }
            catch (Exception)
            {
               return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public UsuarioDTO GetById(int UsuarioId)
        {
            try
            {
                UsuarioDTO usuario;
                var usuarioModel = _repository.GetById(UsuarioId);

                if (usuarioModel != null)
                {
                    usuario = new UsuarioDTO
                    {
                        Ativo = usuarioModel.Ativo,
                        ConfirmaSenha = usuarioModel.Senha,
                        Email = usuarioModel.Email,
                        Id = UsuarioId,
                        Login = usuarioModel.Login,
                        Nome = usuarioModel.Nome,
                        Senha = usuarioModel.Senha,

                    };


                    return usuario;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public UsuarioDTO LoginUser(string Login, string Senha)
        {
            try
            {
                UsuarioDTO usuario;

                var usuarioModel = _repository.Get(u => u.Login == Login && u.Senha == Senha);

                if (usuarioModel != null)
                {
                    usuario = GetById(usuarioModel.Id);

                    return usuario;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
               return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }


        public void Save(UsuarioDTO usuario)
        {
            try
            {
                UsuarioModel usuarioModel = null;

                if (usuario.Id == 0)
                {
                    usuarioModel = new UsuarioModel
                    {
                        Ativo = usuario.Ativo,
                        Email = usuario.Email,
                      //  Id = usuario.Id,
                        Login = usuario.Login,
                        Nome = usuario.Nome,
                        Senha = usuario.Senha,
                    };

                    _repository.Add(usuarioModel);
                }
                else
                {
                    usuarioModel = new UsuarioRepository(_context).GetById(usuario.Id);

                    usuarioModel.Ativo = usuario.Ativo;
                    usuarioModel.Email = usuario.Email;
                  //  usuarioModel.Id = usuario.Id;
                    usuarioModel.Login = usuario.Login;
                    usuarioModel.Nome = usuario.Nome;
                    usuarioModel.Senha = usuario.Senha;

                    _repository.Update(usuarioModel);
                }

                _repository.SaveChanges();

                usuario.Id = usuarioModel.Id;


            }
            catch (Exception)
            {
                
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Delete(int UsuarioId)
        {
            _repository.Remove(UsuarioId);
            _repository.SaveChanges();
            _repository.Dispose();
        }

        #region Search

        public IList<UsuarioDTO> Search(string campo, object texto)
        {

            switch (campo)
            {
                case "Nome":
                    return GetAll().Where(u => u.Nome.ToLower().Contains(texto.ToString().ToLower())).ToList();
                case "Login":
                    return GetAll().Where(u => u.Login.ToLower().Contains(texto.ToString().ToLower())).ToList();
                case "Email":
                    return GetAll().Where(u => u.Email.ToLower().Contains(texto.ToString().ToLower())).ToList();
                default:

                    if (texto.ToString() != string.Empty)
                    {
                        var usuario = GetById(Convert.ToInt32(texto.ToString()));
                        var lista = new List<UsuarioDTO>
                    {
                        usuario
                    };
                        return lista;
                    }
                    else
                    {
                        return GetAll();
                    }

            }

        }

        #endregion



    }


}
