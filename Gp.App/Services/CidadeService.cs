﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Gp.App.DTO;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using Gp.Infrastructure.Repository;

namespace Gp.App.Services
{
    public class CidadeService
    {
        private readonly CidadeRepository _repository;
        private readonly GpContext _context;

        public CidadeService()
        {
            this._context = new GpContext();
            this._repository = new CidadeRepository(_context);
        }

        public IList<CidadeDTO> GetAll()
        {
            try
            {
                var cidades = _repository.GetAllInformation();
                var ListaCidades = new List<CidadeDTO>();

                foreach (var cidade in cidades)
                {
                    var CidadeDTO = new CidadeDTO
                    {
                        Descricao = cidade.Descricao,
                        Id = cidade.Id,
                        EstadoId= cidade.Estado.Id,
                    };


                    ListaCidades.Add(CidadeDTO);
                }

                return ListaCidades;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public CidadeDTO GetById(int cidadeId)
        {
            try
            {
                CidadeDTO cidade;
                var cidadeModel = _repository.GetWithEstado(cidadeId);

                if (cidadeModel != null)
                {
                    cidade = new CidadeDTO
                    {
                        Descricao = cidadeModel.Descricao,
                        Id = cidadeId,
                        EstadoId = cidadeModel.Estado.Id

                    };


                    return cidade;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public CidadeDTO GetByNome(string nomecidade, string uf)
        {
            try
            {
                CidadeDTO cidade;
                var cidadeModel = _repository.GetWithEstadoByName(nomecidade,uf);

                if (cidadeModel != null)
                {
                    cidade = new CidadeDTO
                    {
                        Descricao = cidadeModel.Descricao,
                        Id = cidadeModel.Id,
                        EstadoId = cidadeModel.Estado.Id

                    };


                    return cidade;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }


        public void Save(CidadeDTO cidade)
        {
            try
            {
                CidadeModel cidadeModel = null;

                if (cidade.Id == 0)
                {
                    cidadeModel = new CidadeModel
                    {
                        Descricao = cidade.Descricao,
                        Id = cidade.Id,
                        Estado = new EstadoRepository(_context).GetById(cidade.EstadoId)

                    };

                    _repository.Add(cidadeModel);
                }
                else
                {
                    cidadeModel = new CidadeRepository(_context).GetById(cidade.Id);
                    cidadeModel.Descricao = cidade.Descricao;
                    cidadeModel.Estado = new EstadoRepository(_context).GetById(cidade.EstadoId);

                    _repository.Update(cidadeModel);
                }

                _repository.SaveChanges();

                cidade.Id = cidadeModel.Id;


            }
            catch (Exception)
            {

            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Delete(int cidadeId)
        {
            _repository.Remove(cidadeId);
            _repository.SaveChanges();
            _repository.Dispose();
        }

        #region Search

        public IList<CidadeDTO> Search(string campo, object texto)
        {

            switch (campo)
            {
                case "Descrição":
                    return GetAll().Where(u => u.Descricao.ToLower().Contains(texto.ToString().ToLower())).ToList();
                default:

                    if (texto.ToString() != string.Empty)
                    {
                        var cidade = GetById(Convert.ToInt32(texto.ToString()));
                        var lista = new List<CidadeDTO>
                    {
                        cidade
                    };
                        return lista;
                    }
                    else
                    {
                        return GetAll();
                    }

            }

        }

        #endregion

    }
}
