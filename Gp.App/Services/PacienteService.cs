﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gp.App.DTO;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using Gp.Infrastructure.Repository;

namespace Gp.App.Services
{
    public class PacienteService
    {
        private readonly PacienteRepository _repository;
        private readonly GpContext _context;

        public PacienteService()
        {
            this._context = new GpContext();
            this._repository = new PacienteRepository(_context);
        }

        public IList<PacienteDTO> GetAll()
        {
            try
            {
                var pacientess = _repository.GetAllInformation();
                var Listapacientess = new List<PacienteDTO>();

                foreach (var paciente in pacientess)
                {
                    var pacienteDTO = new PacienteDTO
                    {
                        Id = paciente.Id,
                        CPF = paciente.CPF,
                        Nascimento = paciente.Nascimento,
                        Nome = paciente.Nome,
                        Observacao = paciente.Observacao,
                        RG = paciente.RG,
                        Matricula = paciente.Matricula,
                        Logradouro = paciente.Logradouro,
                        Bairro = paciente.Bairro,
                        CEP = paciente.CEP,
                        Complemento = paciente.Complemento,
                        Numero = paciente.Numero,
                        CodigoAreaCelular = paciente.CodigoAreaCelular,
                        CodigoAreaComercial = paciente.CodigoAreaComercial,
                        CodigoAreaResidencial = paciente.CodigoAreaResidencial,
                        Email = paciente.Email,
                        TelefoneCelular = paciente.TelefoneCelular,
                        TelefoneComercial = paciente.TelefoneComercial,
                        TelefoneResidencial = paciente.TelefoneResidencial,
                        Ficha = paciente.Ficha,
                        Empresa= paciente.Empresa,
                        DataCadastro = paciente.DataCadastro,
                        DataAtualizacao = paciente.DataAtualizacao,

                    };

                    pacienteDTO.CidadeId = paciente.Cidade != null ? paciente.Cidade.Id : 0;
                    pacienteDTO.CidadeNome = paciente.Cidade != null ? paciente.Cidade.Descricao : string.Empty;
                    pacienteDTO.ProfissaoId = paciente.Profissao != null ? paciente.Profissao.Id : 0;
                    pacienteDTO.EstadoCivilId = paciente.EstadoCivil != null ? paciente.EstadoCivil.Id : 0;
                    pacienteDTO.SexoId = paciente.Sexo != null ? paciente.Sexo.Id : 0;
                    pacienteDTO.PlanoId = paciente.Plano != null ? paciente.Plano.Id : 0;
                    pacienteDTO.IndicacaoId = paciente.Indicacao != null ? paciente.Indicacao.Id : 0;

                    Listapacientess.Add(pacienteDTO);
                }

                return Listapacientess;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public IList<PacienteDTO> GetAniversariantes(DateTime Inicial, DateTime Final)
        {

            var pacientes = _repository.Find(x => x.Nascimento.Day >= Inicial.Day
                                && x.Nascimento.Day <= Final.Day
                                && x.Nascimento.Month >= Inicial.Month
                                && x.Nascimento.Month <= Final.Month).OrderBy(x => x.Nascimento.Month).ThenBy(x => x.Nascimento.Day).ToList();

            var Lista = new List<PacienteDTO>();

            foreach (var paciente in pacientes)
            {
                var pacientedto = new PacienteDTO
                {
                    Id = paciente.Id,
                    Ficha = paciente.Ficha,
                    Nascimento = paciente.Nascimento,
                    Nome = paciente.Nome,
                    CPF = paciente.CPF,
                    TelefoneCelular = paciente.CodigoAreaCelular + " " + paciente.TelefoneCelular,
                    TelefoneResidencial = paciente.CodigoAreaResidencial + " " + paciente.TelefoneResidencial,
                    TelefoneComercial = paciente.CodigoAreaComercial + " " + paciente.TelefoneComercial,
                    Email = paciente.Email
                };

                Lista.Add(pacientedto);
            }

            return Lista;


        }

        public DataTable ReportAniversariantes(DateTime Inicial, DateTime Final)
        {
            var tabela = _repository.ReportAniversariantes(Inicial, Final);

            return tabela;
        }

        public PacienteDTO GetById(int pacienteId)
        {
            try
            {
                PacienteDTO paciente;
                var pacienteModel = _repository.GetByIdAllInformation(pacienteId);

                if (pacienteModel != null)
                {
                    paciente = new PacienteDTO
                    {
                        Id = pacienteModel.Id,
                        CPF = pacienteModel.CPF,
                        Nascimento = pacienteModel.Nascimento,
                        Nome = pacienteModel.Nome,
                        Observacao = pacienteModel.Observacao,
                        RG = pacienteModel.RG,
                        Matricula = pacienteModel.Matricula,
                        Logradouro = pacienteModel.Logradouro,
                        Bairro = pacienteModel.Bairro,
                        CEP = pacienteModel.CEP,
                        Complemento = pacienteModel.Complemento,
                        Numero = pacienteModel.Numero,
                        CodigoAreaCelular = pacienteModel.CodigoAreaCelular,
                        CodigoAreaComercial = pacienteModel.CodigoAreaComercial,
                        CodigoAreaResidencial = pacienteModel.CodigoAreaResidencial,
                        Email = pacienteModel.Email,
                        TelefoneCelular = pacienteModel.TelefoneCelular,
                        TelefoneComercial = pacienteModel.TelefoneComercial,
                        TelefoneResidencial = pacienteModel.TelefoneResidencial,
                        Ficha = pacienteModel.Ficha,
                        Empresa= pacienteModel.Empresa,
                        DataCadastro = pacienteModel.DataCadastro,
                        DataAtualizacao = pacienteModel.DataAtualizacao,

                    };

                    paciente.CidadeId = pacienteModel.Cidade != null ? pacienteModel.Cidade.Id : 0;
                    paciente.ProfissaoId = pacienteModel.Profissao != null ? pacienteModel.Profissao.Id : 0;
                    paciente.EstadoCivilId = pacienteModel.EstadoCivil != null ? pacienteModel.EstadoCivil.Id : 0;
                    paciente.SexoId = pacienteModel.Sexo != null ? pacienteModel.Sexo.Id : 0;
                    paciente.PlanoId = pacienteModel.Plano != null ? pacienteModel.Plano.Id : 0;
                    paciente.IndicacaoId = pacienteModel.Indicacao != null ? pacienteModel.Indicacao.Id : 0;


                    return paciente;
                }
                else
                {


                    return null;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public PacienteDTO GetByIdBusca(int pacienteId)
        {
            try
            {
                PacienteDTO paciente;
                var pacienteModel = _repository.GetByIdBusca(pacienteId);

                if (pacienteModel != null)
                {
                    paciente = new PacienteDTO
                    {
                        Id = pacienteModel.Id,
                        CPF = pacienteModel.CPF,
                        Nome = pacienteModel.Nome,
                        Bairro = pacienteModel.Bairro,
                        Ficha = pacienteModel.Ficha,
                        //Indicacoes =  new IndicacoesService().GetAllIndicacoesPessoa(pacienteId),
                    };

                    paciente.CidadeId = pacienteModel.Cidade != null ? pacienteModel.Cidade.Id : 0;
                    paciente.CidadeNome = pacienteModel.Cidade != null ? pacienteModel.Cidade.Descricao : string.Empty;
                    paciente.ProfissaoId = pacienteModel.Profissao != null ? pacienteModel.Profissao.Id : 0;
                    paciente.EstadoCivilId = pacienteModel.EstadoCivil != null ? pacienteModel.EstadoCivil.Id : 0;
                    paciente.SexoId = pacienteModel.Sexo != null ? pacienteModel.Sexo.Id : 0;
                    paciente.PlanoId = pacienteModel.Plano != null ? pacienteModel.Plano.Id : 0;
                    paciente.IndicacaoId = pacienteModel.Indicacao != null ? pacienteModel.Indicacao.Id : 0;



                    return paciente;
                }
                else
                {


                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }


        }

        public void Save(PacienteDTO paciente)
        {
            try
            {
                PacienteModel pacienteModel = null;

                if (paciente.Id == 0)
                {
                    pacienteModel = new PacienteModel
                    {
                        Id = paciente.Id,
                        CPF = paciente.CPF,
                        Nascimento = paciente.Nascimento,
                        Nome = paciente.Nome,
                        Observacao = paciente.Observacao,
                        RG = paciente.RG,
                        Matricula = paciente.Matricula,
                        Logradouro = paciente.Logradouro,
                        Bairro = paciente.Bairro,
                        CEP = paciente.CEP,
                        Complemento = paciente.Complemento,
                        Numero = paciente.Numero,
                        CodigoAreaCelular = paciente.CodigoAreaCelular,
                        CodigoAreaComercial = paciente.CodigoAreaComercial,
                        CodigoAreaResidencial = paciente.CodigoAreaResidencial,
                        Email = paciente.Email,
                        TelefoneCelular = paciente.TelefoneCelular,
                        TelefoneComercial = paciente.TelefoneComercial,
                        TelefoneResidencial = paciente.TelefoneResidencial,
                        Ficha = paciente.Ficha,
                        Empresa=paciente.Empresa
                                                
                    };

                    pacienteModel.Profissao = paciente.ProfissaoId != 0 ? new ProfissaoRepository(_context).GetById(paciente.ProfissaoId) : null;
                    pacienteModel.EstadoCivil = paciente.EstadoCivilId != 0 ? new EstadoCivilRepository(_context).GetById(paciente.EstadoCivilId) : null;
                    pacienteModel.Sexo = paciente.SexoId != 0 ? new SexoRepository(_context).GetById(paciente.SexoId) : null;
                    pacienteModel.Plano = paciente.PlanoId != 0 ? new PlanoRepository(_context).GetById(paciente.PlanoId) : null;
                    pacienteModel.Cidade = paciente.CidadeId != 0 ? new CidadeRepository(_context).GetById(paciente.CidadeId) : null;


                    _repository.Add(pacienteModel);
                }
                else
                {
                    pacienteModel = new PacienteRepository(_context).GetByIdAllInformation(paciente.Id);

                    pacienteModel.CPF = paciente.CPF;
                    pacienteModel.Nascimento = paciente.Nascimento;
                    pacienteModel.Nome = paciente.Nome;
                    pacienteModel.Observacao = paciente.Observacao;
                    pacienteModel.RG = paciente.RG;
                    pacienteModel.Matricula = paciente.Matricula;
                    pacienteModel.Logradouro = paciente.Logradouro;
                    pacienteModel.Bairro = paciente.Bairro;
                    pacienteModel.CEP = paciente.CEP;
                    pacienteModel.Complemento = paciente.Complemento;
                    pacienteModel.Numero = paciente.Numero;
                    pacienteModel.CodigoAreaCelular = paciente.CodigoAreaCelular;
                    pacienteModel.CodigoAreaComercial = paciente.CodigoAreaComercial;
                    pacienteModel.CodigoAreaResidencial = paciente.CodigoAreaResidencial;
                    pacienteModel.Email = paciente.Email;
                    pacienteModel.TelefoneCelular = paciente.TelefoneCelular;
                    pacienteModel.TelefoneComercial = paciente.TelefoneComercial;
                    pacienteModel.TelefoneResidencial = paciente.TelefoneResidencial;
                    pacienteModel.Ficha = paciente.Ficha;
                    pacienteModel.Empresa = paciente.Empresa;

                    pacienteModel.Profissao = paciente.ProfissaoId != 0 ? new ProfissaoRepository(_context).GetById(paciente.ProfissaoId) : null;
                    pacienteModel.EstadoCivil = paciente.EstadoCivilId != 0 ? new EstadoCivilRepository(_context).GetById(paciente.EstadoCivilId) : null;
                    pacienteModel.Sexo = paciente.SexoId != 0 ? new SexoRepository(_context).GetById(paciente.SexoId) : null;
                    pacienteModel.Plano = paciente.PlanoId != 0 ? new PlanoRepository(_context).GetById(paciente.PlanoId) : null;
                    pacienteModel.Cidade = paciente.CidadeId != 0 ? new CidadeRepository(_context).GetById(paciente.CidadeId) : null;
                    pacienteModel.Indicacao = paciente.IndicacaoId != 0 ? new IndicacaoRepository(_context).GetById(paciente.IndicacaoId) : null;



                    _repository.Update(pacienteModel);
                }


                _repository.SaveChanges();

                paciente.Id = pacienteModel.Id;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Delete(int pacienteId)
        {
            _repository.Remove(pacienteId);
            _repository.SaveChanges();
            _repository.Dispose();
        }

        public DataTable Ficha(int id)
        {

            var tabela = _repository.ReportFicha(id);


            return tabela;
        }

        #region Search

        public IList<PacienteDTO> Search(string campo, object texto, string metodo)
        {
            var Busca = texto.ToString().Split(' ');
            IList<PacienteModel> lista = null;
            switch (metodo)
            {
                case "Inicia com":
                    switch (campo)
                    {
                        case "Nome":
                            return SetBusca(_repository.Find(u => u.Nome.ToLower().StartsWith(texto.ToString().ToLower())).ToList());
                        case "CPF":
                            return SetBusca(_repository.Find(u => u.CPF.ToLower().StartsWith(texto.ToString().ToLower())).ToList());
                        case "RG":
                            return SetBusca(_repository.Find(u => u.RG.ToLower().StartsWith(texto.ToString().ToLower())).ToList());
                        case "Celular":
                            return SetBusca(_repository.Find(u => u.TelefoneCelular.ToLower().StartsWith(texto.ToString().ToLower())).ToList());
                        case "Ficha":
                            return SetBusca(_repository.Find(u => u.Ficha.ToString().ToLower().StartsWith(texto.ToString().ToLower())).ToList());
                        default:

                            if (texto.ToString() != string.Empty)
                            {
                                var paciente = GetById(Convert.ToInt32(texto.ToString()));
                                var listagem = new List<PacienteDTO>
                                {
                                    paciente
                                };
                                return listagem;
                            }
                            else
                            {
                                return GetAll();
                            }

                    }

                case "Termina com":
                    switch (campo)
                    {
                        case "Nome":
                            return SetBusca(_repository.Find(u => u.Nome.ToLower().EndsWith(texto.ToString().ToLower())).ToList());
                        case "CPF":
                             return SetBusca(_repository.Find(u => u.CPF.ToLower().EndsWith(texto.ToString().ToLower())).ToList());                           
                        case "RG":
                            return SetBusca(_repository.Find(u => u.RG.ToLower().EndsWith(texto.ToString().ToLower())).ToList());                           
                        case "Celular":
                             return SetBusca(_repository.Find(u => u.TelefoneCelular.ToLower().EndsWith(texto.ToString().ToLower())).ToList());                            
                        case "Ficha":
                            return SetBusca(_repository.Find(u => u.Ficha.ToString().ToLower().EndsWith(texto.ToString().ToLower())).ToList());                           
                        default:

                            if (texto.ToString() != string.Empty)
                            {
                                var paciente = GetById(Convert.ToInt32(texto.ToString()));
                                var listagem = new List<PacienteDTO>
                                {
                                    paciente
                                };
                                return listagem;
                            }
                            else
                            {
                                return GetAll();
                            }

                    }

                default:
                    switch (campo)
                    {
                        case "Nome":
                             lista = (from p in _repository.GetAll()
                                         where
                                         Busca.Any(n => p.Nome.ToLower().Contains(n.ToLower()))
                                         select p).ToList();

                            return SetBusca(lista);
                        case "CPF":
                           
                             lista = (from p in _repository.GetAll()
                                         where
                                         Busca.Any(n => p.CPF.ToLower().Contains(n.ToLower()))
                                         select p).ToList();

                            return SetBusca(lista);
                        case "RG":
                            
                            lista = (from p in _repository.GetAll()
                                     where
                                     Busca.Any(n => p.RG.ToLower().Contains(n.ToLower()))
                                     select p).ToList();

                            return SetBusca(lista);
                        case "Celular":
                            lista = (from p in _repository.GetAll()
                                     where
                                     Busca.Any(n => p.TelefoneCelular.ToLower().Contains(n.ToLower()))
                                     select p).ToList();

                            return SetBusca(lista);
                        case "Ficha":
                           
                            lista = (from p in _repository.GetAll()
                                     where
                                     Busca.Any(n => p.Ficha.ToString().ToLower().Contains(n.ToLower()))
                                     select p).ToList();

                            return SetBusca(lista);
                        default:

                            if (texto.ToString() != string.Empty)
                            {
                                var paciente = GetById(Convert.ToInt32(texto.ToString()));
                                var listagem = new List<PacienteDTO>
                                {
                                    paciente
                                };
                                return listagem;
                            }
                            else
                            {
                                return GetAll();
                            }

                    }
            }



        }

        public IList<PacienteDTO> GetIndicacoes(int PessoaIndicouId)
        {
            var Lista = new List<PacienteDTO>();

            var pacientes = _repository.GetIndicacoes(PessoaIndicouId);

            foreach (var paciente in pacientes)
            {
                var pacientedto = new PacienteDTO
                {
                    Id = paciente.Id,
                    Ficha = paciente.Ficha,
                    Nome = paciente.Nome,
                    CPF = paciente.CPF
                };

                Lista.Add(pacientedto);
            }

            return Lista;
        }

        public IList<PacienteDTO> SetBusca(IList<PacienteModel> pacientes)
        {
            try
            {
                var Listapacientes = new List<PacienteDTO>();

                foreach (var paciente in pacientes)
                {

                    Listapacientes.Add(GetByIdBusca(paciente.Id));
                }

                return Listapacientes;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        #endregion



    }
}
