﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Gp.App.DTO;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using Gp.Infrastructure.Repository;

namespace Gp.App.Services
{
    public class PessoaJuridicaService
    {

        private readonly PessoaJuridicaRepository _repository;
        private readonly GpContext _context;

        public PessoaJuridicaService()
        {
            this._context = new GpContext();
            this._repository = new PessoaJuridicaRepository(_context);
        }

        public IList<PessoaJuridicaDTO> GetAll()
        {
            try
            {
                var pessoasjuridicas = _repository.GetAllInformation();
                var Listapessoasjuridicas = new List<PessoaJuridicaDTO>();

                foreach (var pessoajuridica in pessoasjuridicas)
                {
                    var pessoajuridicaDTO = new PessoaJuridicaDTO
                    {
                        Id = pessoajuridica.Id,
                        CNPJ = pessoajuridica.CNPJ,
                        InscricaoEstadual = pessoajuridica.InscricaoEstadual,
                        Nome = pessoajuridica.Nome,
                        NomeFantasia = pessoajuridica.NomeFantasia,

                        Logradouro = pessoajuridica.Logradouro,
                        Observacao = pessoajuridica.Observacao,

                        Bairro = pessoajuridica.Bairro,
                        CEP = pessoajuridica.CEP,
                        CidadeId = pessoajuridica.Cidade != null ? pessoajuridica.Cidade.Id : 0,
                        Complemento = pessoajuridica.Complemento,
                        Numero = pessoajuridica.Numero,

                        CodigoAreaCelular = pessoajuridica.CodigoAreaCelular,
                        CodigoAreaComercial = pessoajuridica.CodigoAreaComercial,
                        CodigoAreaResidencial = pessoajuridica.CodigoAreaResidencial,
                        Email = pessoajuridica.Email,
                        TelefoneCelular = pessoajuridica.TelefoneCelular,
                        TelefoneComercial = pessoajuridica.TelefoneComercial,
                        TelefoneResidencial = pessoajuridica.TelefoneResidencial,
                        Planos = new PlanoService().GetAllplanosPessoa(pessoajuridica.Id),
                    };


                    Listapessoasjuridicas.Add(pessoajuridicaDTO);
                }

                return Listapessoasjuridicas;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public PessoaJuridicaDTO GetById(int pessoajuridicaId)
        {
            try
            {
                PessoaJuridicaDTO pessoajuridica;
                var pessoajuridicaModel = _repository.GetByIdAllInformation(pessoajuridicaId);

                if (pessoajuridicaModel != null)
                {
                    pessoajuridica = new PessoaJuridicaDTO
                    {
                        Id = pessoajuridicaId,
                        CNPJ = pessoajuridicaModel.CNPJ,
                        InscricaoEstadual = pessoajuridicaModel.InscricaoEstadual,
                        Nome = pessoajuridicaModel.Nome,
                        NomeFantasia = pessoajuridicaModel.NomeFantasia,

                        Logradouro = pessoajuridicaModel.Logradouro,
                        Observacao = pessoajuridicaModel.Observacao,

                        Bairro = pessoajuridicaModel.Bairro,
                        CEP = pessoajuridicaModel.CEP,
                        CidadeId = pessoajuridicaModel.Cidade != null ? pessoajuridicaModel.Cidade.Id : 0,
                        Complemento = pessoajuridicaModel.Complemento,
                        Numero = pessoajuridicaModel.Numero,

                        CodigoAreaCelular = pessoajuridicaModel.CodigoAreaCelular,
                        CodigoAreaComercial = pessoajuridicaModel.CodigoAreaComercial,
                        CodigoAreaResidencial = pessoajuridicaModel.CodigoAreaResidencial,
                        Email = pessoajuridicaModel.Email,
                        TelefoneCelular = pessoajuridicaModel.TelefoneCelular,
                        TelefoneComercial = pessoajuridicaModel.TelefoneComercial,
                        TelefoneResidencial = pessoajuridicaModel.TelefoneResidencial,

                        Planos = new PlanoService().GetAllplanosPessoa(pessoajuridicaId),

                    };


                    return pessoajuridica;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {

                
                return null;

            }
            finally
            {
                _repository.Dispose();
            }

        }




        public void Save(PessoaJuridicaDTO pessoajuridica)
        {
            try
            {
                PessoaJuridicaModel pessoajuridicaModel = null;

                if (pessoajuridica.Id == 0)
                {
                    pessoajuridicaModel = new PessoaJuridicaModel
                    {
                        Id = pessoajuridica.Id,
                        CNPJ = pessoajuridica.CNPJ,
                        InscricaoEstadual = pessoajuridica.InscricaoEstadual,
                        Nome = pessoajuridica.Nome,
                        NomeFantasia = pessoajuridica.NomeFantasia,
                        Logradouro = pessoajuridica.Logradouro,
                        Observacao = pessoajuridica.Observacao,

                        Bairro = pessoajuridica.Bairro,
                        CEP = pessoajuridica.CEP,
                        Cidade = pessoajuridica.CidadeId != 0 ?  new CidadeRepository(_context).GetById(pessoajuridica.CidadeId): null,
                        Complemento = pessoajuridica.Complemento,
                        Numero = pessoajuridica.Numero,

                        CodigoAreaCelular = pessoajuridica.CodigoAreaCelular,
                        CodigoAreaComercial = pessoajuridica.CodigoAreaComercial,
                        CodigoAreaResidencial = pessoajuridica.CodigoAreaResidencial,
                        Email = pessoajuridica.Email,
                        TelefoneCelular = pessoajuridica.TelefoneCelular,
                        TelefoneComercial = pessoajuridica.TelefoneComercial,
                        TelefoneResidencial = pessoajuridica.TelefoneResidencial,


                    };

                    _repository.Add(pessoajuridicaModel);
                }
                else
                {
                    pessoajuridicaModel = new PessoaJuridicaRepository(_context).GetByIdAllInformation(pessoajuridica.Id);
                    pessoajuridicaModel.CNPJ = pessoajuridica.CNPJ;
                    pessoajuridicaModel.InscricaoEstadual = pessoajuridica.InscricaoEstadual;
                    pessoajuridicaModel.Nome = pessoajuridica.Nome;
                    pessoajuridicaModel.NomeFantasia = pessoajuridica.NomeFantasia;

                    pessoajuridicaModel.Logradouro = pessoajuridica.Logradouro;
                    pessoajuridicaModel.Observacao = pessoajuridica.Observacao;

                    pessoajuridicaModel.Bairro = pessoajuridica.Bairro;
                    pessoajuridicaModel.CEP = pessoajuridica.CEP;
                    pessoajuridicaModel.Cidade = pessoajuridica.CidadeId != 0 ? new CidadeRepository(_context).GetById(pessoajuridica.CidadeId) : null;
                    pessoajuridicaModel.Complemento = pessoajuridica.Complemento;
                    pessoajuridicaModel.Numero = pessoajuridica.Numero;

                    pessoajuridicaModel.CodigoAreaCelular = pessoajuridica.CodigoAreaCelular;
                    pessoajuridicaModel.CodigoAreaComercial = pessoajuridica.CodigoAreaComercial;
                    pessoajuridicaModel.CodigoAreaResidencial = pessoajuridica.CodigoAreaResidencial;
                    pessoajuridicaModel.Email = pessoajuridica.Email;
                    pessoajuridicaModel.TelefoneCelular = pessoajuridica.TelefoneCelular;
                    pessoajuridicaModel.TelefoneComercial = pessoajuridica.TelefoneComercial;
                    pessoajuridicaModel.TelefoneResidencial = pessoajuridica.TelefoneResidencial;

                    _repository.Update(pessoajuridicaModel);
                }

                _repository.SaveChanges();

                pessoajuridica.Id = pessoajuridicaModel.Id;


            }
            catch (Exception)
            {

            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Delete(int pessoajuridicaId)
        {
            _repository.Remove(pessoajuridicaId);
            _repository.SaveChanges();
            _repository.Dispose();
        }

        #region Search

        public IList<PessoaJuridicaDTO> Search(string campo, object texto)
        {

            switch (campo)
            {
                case "Nome":
                    return GetAll().Where(u => u.Nome.ToLower().Contains(texto.ToString().ToLower())).ToList();
                default:

                    if (texto.ToString() != string.Empty)
                    {
                        var pessoajuridica = GetById(Convert.ToInt32(texto.ToString()));
                        var lista = new List<PessoaJuridicaDTO>
                    {
                        pessoajuridica
                    };
                        return lista;
                    }
                    else
                    {
                        return GetAll();
                    }

            }

        }

        #endregion

    }
}
