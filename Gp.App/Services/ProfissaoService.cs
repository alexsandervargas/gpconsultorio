﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Gp.App.DTO;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using Gp.Infrastructure.Repository;

namespace Gp.App.Services
{
    public class ProfissaoService
    {
        private readonly ProfissaoRepository _repository;
        private readonly GpContext _context;

        public ProfissaoService()
        {
            this._context = new GpContext();
            this._repository = new ProfissaoRepository(_context);
        }

        public IList<ProfissaoDTO> GetAll()
        {
            try
            {
                var profissoes = _repository.GetAll();
                var Listaprofissoes = new List<ProfissaoDTO>();

                foreach (var profissao in profissoes)
                {
                    var profissaoDTO = new ProfissaoDTO
                    {
                        Id = profissao.Id,
                        Descricao = profissao.Descricao,
                    };


                    Listaprofissoes.Add(profissaoDTO);
                }

                return Listaprofissoes;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public ProfissaoDTO GetById(int profissaoId)
        {
            try
            {
                ProfissaoDTO profissao;
                var profissaoModel = _repository.GetById(profissaoId);

                if (profissaoModel != null)
                {
                    profissao = new ProfissaoDTO
                    {
                        Id = profissaoModel.Id,
                        Descricao = profissaoModel.Descricao,

                    };


                    return profissao;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Save(ProfissaoDTO profissao)
        {
            try
            {
                ProfissaoModel profissaoModel = null;

                if (profissao.Id == 0)
                {
                    profissaoModel = new ProfissaoModel
                    {
                        Descricao = profissao.Descricao,
                    };

                    _repository.Add(profissaoModel);
                }
                else
                {
                    profissaoModel = new ProfissaoRepository(_context).GetById(profissao.Id);
                    profissaoModel.Descricao = profissao.Descricao;

                    _repository.Update(profissaoModel);
                }

                _repository.SaveChanges();

                profissao.Id = profissaoModel.Id;


            }
            catch (Exception)
            {

            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Delete(int profissaoId)
        {
            _repository.Remove(profissaoId);
            _repository.SaveChanges();
            _repository.Dispose();
        }

        #region Search

        public IList<ProfissaoDTO> Search(string campo, object texto)
        {

            switch (campo)
            {
                case "Descrição":
                    return GetAll().Where(u => u.Descricao.ToLower().Contains(texto.ToString().ToLower())).ToList();
                default:

                    if (texto.ToString() != string.Empty)
                    {
                        var profissao = GetById(Convert.ToInt32(texto.ToString()));
                        var lista = new List<ProfissaoDTO>
                    {
                        profissao
                    };
                        return lista;
                    }
                    else
                    {
                        return GetAll();
                    }

            }

        }

        #endregion
    }
}
