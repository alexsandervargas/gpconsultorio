﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Gp.App.DTO;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using Gp.Infrastructure.Repository;

namespace Gp.App.Services
{
    public class ProfissionalService
    {
        private readonly ProfissionalRepository _repository;
        private readonly GpContext _context;

        public ProfissionalService()
        {
            this._context = new GpContext();
            this._repository = new ProfissionalRepository(_context);
        }

        public IList<ProfissionalDTO> GetAll()
        {
            try
            {
                var profissionais = _repository.GetAllInformation();
                var Listaprofissionais = new List<ProfissionalDTO>();

                foreach (var profissional in profissionais)
                {
                    var profissionalDTO = new ProfissionalDTO
                    {
                        Id = profissional.Id,
                        CPF = profissional.CPF,
                        ConselhoId = profissional.Conselho.Id,
                        Nome = profissional.Nome,
                        EstadoCivilId = profissional.EstadoCivil != null ? profissional.EstadoCivil.Id : 0,
                        Logradouro = profissional.Logradouro,
                        Observacao = profissional.Observacao,
                        Bairro = profissional.Bairro,
                        CEP = profissional.CEP,
                        CidadeId = profissional.Cidade != null ? profissional.Cidade.Id : 0,
                        Complemento = profissional.Complemento,
                        Numero = profissional.Numero,
                        CodigoAreaCelular = profissional.CodigoAreaCelular,
                        CodigoAreaComercial = profissional.CodigoAreaComercial,
                        CodigoAreaResidencial = profissional.CodigoAreaResidencial,
                        Email = profissional.Email,
                        TelefoneCelular = profissional.TelefoneCelular,
                        TelefoneComercial = profissional.TelefoneComercial,
                        TelefoneResidencial = profissional.TelefoneResidencial,
                        MatriculaConselho = profissional.MatriculaConselho,
                        Nascimento = profissional.Nascimento,
                        ProfissaoId = profissional.Profissao != null ? profissional.Profissao.Id : 0,
                        RG = profissional.RG,
                        SexoId = profissional.Sexo != null ? profissional.Sexo.Id : 0,

                    };


                    Listaprofissionais.Add(profissionalDTO);
                }

                return Listaprofissionais;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }


        public IList<PacienteDTO> GetIndicacoes(int PessoaIndicouId)
        {
            var Lista = new List<PacienteDTO>();

            var pacientes = new PacienteRepository(_context).GetIndicacoes(PessoaIndicouId);

            foreach (var paciente in pacientes)
            {
                var pacientedto = new PacienteDTO
                {
                    Id = paciente.Id,
                    Ficha = paciente.Ficha,
                    Nome = paciente.Nome,
                    CPF = paciente.CPF
                };

                Lista.Add(pacientedto);
            }

            return Lista;
        }

        public ProfissionalDTO GetById(int profissionalId)
        {
            try
            {
                ProfissionalDTO profissional;
                var profissionalModel = _repository.GetByIdAllInformation(profissionalId);

                if (profissionalModel != null)
                {
                    profissional = new ProfissionalDTO
                    {
                        Id = profissionalModel.Id,
                        CPF = profissionalModel.CPF,
                        ConselhoId = profissionalModel.Conselho.Id,
                        Nome = profissionalModel.Nome,
                        EstadoCivilId = profissionalModel.EstadoCivil != null ? profissionalModel.EstadoCivil.Id : 0,
                        Logradouro = profissionalModel.Logradouro,
                        Observacao = profissionalModel.Observacao,
                        Bairro = profissionalModel.Bairro,
                        CEP = profissionalModel.CEP,
                        CidadeId = profissionalModel.Cidade != null ? profissionalModel.Cidade.Id : 0,
                        Complemento = profissionalModel.Complemento,
                        Numero = profissionalModel.Numero,
                        CodigoAreaCelular = profissionalModel.CodigoAreaCelular,
                        CodigoAreaComercial = profissionalModel.CodigoAreaComercial,
                        CodigoAreaResidencial = profissionalModel.CodigoAreaResidencial,
                        Email = profissionalModel.Email,
                        TelefoneCelular = profissionalModel.TelefoneCelular,
                        TelefoneComercial = profissionalModel.TelefoneComercial,
                        TelefoneResidencial = profissionalModel.TelefoneResidencial,
                        MatriculaConselho = profissionalModel.MatriculaConselho,
                        Nascimento = profissionalModel.Nascimento,
                        ProfissaoId = profissionalModel.Profissao != null ? profissionalModel.Profissao.Id : 0,
                        RG = profissionalModel.RG,
                        SexoId = profissionalModel.Sexo != null ? profissionalModel.Sexo.Id : 0,

                    };


                    return profissional;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {


                return null;

            }
            finally
            {
                _repository.Dispose();
            }

        }




        public void Save(ProfissionalDTO profissional)
        {
            try
            {
                ProfissionalModel profissionalModel = null;

                if (profissional.Id == 0)
                {
                    profissionalModel = new ProfissionalModel
                    {
                        Id = profissional.Id,
                        CPF = profissional.CPF,
                        Conselho = new ConselhoRepository(_context).GetById(profissional.ConselhoId),
                        Nome = profissional.Nome,
                        EstadoCivil = profissional.EstadoCivilId != 0 ? new EstadoCivilRepository(_context).GetById(profissional.EstadoCivilId) : null,
                        Logradouro = profissional.Logradouro,
                        Observacao = profissional.Observacao,
                        Bairro = profissional.Bairro,
                        CEP = profissional.CEP,
                        Cidade = profissional.CidadeId != 0 ? new CidadeRepository(_context).GetById(profissional.CidadeId) : null,
                        Complemento = profissional.Complemento,
                        Numero = profissional.Numero,
                        CodigoAreaCelular = profissional.CodigoAreaCelular,
                        CodigoAreaComercial = profissional.CodigoAreaComercial,
                        CodigoAreaResidencial = profissional.CodigoAreaResidencial,
                        Email = profissional.Email,
                        TelefoneCelular = profissional.TelefoneCelular,
                        TelefoneComercial = profissional.TelefoneComercial,
                        TelefoneResidencial = profissional.TelefoneResidencial,
                        MatriculaConselho = profissional.MatriculaConselho,
                        Nascimento = profissional.Nascimento,
                        Profissao = profissional.ProfissaoId != 0 ? new ProfissaoRepository(_context).GetById(profissional.ProfissaoId) : null,
                        RG = profissional.RG,
                        Sexo = profissional.SexoId != 0 ? new SexoRepository(_context).GetById(profissional.SexoId) : null,

                    };

                    _repository.Add(profissionalModel);
                }
                else
                {
                    profissionalModel = new ProfissionalRepository(_context).GetByIdAllInformation(profissional.Id);
                    profissionalModel.Id = profissional.Id;
                    profissionalModel.CPF = profissional.CPF;
                    profissionalModel.Conselho = new ConselhoRepository(_context).GetById(profissional.ConselhoId);
                    profissionalModel.Nome = profissional.Nome;
                    profissionalModel.EstadoCivil = profissional.EstadoCivilId != 0 ?  new EstadoCivilRepository(_context).GetById(profissional.EstadoCivilId) : null;
                    profissionalModel.Logradouro = profissional.Logradouro;
                    profissionalModel.Observacao = profissional.Observacao;
                    profissionalModel.Bairro = profissional.Bairro;
                    profissionalModel.CEP = profissional.CEP;
                    profissionalModel.Cidade = profissional.CidadeId != 0 ? new CidadeRepository(_context).GetById(profissional.CidadeId) : null;
                    profissionalModel.Complemento = profissional.Complemento;
                    profissionalModel.Numero = profissional.Numero;
                    profissionalModel.CodigoAreaCelular = profissional.CodigoAreaCelular;
                    profissionalModel.CodigoAreaComercial = profissional.CodigoAreaComercial;
                    profissionalModel.CodigoAreaResidencial = profissional.CodigoAreaResidencial;
                    profissionalModel.Email = profissional.Email;
                    profissionalModel.TelefoneCelular = profissional.TelefoneCelular;
                    profissionalModel.TelefoneComercial = profissional.TelefoneComercial;
                    profissionalModel.TelefoneResidencial = profissional.TelefoneResidencial;
                    profissionalModel.MatriculaConselho = profissional.MatriculaConselho;
                    profissionalModel.Nascimento = profissional.Nascimento;
                    profissionalModel.Profissao = profissional.ProfissaoId != 0 ? new ProfissaoRepository(_context).GetById(profissional.ProfissaoId) : null;
                    profissionalModel.RG = profissional.RG;
                    profissionalModel.Sexo = profissional.SexoId != 0 ? new SexoRepository(_context).GetById(profissional.SexoId) : null;

                    _repository.Update(profissionalModel);
                }

                _repository.SaveChanges();

                profissional.Id = profissionalModel.Id;


            }
            catch (Exception)
            {

            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Delete(int profissionalId)
        {
            _repository.Remove(profissionalId);
            _repository.SaveChanges();
            _repository.Dispose();
        }

        #region Search

        public IList<ProfissionalDTO> Search(string campo, object texto)
        {

            switch (campo)
            {
                case "Nome":
                    return GetAll().Where(u => u.Nome.ToLower().Contains(texto.ToString().ToLower())).ToList();
                default:

                    if (texto.ToString() != string.Empty)
                    {
                        var profissional = GetById(Convert.ToInt32(texto.ToString()));
                        var lista = new List<ProfissionalDTO>
                    {
                        profissional
                    };
                        return lista;
                    }
                    else
                    {
                        return GetAll();
                    }

            }

        }

        #endregion

    }
}

