﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Gp.App.DTO;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using Gp.Infrastructure.Repository;

namespace Gp.App.Services
{
    public class ConselhoService
    {
        private readonly ConselhoRepository _repository;
        private readonly GpContext _context;

        public ConselhoService()
        {
            this._context = new GpContext();
            this._repository = new ConselhoRepository(_context);
        }

        public IList<ConselhoDTO> GetAll()
        {
            try
            {
                var conselhos = _repository.GetAll();
                var Listaconselhos = new List<ConselhoDTO>();

                foreach (var conselho in conselhos)
                {
                    var conselhoDTO = new ConselhoDTO
                    {
                        Id = conselho.Id,
                        Descricao = conselho.Descricao,
                    };


                    Listaconselhos.Add(conselhoDTO);
                }

                return Listaconselhos;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public ConselhoDTO GetById(int conselhoId)
        {
            try
            {
                ConselhoDTO conselho;
                var conselhoModel = _repository.GetById(conselhoId);

                if (conselhoModel != null)
                {
                    conselho = new ConselhoDTO
                    {
                        Id = conselhoModel.Id,
                        Descricao = conselhoModel.Descricao,

                    };


                    return conselho;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Save(ConselhoDTO conselho)
        {
            try
            {
                ConselhoModel conselhoModel = null;

                if (conselho.Id == 0)
                {
                    conselhoModel = new ConselhoModel
                    {                        
                        Descricao = conselho.Descricao,
                    };

                    _repository.Add(conselhoModel);
                }
                else
                {
                    conselhoModel = new ConselhoRepository(_context).GetById(conselho.Id);
                    conselhoModel.Descricao = conselho.Descricao;
                   
                    _repository.Update(conselhoModel);
                }

                _repository.SaveChanges();

                conselho.Id = conselhoModel.Id;


            }
            catch (Exception)
            {

            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Delete(int conselhoId)
        {
            _repository.Remove(conselhoId);
            _repository.SaveChanges();
            _repository.Dispose();
        }

        #region Search

        public IList<ConselhoDTO> Search(string campo, object texto)
        {

            switch (campo)
            {
                case "Descrição":
                    return GetAll().Where(u => u.Descricao.ToLower().Contains(texto.ToString().ToLower())).ToList();
                default:

                    if (texto.ToString() != string.Empty)
                    {
                        var conselho = GetById(Convert.ToInt32(texto.ToString()));
                        var lista = new List<ConselhoDTO>
                    {
                        conselho
                    };
                        return lista;
                    }
                    else
                    {
                        return GetAll();
                    }

            }

        }

        #endregion
    }
}
