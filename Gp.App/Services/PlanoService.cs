﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Gp.App.DTO;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using Gp.Infrastructure.Repository;

namespace Gp.App.Services
{
    public class PlanoService
    {
        private readonly PlanoRepository _repository;
        private readonly GpContext _context;

        public PlanoService()
        {
            this._context = new GpContext();
            this._repository = new PlanoRepository(_context);
        }

        public IList<PlanoDTO> GetAll()
        {
            try
            {
                var planos = _repository.GetAllInformation();
                var Listaplanos = new List<PlanoDTO>();

                foreach (var plano in planos)
                {
                    var planoDTO = new PlanoDTO
                    {
                        Observacao = plano.Observacao,
                        Id = plano.Id,
                        Ativo=plano.Ativo,
                        Cooparticipacao=plano.Cooparticipacao,
                        Descricao=plano.Descricao,
                        PessoaJuridicaId = plano.PessoaJuridica.Id
                    };


                    Listaplanos.Add(planoDTO);
                }

                return Listaplanos;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }


        public IList<PlanoDTO> GetAllplanosPessoa(int PessoaId)
        {
            try
            {
                var planos = _repository.GetAllInformationByPessoaId(PessoaId);
                var Listaplanos = new List<PlanoDTO>();

                foreach (var plano in planos)
                {
                    var planoDTO = new PlanoDTO
                    {
                        Observacao = plano.Observacao,
                        Id = plano.Id,
                        Ativo = plano.Ativo,
                        Cooparticipacao = plano.Cooparticipacao,
                        Descricao = plano.Descricao,
                        PessoaJuridicaId = plano.PessoaJuridica.Id
                    };


                    Listaplanos.Add(planoDTO);
                }

                return Listaplanos;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }


        public PlanoDTO GetById(int planoId)
        {
            try
            {
                PlanoDTO plano;
                var planoModel = _repository.GetWithPessoaJuridicaById(planoId);

                if (planoModel != null)
                {
                    plano = new PlanoDTO
                    {
                        Observacao = planoModel.Observacao,
                        Id = planoModel.Id,
                        Ativo = planoModel.Ativo,
                        Cooparticipacao = planoModel.Cooparticipacao,
                        Descricao = planoModel.Descricao,
                        PessoaJuridicaId = planoModel.PessoaJuridica.Id

                    };


                    return plano;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }




        public void Save(PlanoDTO plano)
        {
            try
            {
                PlanoModel planoModel = null;

                if (plano.Id == 0)
                {
                    planoModel = new PlanoModel
                    {
                        Observacao = plano.Observacao,
                        Ativo = plano.Ativo,
                        Cooparticipacao = plano.Cooparticipacao,
                        Descricao = plano.Descricao,                        
                        PessoaJuridica = new PessoaJuridicaRepository(_context).GetByIdAllInformation(plano.PessoaJuridicaId)
                    };

                    _repository.Add(planoModel);
                }
                else
                {
                    planoModel = new PlanoRepository(_context).GetById(plano.Id);
                    planoModel.Observacao = plano.Observacao;
                    planoModel.Ativo = plano.Ativo;
                    planoModel.Cooparticipacao = plano.Cooparticipacao;
                    planoModel.Descricao = plano.Descricao;
                    planoModel.PessoaJuridica = new PessoaJuridicaRepository(_context).GetByIdAllInformation(plano.PessoaJuridicaId);

                    _repository.Update(planoModel);
                }

                _repository.SaveChanges();

                plano.Id = planoModel.Id;


            }
            catch (Exception)
            {

            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Delete(int planoId)
        {
            _repository.Remove(planoId);
            _repository.SaveChanges();
            _repository.Dispose();
        }

        #region Search

        public IList<PlanoDTO> Search(string campo, object texto)
        {

            switch (campo)
            {
                case "Descrição":
                    return GetAll().Where(u => u.Descricao.ToLower().Contains(texto.ToString().ToLower())).ToList();
                default:

                    if (texto.ToString() != string.Empty)
                    {
                        var plano = GetById(Convert.ToInt32(texto.ToString()));
                        var lista = new List<PlanoDTO>
                    {
                        plano
                    };
                        return lista;
                    }
                    else
                    {
                        return GetAll();
                    }

            }

        }

        #endregion

    }
}
