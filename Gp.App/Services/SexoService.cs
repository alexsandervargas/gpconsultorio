﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Gp.App.DTO;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using Gp.Infrastructure.Repository;

namespace Gp.App.Services
{
    public class SexoService
    {
        private readonly SexoRepository _repository;
        private readonly GpContext _context;

        public SexoService()
        {
            this._context = new GpContext();
            this._repository = new SexoRepository(_context);
        }

        public IList<SexoDTO> GetAll()
        {
            try
            {
                var sexos = _repository.GetAll();
                var Listasexos = new List<SexoDTO>();

                foreach (var sexo in sexos)
                {
                    var sexoDTO = new SexoDTO
                    {
                        Descricao = sexo.Descricao,
                        Id = sexo.Id,
                    };


                    Listasexos.Add(sexoDTO);
                }

                return Listasexos;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public SexoDTO GetById(int sexoId)
        {
            try
            {
                SexoDTO sexo;
                var sexoModel = _repository.GetById(sexoId);

                if (sexoModel != null)
                {
                    sexo = new SexoDTO
                    {
                        Descricao = sexoModel.Descricao,
                        Id = sexoId,

                    };


                    return sexo;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }




        public void Save(SexoDTO sexo)
        {
            try
            {
                SexoModel sexoModel = null;

                if (sexo.Id == 0)
                {
                    sexoModel = new SexoModel
                    {
                        Descricao = sexo.Descricao,
                        Id = sexo.Id,
                    };

                    _repository.Add(sexoModel);
                }
                else
                {
                    sexoModel = new SexoRepository(_context).GetById(sexo.Id);
                    sexoModel.Descricao = sexo.Descricao;
                    
                    _repository.Update(sexoModel);
                }

                _repository.SaveChanges();

                sexo.Id = sexoModel.Id;


            }
            catch (Exception)
            {

            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Delete(int sexoId)
        {
            _repository.Remove(sexoId);
            _repository.SaveChanges();
            _repository.Dispose();
        }

        #region Search

        public IList<SexoDTO> Search(string campo, object texto)
        {

            switch (campo)
            {
                case "Descrição":
                    return GetAll().Where(u => u.Descricao.ToLower().Contains(texto.ToString().ToLower())).ToList();
                default:

                    if (texto.ToString() != string.Empty)
                    {
                        var sexo = GetById(Convert.ToInt32(texto.ToString()));
                        var lista = new List<SexoDTO>
                    {
                        sexo
                    };
                        return lista;
                    }
                    else
                    {
                        return GetAll();
                    }

            }

        }

        #endregion

    }
}
