﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Gp.App.DTO;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using Gp.Infrastructure.Repository;

namespace Gp.App.Services
{
   public class EstadoCivilService
    {
        private readonly EstadoCivilRepository _repository;
        private readonly GpContext _context;

        public EstadoCivilService()
        {
            this._context = new GpContext();
            this._repository = new EstadoCivilRepository(_context);
        }

        public IList<EstadoCivilDTO> GetAll()
        {
            try
            {
                var estadoscivis = _repository.GetAll();
                var ListaEstados = new List<EstadoCivilDTO>();

                foreach (var estado in estadoscivis)
                {
                    var estadoDTO = new EstadoCivilDTO
                    {

                        Descricao = estado.Descricao,
                        Id = estado.Id,
                        

                    };


                    ListaEstados.Add(estadoDTO);
                }

                return ListaEstados;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public EstadoCivilDTO GetById(int EstadoCivilId)
        {
            try
            {
                EstadoCivilDTO estado;
                var estadoCivilModel = _repository.GetById(EstadoCivilId);

                if (estadoCivilModel != null)
                {
                    estado = new EstadoCivilDTO
                    {
                        Descricao= estadoCivilModel.Descricao,
                        Id = EstadoCivilId,
                        
                    };


                    return estado;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

       


        public void Save(EstadoCivilDTO estado)
        {
            try
            {
                EstadoCivilModel estadoCivilModel = null;

                if (estado.Id == 0)
                {
                    estadoCivilModel = new EstadoCivilModel
                    {
                        Descricao = estado.Descricao,
                        Id = estado.Id
                 
                    };

                    _repository.Add(estadoCivilModel);
                }
                else
                {
                    estadoCivilModel = new EstadoCivilRepository(_context).GetById(estado.Id);

                    estadoCivilModel.Descricao = estado.Descricao;

                    _repository.Update(estadoCivilModel);
                }

                _repository.SaveChanges();

                estado.Id = estadoCivilModel.Id;


            }
            catch (Exception)
            {

            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Delete(int EstadoCiVilId)
        {
            _repository.Remove(EstadoCiVilId);
            _repository.SaveChanges();
            _repository.Dispose();
        }

        #region Search

        public IList<EstadoCivilDTO> Search(string campo, object texto)
        {

            switch (campo)
            {
                case "Descrição":
                    return GetAll().Where(u => u.Descricao.ToLower().Contains(texto.ToString().ToLower())).ToList();
               default:

                    if (texto.ToString() != string.Empty)
                    {
                        var estado = GetById(Convert.ToInt32(texto.ToString()));
                        var lista = new List<EstadoCivilDTO>
                    {
                        estado
                    };
                        return lista;
                    }
                    else
                    {
                        return GetAll();
                    }

            }

        }

        #endregion



    }
}

