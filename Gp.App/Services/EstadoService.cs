﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Gp.App.DTO;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using Gp.Infrastructure.Repository;

namespace Gp.App.Services
{
    public class EstadoService
    {

        private readonly EstadoRepository _repository;
        private readonly GpContext _context;

        public EstadoService()
        {
            this._context = new GpContext();
            this._repository = new EstadoRepository(_context);
        }

        public IList<EstadoDTO> GetAll()
        {
            try
            {
                var estados = _repository.GetAllInformation();
                var ListaEstados = new List<EstadoDTO>();

                foreach (var estado in estados)
                {
                    var estadoDTO = new EstadoDTO
                    {
                        Descricao = estado.Descricao,
                        Id = estado.Id,
                        Sigla = estado.Sigla,
                        PaisId = estado.Pais.Id
                    };


                    ListaEstados.Add(estadoDTO);
                }

                return ListaEstados;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public EstadoDTO GetById(int EstadoId)
        {
            try
            {
                EstadoDTO estado;
                var estadoModel = _repository.GetWithPais(EstadoId);

                if (estadoModel != null)
                {
                    estado = new EstadoDTO
                    {
                        Descricao = estadoModel.Descricao,
                        Id = EstadoId,
                        Sigla=estadoModel.Sigla,
                        PaisId = estadoModel.Pais.Id

                    };


                    return estado;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }




        public void Save(EstadoDTO estado)
        {
            try
            {
                EstadoModel estadoModel = null;

                if (estado.Id == 0)
                {
                    estadoModel = new EstadoModel
                    {
                        Descricao = estado.Descricao,
                        Sigla=estado.Sigla.ToUpper(),
                        Id = estado.Id,
                        Pais= new PaisRepository(_context).GetById(estado.PaisId)

                    };

                    _repository.Add(estadoModel);
                }
                else
                {
                    estadoModel = new EstadoRepository(_context).GetById(estado.Id);
                    estadoModel.Pais = new PaisRepository(_context).GetById(estado.PaisId);
                    estadoModel.Descricao = estado.Descricao;
                    estadoModel.Sigla = estado.Sigla.ToUpper();

                    _repository.Update(estadoModel);
                }

                _repository.SaveChanges();

                estado.Id = estadoModel.Id;


            }
            catch (Exception)
            {

            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Delete(int EstadoId)
        {
            _repository.Remove(EstadoId);
            _repository.SaveChanges();
            _repository.Dispose();
        }

        #region Search

        public IList<EstadoDTO> Search(string campo, object texto)
        {

            switch (campo)
            {
                case "Descrição":
                    return GetAll().Where(u => u.Descricao.ToLower().Contains(texto.ToString().ToLower())).ToList();
                case "Sigla":
                    return GetAll().Where(u => u.Sigla.ToLower().Contains(texto.ToString().ToLower())).ToList();
                default:

                    if (texto.ToString() != string.Empty)
                    {
                        var estado = GetById(Convert.ToInt32(texto.ToString()));
                        var lista = new List<EstadoDTO>
                    {
                        estado
                    };
                        return lista;
                    }
                    else
                    {
                        return GetAll();
                    }

            }

        }
        #endregion

    }
}
