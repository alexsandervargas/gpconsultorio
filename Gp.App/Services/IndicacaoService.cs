﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gp.App.DTO;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using Gp.Infrastructure.Repository;

namespace Gp.App.Services
{
    public class IndicacaoService
    {
        private readonly IndicacaoRepository _repository;
        private readonly GpContext _context;

        public IndicacaoService()
        {
            this._context = new GpContext();
            this._repository = new IndicacaoRepository(_context);
        }

        public IList<IndicacaoDTO> GetAll()
        {
            try
            {
                var Indicacoes = _repository.GetAllInformation();
                var ListaIndicacoes = new List<IndicacaoDTO>();

                foreach (var Indicacao in Indicacoes)
                {
                    var IndicacaoDTO = new IndicacaoDTO
                    {
                        Id = Indicacao.Id,
                        NomeIndicacao = Indicacao.NomeIndicacao,
                        Tipo = (TipoIndicacaoDTO)Indicacao.Tipo

                    };



                    if (Indicacao.PessoaIndicou != null)
                    {
                        IndicacaoDTO.PessoaIndicouId = Indicacao.PessoaIndicou.Id;
                    };


                    ListaIndicacoes.Add(IndicacaoDTO);
                }

                return ListaIndicacoes;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

       


        public IndicacaoDTO GetById(int IndicacaoId)
        {
            try
            {
                IndicacaoDTO Indicacao;
                var IndicacaoModel = _repository.GetWithPaciente(IndicacaoId);

                if (IndicacaoModel != null)
                {
                    Indicacao = new IndicacaoDTO
                    {

                        Id = IndicacaoId,
                        NomeIndicacao = IndicacaoModel.NomeIndicacao,
                        Tipo = (TipoIndicacaoDTO)IndicacaoModel.Tipo

                    };

                    if (IndicacaoModel.PessoaIndicou != null)
                    {
                        Indicacao.PessoaIndicouId = IndicacaoModel.PessoaIndicou.Id;
                    };


                    return Indicacao;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }




        public void Save(IndicacaoDTO Indicacao)
        {
            try
            {
                IndicacaoModel IndicacaoModel = null;

                if (Indicacao.Id == 0)
                {
                    IndicacaoModel = new IndicacaoModel
                    {

                        NomeIndicacao = Indicacao.NomeIndicacao,
                        Tipo = (TipoIndicacaoModel)Indicacao.Tipo,

                    };

                    if (Indicacao.Tipo == TipoIndicacaoDTO.Cliente)
                    {
                        IndicacaoModel.PessoaIndicou = new PacienteRepository(_context).GetById(Indicacao.PessoaIndicouId);
                    }
                    else if (Indicacao.Tipo == TipoIndicacaoDTO.Médico)
                    {
                        IndicacaoModel.PessoaIndicou = new ProfissionalRepository(_context).GetById(Indicacao.PessoaIndicouId);
                    }

                    _repository.Add(IndicacaoModel);
                }
                else
                {
                    IndicacaoModel = new IndicacaoRepository(_context).GetWithPaciente(Indicacao.Id);
                    IndicacaoModel.NomeIndicacao = Indicacao.NomeIndicacao;
                    IndicacaoModel.Tipo = (TipoIndicacaoModel)Indicacao.Tipo;

                    if (Indicacao.Tipo == TipoIndicacaoDTO.Cliente)
                    {
                        IndicacaoModel.PessoaIndicou = new PacienteRepository(_context).GetByIdAllInformation(Indicacao.PessoaIndicouId);
                    }
                    else if (Indicacao.Tipo == TipoIndicacaoDTO.Médico)
                    {
                        IndicacaoModel.PessoaIndicou = new ProfissionalRepository(_context).GetById(Indicacao.PessoaIndicouId);
                    }
                    else
                    {
                        IndicacaoModel.PessoaIndicou = null;
                    }

                    _repository.Update(IndicacaoModel);
                }

                _repository.SaveChanges();

                Indicacao.Id = IndicacaoModel.Id;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Delete(int IndicacaoId)
        {
            _repository.Remove(IndicacaoId);
            _repository.SaveChanges();
            _repository.Dispose();
        }

        #region Search

        public IList<IndicacaoDTO> Search(string campo, object texto)
        {

            switch (campo)
            {
                case "Descrição":
                    return GetAll().Where(u => u.NomeIndicacao.ToLower().Contains(texto.ToString().ToLower())).ToList();
                default:

                    if (texto.ToString() != string.Empty)
                    {
                        var Indicacao = GetById(Convert.ToInt32(texto.ToString()));
                        var lista = new List<IndicacaoDTO>
                    {
                        Indicacao
                    };
                        return lista;
                    }
                    else
                    {
                        return GetAll();
                    }

            }

        }

        #endregion



    }
}
