﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Gp.App.DTO;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using Gp.Infrastructure.Repository;

namespace Gp.App.Services
{
    public class PaisService
    {
        private readonly PaisRepository _repository;
        private readonly GpContext _context;

        public PaisService()
        {
            this._context = new GpContext();
            this._repository = new PaisRepository(_context);
        }

        public IList<PaisDTO> GetAll()
        {
            try
            {
                var paises = _repository.GetAll();
                var Listapaises = new List<PaisDTO>();

                foreach (var pais in paises)
                {
                    var paisDTO = new PaisDTO
                    {

                        Descricao = pais.Descricao,
                        Id = pais.Id,


                    };


                    Listapaises.Add(paisDTO);
                }

                return Listapaises;

            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }

        public PaisDTO GetById(int paisId)
        {
            try
            {
                PaisDTO pais;
                var paisModel = _repository.GetById(paisId);

                if (paisModel != null)
                {
                    pais = new PaisDTO
                    {
                        Descricao = paisModel.Descricao,
                        Id = paisId,

                    };


                    return pais;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                _repository.Dispose();
            }

        }




        public void Save(PaisDTO pais)
        {
            try
            {
                PaisModel paisModel = null;

                if (pais.Id == 0)
                {
                    paisModel = new PaisModel
                    {
                        Descricao = pais.Descricao,
                        Id = pais.Id

                    };

                    _repository.Add(paisModel);
                }
                else
                {
                    paisModel = new PaisRepository(_context).GetById(pais.Id);

                    paisModel.Descricao = pais.Descricao;

                    _repository.Update(paisModel);
                }

                _repository.SaveChanges();

                pais.Id = paisModel.Id;


            }
            catch (Exception)
            {

            }
            finally
            {
                _repository.Dispose();
            }

        }

        public void Delete(int paisId)
        {
            _repository.Remove(paisId);
            _repository.SaveChanges();
            _repository.Dispose();
        }

        #region Search

        public IList<PaisDTO> Search(string campo, object texto)
        {

            switch (campo)
            {
                case "Descrição":
                    return GetAll().Where(u => u.Descricao.ToLower().Contains(texto.ToString().ToLower())).ToList();
                default:

                    if (texto.ToString() != string.Empty)
                    {
                        var pais = GetById(Convert.ToInt32(texto.ToString()));
                        var lista = new List<PaisDTO>
                    {
                        pais
                    };
                        return lista;
                    }
                    else
                    {
                        return GetAll();
                    }

            }

        }
        #endregion
    }
}
