﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using Gp.App.DTO;

namespace Gp
{
    public class FormsService
    {
        //Apaga dados do Form
        public void ClearForm(Control form)
        {
            foreach (Control C in form.Controls)
            {
                if (C.Controls.Count > 0)
                {
                    ClearForm(C);
                }

                if (C is TextBox || C is MaskedTextBox)
                {
                    C.Text = "";
                }
                else if (C is Label && C.Name.Contains("Lbl"))
                {
                    C.Text = "";
                }
                

            }

        }

        //Desabilita Campos do Forme
        public void DisableControls(Control form)
        {
            foreach (Control C in form.Controls)
            {
                if (C.Controls.Count > 0)
                {
                    DisableControls(C);
                }

                if (C is ToolStrip)
                {

                }
                else
                {
                    C.Enabled = false;
                }
            }
        }

        //Habilita campos do Form
        public void EnableControls(Control form)
        {
            foreach (Control C in form.Controls)
            {
                if (C.Controls.Count > 0)
                {
                    EnableControls(C);
                }

                if (C is ToolStrip)
                {

                }
                else
                {
                    C.Enabled = true;
                }
            }
        }

        //Ajusta os botões ao iniciar o Formulario 
        public void ButtonsLoadForm(UsuarioDTO usuario, Form Form, ToolStripButton BtnNovo = null, ToolStripButton BtnSalvar = null,
            ToolStripButton BtnEditar = null, ToolStripButton BtnCopiar = null, ToolStripButton BtnExcluir = null)
        {

            if (BtnNovo != null) { BtnNovo.Enabled = true; }
            if (BtnSalvar != null) { BtnSalvar.Enabled = false; }
            if (BtnEditar != null) { BtnEditar.Enabled = false; }
            if (BtnCopiar != null) { BtnCopiar.Enabled = false; }
            if (BtnExcluir != null) { BtnExcluir.Enabled = false; }

        }

        //Ajusta os botões ao clicar em #BtnNovo
        public void ButtonsNewInsert(UsuarioDTO usuario, Form Form, ToolStripButton BtnNovo = null, ToolStripButton BtnSalvar = null,
            ToolStripButton BtnEditar = null, ToolStripButton BtnCopiar = null, ToolStripButton BtnExcluir = null)
        {

            if (BtnNovo != null) { BtnNovo.Enabled = true; }
            if (BtnSalvar != null) { BtnSalvar.Enabled = true; }
            if (BtnEditar != null) { BtnEditar.Enabled = false; }
            if (BtnCopiar != null) { BtnCopiar.Enabled = false; }
            if (BtnExcluir != null) { BtnExcluir.Enabled = false; }

        }

        //Ajusta botoes ao clique do botão editar
        public void AdjutsButtonsToEdition(UsuarioDTO usuario, Form Form, ToolStripButton BtnNovo = null, ToolStripButton BtnSalvar = null,
            ToolStripButton BtnEditar = null, ToolStripButton BtnCopiar = null, ToolStripButton BtnExcluir = null)
        {
            if (BtnNovo != null) { BtnNovo.Enabled = true; }
            if (BtnSalvar != null) { BtnSalvar.Enabled = true; }
            if (BtnCopiar != null) { BtnCopiar.Enabled = true; }
            if (BtnExcluir != null) { BtnExcluir.Enabled = true; }

        }

        //Ajusta botoes ao carregar pesquisa e ao iniciar novo item
        public void AdjutsButtonsToSearchAndSave(UsuarioDTO usuario, Form Form, ToolStripButton BtnNovo = null, ToolStripButton BtnSalvar = null,
            ToolStripButton BtnEditar = null, ToolStripButton BtnCopiar = null, ToolStripButton BtnExcluir = null)
        {

            if (BtnNovo != null) { BtnNovo.Enabled = true; }
            if (BtnEditar != null) { BtnEditar.Enabled = true; }
            if (BtnSalvar != null) { BtnSalvar.Enabled = false; }
            if (BtnCopiar != null) { BtnCopiar.Enabled = false; }
            if (BtnExcluir != null) { BtnExcluir.Enabled = false; }
        }

        //Teclas de Atalho
        public void Keys(KeyEventArgs keyEvent, ToolStripButton BtnNovo = null, ToolStripButton BtnSalvar = null, ToolStripButton BtnEditar = null,
            ToolStripButton BtnPesquisar = null, ToolStripButton BtnSair = null, ToolStripButton BtnExcluir = null)
        {

            switch (keyEvent.KeyCode)
            {
                case System.Windows.Forms.Keys.F1:
                    if (BtnNovo != null) { BtnNovo.PerformClick(); } else { break; }
                    break;
                case System.Windows.Forms.Keys.F2:
                    if (BtnEditar != null) { BtnEditar.PerformClick(); } else { break; }
                    break;
                case System.Windows.Forms.Keys.F3:
                    if (BtnExcluir != null) { BtnExcluir.PerformClick(); } else { break; }
                    break;
                case System.Windows.Forms.Keys.F4:
                    if (BtnPesquisar != null) { BtnPesquisar.PerformClick(); } else { break; }
                    break;
                case System.Windows.Forms.Keys.F6:
                    if (BtnSalvar != null) { BtnSalvar.PerformClick(); } else { break; }
                    break;
                case System.Windows.Forms.Keys.Escape:
                    if (BtnSair != null) { BtnSair.PerformClick(); } else { break; }
                    break;
                case System.Windows.Forms.Keys.Enter:
                    keyEvent.Handled = true;
                    SendKeys.Send("{TAB}");
                    break;
                default:
                    break;
            }

        }

    }
}
