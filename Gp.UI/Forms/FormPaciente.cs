﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using Gp.App.DTO;
using Gp.App.Services;

namespace Gp
{
    public partial class FormPaciente : Form
    {
        FormsService Service;
        public UsuarioDTO UsuarioLogado { get; set; }
        public string Strcon { get; set; }

        public FormPaciente()
        {
            InitializeComponent();

            //Instancia FormService
            Service = new FormsService();

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.DisableControls(this);

            this.WindowState = FormWindowState.Maximized;
            this.Refresh();
        }

        //Gerencia as teclas de atalho
        private void FormPaciente_KeyDown(object sender, KeyEventArgs e)
        {
            Service.Keys(keyEvent: e, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar, BtnPesquisar: this.Pesquisar,
                BtnSair: this.Sair, BtnExcluir: this.Excluir);

        }

        //Inicialização do form com ajustes de botões
        private void FormPaciente_Load(object sender, EventArgs e)
        {
            //Ajusta os botoes
            Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

            this.AutoScroll = true;

            this.ComboTipo.DataSource = Enum.GetNames(typeof(TipoIndicacaoDTO)).ToArray();

            this.ComboTipo.SelectedIndex = 0;

            CamposIndicacao(TipoIndicacaoDTO.Nenhum);

        }

        #region LoadsRegion

        //Preenche o formulário com os dados
        public void LoadForm()
        {
            try
            {
                var Paciente = new PacienteService().GetById(Convert.ToInt32(this.TxtId.Text));

                this.TxtNome.Text = Paciente.Nome;
                this.TxtCPF.Text = Paciente.CPF;
                this.TxtLogradouro.Text = Paciente.Logradouro;
                this.TxtNumero.Text = Paciente.Numero;
                this.TxtComplemento.Text = Paciente.Complemento;
                this.TxtBairro.Text = Paciente.Bairro;
                this.TxtCidadeId.Text = Paciente.CidadeId != 0 ? Convert.ToString(Paciente.CidadeId) : string.Empty;
                this.TxtCEP.Text = Paciente.CEP;
                this.TxtObservacao.Text = Paciente.Observacao;
                this.TxtPlanoId.Text = Paciente.PlanoId != 0 ? Convert.ToString(Paciente.PlanoId) : string.Empty;
                this.TxtEstadoCivilId.Text = Paciente.EstadoCivilId != 0 ? Convert.ToString(Paciente.EstadoCivilId) : string.Empty;
                this.TxtMatricula.Text = Paciente.Matricula;
                this.TxtDataNascimento.Value = Paciente.Nascimento;
                this.TxtProfissaoId.Text = Paciente.ProfissaoId != 0 ? Convert.ToString(Paciente.ProfissaoId) : string.Empty;
                this.TxtRG.Text = Paciente.RG;
                this.TxtSexoId.Text = Paciente.SexoId != 0 ? Convert.ToString(Paciente.SexoId) : string.Empty;
                this.TxtFicha.Text = Paciente.Ficha == 0 ? this.TxtId.Text : Convert.ToString(Paciente.Ficha);
                this.TxtDDDCelular.Text = Paciente.CodigoAreaCelular == 0 ? string.Empty : Convert.ToString(Paciente.CodigoAreaCelular);
                this.TxtFoneCelular.Text = Paciente.TelefoneCelular;
                this.TxtDDDComercial.Text = Paciente.CodigoAreaComercial == 0 ? string.Empty : Convert.ToString(Paciente.CodigoAreaComercial);
                this.TxtFoneComercial.Text = Paciente.TelefoneComercial;
                this.TxtDDDResidencial.Text = Paciente.CodigoAreaResidencial == 0 ? string.Empty : Convert.ToString(Paciente.CodigoAreaResidencial);
                this.TxtFoneResidencial.Text = Paciente.TelefoneResidencial;
                this.TxtEmail.Text = Paciente.Email;
                this.TextEmpresa.Text = Paciente.Empresa;
                this.TxtLog.Text = "Data de Cadastro: " + Paciente.DataCadastro + " - Data de Atualização: " + Paciente.DataAtualizacao;

                LoadSexo();

                LoadPlano();

                LoadEstadoCivil();

                LoadProfissao();

                LoadCidade();

                if (Paciente.IndicacaoId != 0)
                {
                    var indicacao = new IndicacaoService().GetById(Paciente.IndicacaoId);
                    this.ComboTipo.SelectedItem = Enum.GetName(typeof(TipoIndicacaoDTO), (TipoIndicacaoDTO)indicacao.Tipo);

                    if (indicacao.PessoaIndicouId != 0)
                    {
                        this.TxtPessoaIndicouId.Text = indicacao.PessoaIndicouId.ToString();

                        LoadPessoaIndicou();
                    }

                    this.TxtNomePessoaIndicou.Text = indicacao.NomeIndicacao;

                    this.LblIndicacao.Text = Paciente.IndicacaoId.ToString();

                }
                else
                {
                    this.ComboTipo.SelectedText = (TipoIndicacaoDTO.Nenhum.ToString());
                }

                CamposIndicacao((TipoIndicacaoDTO)Enum.Parse(typeof(TipoIndicacaoDTO), this.ComboTipo.SelectedItem.ToString()));
                
                ValidateForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Ajusta os campos de acordo com a indicação selecionada
        public void CamposIndicacao(TipoIndicacaoDTO Tipo)
        {
            if (Tipo == TipoIndicacaoDTO.Cliente || Tipo == TipoIndicacaoDTO.Médico)
            {
                this.labelNomeIndicou.Enabled = false;
                this.TxtNomePessoaIndicou.Enabled = false;

                this.labelIndicou.Enabled = true;
                this.TxtPessoaIndicouId.Enabled = true;
                this.BtnPessoaIndicou.Enabled = true;
                this.LblPessoaIndicou.Enabled = true;

                this.TxtNomePessoaIndicou.Text = string.Empty;

            }
            else if (Tipo == TipoIndicacaoDTO.Nenhum)
            {
                this.labelNomeIndicou.Enabled = false;
                this.TxtNomePessoaIndicou.Enabled = false;


                this.labelIndicou.Enabled = false;
                this.TxtPessoaIndicouId.Enabled = false;
                this.BtnPessoaIndicou.Enabled = false;
                this.LblPessoaIndicou.Enabled = false;

                this.TxtNomePessoaIndicou.Text = string.Empty;
                this.LblPessoaIndicou.Text = string.Empty;
                this.TxtPessoaIndicouId.Text = string.Empty;

            }
            else
            {
                this.labelNomeIndicou.Enabled = true;
                this.TxtNomePessoaIndicou.Enabled = true;

                this.labelIndicou.Enabled = false;
                this.TxtPessoaIndicouId.Enabled = false;
                this.BtnPessoaIndicou.Enabled = false;
                this.LblPessoaIndicou.Enabled = false;

                this.LblPessoaIndicou.Text = string.Empty;
                this.TxtPessoaIndicouId.Text = string.Empty;
            }


        }

        //Carrega os dados da pessoa que indicou o paciente
        public void LoadPessoaIndicou()
        {
            var Tipo = (TipoIndicacaoDTO)Enum.Parse(typeof(TipoIndicacaoDTO), this.ComboTipo.SelectedItem.ToString());

            PessoaFisicaDTO pessoa = null;
            if (this.TxtPessoaIndicouId.Text != string.Empty)
            {
                if (Tipo == TipoIndicacaoDTO.Cliente)
                {
                    pessoa = new PacienteService().GetById(Convert.ToInt32(this.TxtPessoaIndicouId.Text));
                }
                else if (Tipo == TipoIndicacaoDTO.Médico)
                {
                    pessoa = new ProfissionalService().GetById(Convert.ToInt32(this.TxtPessoaIndicouId.Text));
                }

                if (pessoa != null)
                {
                    this.LblPessoaIndicou.Text = pessoa.Nome;
                    Errors.SetError(TxtPessoaIndicouId, "");
                }
                else
                {
                    this.LblPessoaIndicou.Text = string.Empty;
                    Errors.SetError(TxtPessoaIndicouId, "Código de pessoa informado é inválido");
                }

            }
        }

        //Carrega a cidade nos seus devidos campos do form
        public void LoadCidade()
        {
            if (this.TxtCidadeId.Text != string.Empty)
            {

                var cidade = new CidadeService().GetById(Convert.ToInt32(this.TxtCidadeId.Text));
                if (cidade != null)
                {
                    var estado = new EstadoService().GetById(cidade.EstadoId);
                    this.TxtCidadeId.Text = Convert.ToString(cidade.Id);
                    this.LblCidadeDescricao.Text = cidade.Descricao + "/" + estado.Sigla;
                    this.Errors.SetError(TxtCidadeId, "");
                }
                else
                {
                    this.LblCidadeDescricao.Text = "";
                    this.Errors.SetError(TxtCidadeId, "Código de Cidade Inválido.");
                }

            }

        }

        //Carrega o Sexo nos seus devidos campos do form
        public void LoadSexo()
        {
            if (this.TxtSexoId.Text != string.Empty)
            {


                var sexo = new SexoService().GetById(Convert.ToInt32(this.TxtSexoId.Text));
                if (sexo != null)
                {
                    this.TxtSexoId.Text = Convert.ToString(sexo.Id);
                    this.LblSexo.Text = sexo.Descricao;
                    this.Errors.SetError(TxtSexoId, "");
                }
                else
                {
                    this.LblSexo.Text = "";
                    this.Errors.SetError(TxtSexoId, "Código do Gênero/Sexo Inválido.");
                }

            }


        }

        //Carrega o Plano nos seus devidos campos do form
        public void LoadPlano()
        {
            if (this.TxtPlanoId.Text != string.Empty)
            {

                var Plano = new PlanoService().GetById(Convert.ToInt32(this.TxtPlanoId.Text));
                if (Plano != null)
                {
                    this.TxtPlanoId.Text = Convert.ToString(Plano.Id);
                    this.LblPlano.Text = Plano.Descricao;
                    this.Errors.SetError(TxtPlanoId, "");
                }
                else
                {
                    this.LblPlano.Text = "";
                    this.Errors.SetError(TxtPlanoId, "Código do Plano Inválido.");
                }
            }

        }

        //Carrega o estado civil nos seus devidos campos do form
        public void LoadEstadoCivil()
        {
            if (this.TxtEstadoCivilId.Text != string.Empty)
            {
                var estadocivil = new EstadoCivilService().GetById(Convert.ToInt32(this.TxtEstadoCivilId.Text));
                if (estadocivil != null)
                {

                    this.TxtEstadoCivilId.Text = Convert.ToString(estadocivil.Id);
                    this.LblEstadoCivil.Text = estadocivil.Descricao;
                    this.Errors.SetError(TxtEstadoCivilId, "");
                }
                else
                {
                    this.LblEstadoCivil.Text = "";
                    this.Errors.SetError(TxtEstadoCivilId, "Código do Estado Civil Inválido.");
                }
            }

        }

        //Carrega a profissão nos seus devidos campos do form
        public void LoadProfissao()
        {
            if (this.TxtProfissaoId.Text != string.Empty)
            {
                var profissao = new ProfissaoService().GetById(Convert.ToInt32(this.TxtProfissaoId.Text));
                if (profissao != null)
                {

                    this.TxtProfissaoId.Text = Convert.ToString(profissao.Id);
                    this.LblProfissao.Text = profissao.Descricao;
                    this.Errors.SetError(TxtProfissaoId, "");
                }
                else
                {
                    this.LblProfissao.Text = "";
                    this.Errors.SetError(TxtProfissaoId, "Código da Profissão Inválido.");
                }

            }
        }


        #endregion

        #region CamposForm

        private void ComboTipo_SelectedIndexChanged(object sender, EventArgs e)
        {

            TipoIndicacaoDTO Tipo = (TipoIndicacaoDTO)Enum.Parse(typeof(TipoIndicacaoDTO), this.ComboTipo.SelectedItem.ToString());

            CamposIndicacao(Tipo);

        }

        private void TxtCidadeId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtCidadeId, "");
        }

        private void TxtCidadeId_Leave(object sender, EventArgs e)
        {
            if (this.TxtCidadeId.Text != string.Empty)
            {
                LoadCidade();
            }
            else
            {
                LblCidadeDescricao.Text = string.Empty;
            }
        }

        private void TxtSexoId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtSexoId, "");
        }

        private void TxtSexoId_Leave(object sender, EventArgs e)
        {
            if (this.TxtSexoId.Text != string.Empty)
            {
                LoadSexo();
            }
            else
            {
                LblSexo.Text = string.Empty;
            }
        }

        private void TxtEstadoCivilId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtEstadoCivilId, "");
        }

        private void TxtEstadoCivilId_Leave(object sender, EventArgs e)
        {
            if (this.TxtEstadoCivilId.Text != string.Empty)
            {
                LoadEstadoCivil();
            }
            else
            {
                LblEstadoCivil.Text = string.Empty;
            }
        }

        private void TxtProfissaoId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtProfissaoId, "");
        }

        private void TxtProfissaoId_Leave(object sender, EventArgs e)
        {
            if (this.TxtProfissaoId.Text != string.Empty)
            {
                LoadProfissao();
            }
            else
            {
                LblProfissao.Text = string.Empty;
            }
        }

        private void TxtPlanoId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtPlanoId, "");
        }

        private void TxtPlanoId_Leave(object sender, EventArgs e)
        {
            if (this.TxtPlanoId.Text != string.Empty)
            {
                LoadPlano();
            }
            else
            {
                LblPlano.Text = string.Empty;
            }
        }
                                 
        private void TxtPessoaIndicouId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtPessoaIndicouId, "");
        }

        private void TxtPessoaIndicouId_Leave(object sender, EventArgs e)
        {
            if (this.TxtProfissaoId.Text != string.Empty)
            {
                LoadPessoaIndicou();
            }
            else
            {
                LblProfissao.Text = string.Empty;
            }

        }
        #endregion

        #region ValidationsForm
        //Validações do Formulário Usuário
        public bool ValidateForm()
        {
            bool Validate = true;

            try
            {
                var tipo = (TipoIndicacaoDTO)Enum.Parse(typeof(TipoIndicacaoDTO), this.ComboTipo.SelectedItem.ToString());

                //Veryfy Text Nome
                if (this.TxtNome.Text == string.Empty)
                {
                    this.Errors.SetError(TxtNome, "Informe o Nome do Paciente");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtNome, "");
                }


                //Veryfy Text CPF
                //if (this.TxtCPF.Text == string.Empty)
                //{
                //    this.Errors.SetError(TxtCPF, "Informe o CPF do Paciente");
                //    Validate = false;
                //}
                //else
                //{
                //    this.Errors.SetError(TxtCPF, "");
                //}

                ////Veryfy Text RG
                //if (this.TxtRG.Text == string.Empty)
                //{
                //    this.Errors.SetError(TxtRG, "Informe o RG do Paciente");
                //    Validate = false;
                //}
                //else
                //{
                //    this.Errors.SetError(TxtRG, "");
                //}

                //Veryfy Text Data de Nascimento
                if (this.TxtDataNascimento.Text == string.Empty)
                {
                    this.Errors.SetError(TxtDataNascimento, "Informe a Data de Nascimento do Paciente");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtDataNascimento, "");
                }

                //Veryfy Text Sexo
                //if (this.TxtSexoId.Text == string.Empty || this.LblSexo.Text == string.Empty)
                //{
                //    this.Errors.SetError(TxtSexoId, "Informe o Gênero/Sexo do Paciente");
                //    Validate = false;
                //}
                //else
                //{
                //    this.Errors.SetError(TxtSexoId, "");
                //}

                ////Veryfy Text Estado CIvil
                //if (this.TxtEstadoCivilId.Text == string.Empty || this.LblEstadoCivil.Text == string.Empty)
                //{
                //    this.Errors.SetError(TxtEstadoCivilId, "Informe o Estado Civil do Paciente");
                //    Validate = false;
                //}
                //else
                //{
                //    this.Errors.SetError(TxtEstadoCivilId, "");
                //}

                //Veryfy Text Profissão
                //if (this.TxtProfissaoId.Text == string.Empty || this.LblProfissao.Text == string.Empty)
                //{
                //    this.Errors.SetError(TxtProfissaoId, "Informe a Profissão do Paciente");
                //    Validate = false;
                //}
                //else
                //{
                //    this.Errors.SetError(TxtProfissaoId, "");
                //}

                ////Veryfy Text Plano
                //if (this.TxtPlanoId.Text == string.Empty || this.LblPlano.Text == string.Empty)
                //{
                //    this.Errors.SetError(TxtPlanoId, "Informe o Plano do Paciente");
                //    Validate = false;
                //}
                //else
                //{
                //    this.Errors.SetError(TxtPlanoId, "");
                //}


                //Veryfy Indicações
                if ((tipo == TipoIndicacaoDTO.Cliente || tipo == TipoIndicacaoDTO.Médico) && (this.TxtPessoaIndicouId.Text == string.Empty || this.LblPessoaIndicou.Text == string.Empty))
                {
                    this.Errors.SetError(TxtPessoaIndicouId, "Informe a pessoa que indicou o paciente");
                    Validate = false;
                }
                else if ((tipo == TipoIndicacaoDTO.Funcionário || tipo == TipoIndicacaoDTO.Outros) && this.TxtNomePessoaIndicou.Text == string.Empty)
                {
                    this.Errors.SetError(TxtNomePessoaIndicou, "Informe o nome da pessoa que indicou o paciente");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtPessoaIndicouId, "");
                    this.Errors.SetError(TxtNomePessoaIndicou, "");
                }


                //Veryfy Text Logadrouro
                //if (this.TxtLogradouro.Text == string.Empty)
                //{
                //    this.Errors.SetError(TxtLogradouro, "Informe o logradouro da Empresa");
                //    Validate = false;
                //}
                //else
                //{
                //    this.Errors.SetError(TxtLogradouro, "");
                //}


                ////Veryfy Text numero
                //if (this.TxtNumero.Text == string.Empty)
                //{
                //    this.Errors.SetError(TxtNumero, "Informe o número da Empresa");
                //    Validate = false;
                //}
                //else
                //{
                //    this.Errors.SetError(TxtNumero, "");
                //}

                ////Veryfy Text bairro
                //if (this.TxtBairro.Text == string.Empty)
                //{
                //    this.Errors.SetError(TxtBairro, "Informe o Bairro da Empresa");
                //    Validate = false;
                //}
                //else
                //{
                //    this.Errors.SetError(TxtBairro, "");
                //}

                ////Veryfy Cidade
                //if (this.TxtCidadeId.Text == string.Empty || this.LblCidadeDescricao.Text == string.Empty)
                //{
                //    this.Errors.SetError(TxtCidadeId, "Informe a Cidade");
                //    Validate = false;
                //}
                //else
                //{
                //    this.Errors.SetError(TxtCidadeId, "");
                //}

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return Validate;

        }

        #endregion

        #region ButtonsMenu

        //Habilita nova inserção de empresa no formulário
        private void Novo_Click(object sender, EventArgs e)
        {
            //Verifica se formulário esta preenchido e em edição de algum estado 
            //Caso esteja questiona o usuário se deseja salvar as alterações realizadas ou não
            if (this.TxtId.Text != "" && GpPaciente.Enabled == true)
            {
                if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Salvar.PerformClick();
                }
            }

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            this.ComboTipo.SelectedIndex = 1;
            this.ComboTipo.SelectedIndex = 0;


            //Desabilita os Campos do Formulário
            Service.EnableControls(this);

            //Desabilita os Botoes do form
            Service.ButtonsNewInsert(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

        }

        //Salva dados preenchido no formulário
        private void Salvar_Click(object sender, EventArgs e)
        {

            if (ValidateForm())
            {
                try
                {

                    PacienteDTO Paciente = new PacienteDTO
                    {
                        Nome = this.TxtNome.Text,
                        CPF = this.TxtCPF.Text,
                        Logradouro = this.TxtLogradouro.Text,
                        Numero = this.TxtNumero.Text,
                        Complemento = this.TxtComplemento.Text,
                        Bairro = this.TxtBairro.Text,
                        CidadeId = this.TxtCidadeId.Text != string.Empty ? Convert.ToInt32(this.TxtCidadeId.Text) : 0,
                        CEP = this.TxtCEP.Text,
                        Observacao = this.TxtObservacao.Text,
                        Email = this.TxtEmail.Text,
                        TelefoneCelular = this.TxtFoneCelular.Text,
                        TelefoneComercial = this.TxtFoneComercial.Text,
                        TelefoneResidencial = this.TxtFoneResidencial.Text,
                        PlanoId = this.TxtPlanoId.Text != string.Empty ? Convert.ToInt32(this.TxtPlanoId.Text) : 0,
                        EstadoCivilId = this.TxtEstadoCivilId.Text != string.Empty ? Convert.ToInt32(this.TxtEstadoCivilId.Text) : 0,
                        Matricula = this.TxtMatricula.Text,
                        Nascimento = this.TxtDataNascimento.Value,
                        ProfissaoId = this.TxtProfissaoId.Text != string.Empty ? Convert.ToInt32(this.TxtProfissaoId.Text) : 0,
                        RG = this.TxtRG.Text,
                        SexoId = this.TxtSexoId.Text != string.Empty ? Convert.ToInt32(this.TxtSexoId.Text) : 0,
                        Empresa = this.TextEmpresa.Text

                    };

                    Paciente.Ficha = this.TxtFicha.Text != string.Empty ? Convert.ToInt32(this.TxtFicha.Text) : 0;
                    Paciente.Id = this.TxtId.Text != string.Empty ? Convert.ToInt32(this.TxtId.Text) : 0;
                    Paciente.CodigoAreaCelular = this.TxtDDDCelular.Text != string.Empty ? Convert.ToInt32(this.TxtDDDCelular.Text) : 0;
                    Paciente.CodigoAreaComercial = this.TxtDDDComercial.Text != string.Empty ? Convert.ToInt32(this.TxtDDDComercial.Text) : 0;
                    Paciente.CodigoAreaResidencial = this.TxtDDDResidencial.Text != string.Empty ? Convert.ToInt32(this.TxtDDDResidencial.Text) : 0;
                    Paciente.IndicacaoId = this.LblIndicacao.Text != string.Empty ? Convert.ToInt32(this.LblIndicacao.Text) : 0;
                    var Tipo = (TipoIndicacaoDTO)Enum.Parse(typeof(TipoIndicacaoDTO), this.ComboTipo.SelectedItem.ToString());

                    if (Paciente.Id != 0)
                    {
                        if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                    }

                    new PacienteService().Save(Paciente);

                    if (Paciente.Id != 0)
                    {
                        this.TxtId.Text = Convert.ToString(Paciente.Id);

                        Service.DisableControls(this);

                        SalvarIndicacao(Tipo, Paciente);

                        Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                        MessageBox.Show("Paciente [" + this.TxtNome.Text + "] salvo com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        //Salva a indicação do paciente
        private void SalvarIndicacao(TipoIndicacaoDTO Tipo, PacienteDTO Paciente)
        {
            try
            {
                //Salvar a indicação
                if (Tipo == TipoIndicacaoDTO.Nenhum && Paciente.IndicacaoId != 0)
                {
                    var Id = Paciente.IndicacaoId;
                    Paciente.IndicacaoId = 0; ;
                    new PacienteService().Save(Paciente);
                    new IndicacaoService().Delete(Convert.ToInt32(Id));
                    this.LblIndicacao.Text = string.Empty;
                }
                else if (Tipo != TipoIndicacaoDTO.Nenhum)
                {
                    IndicacaoDTO indicacao = new IndicacaoDTO
                    {
                        NomeIndicacao = this.TxtNomePessoaIndicou.Text,
                        Tipo = Tipo,
                    };

                    indicacao.PessoaIndicouId = TxtPessoaIndicouId.Text != string.Empty ? Convert.ToInt32(TxtPessoaIndicouId.Text) : 0;

                    indicacao.Id = Paciente.IndicacaoId != 0 ? Paciente.IndicacaoId : 0;

                    new IndicacaoService().Save(indicacao);

                    if (indicacao.Id != 0)
                    {

                        Paciente.IndicacaoId = indicacao.Id;
                        new PacienteService().Save(Paciente);
                        this.LblIndicacao.Text = Convert.ToString(indicacao.Id);
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Habilita controle para edição
        private void Editar_Click(object sender, EventArgs e)
        {
            Service.EnableControls(this);

            Service.AdjutsButtonsToEdition(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

        }

        //Exclui 
        private void Excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Confirma Exclusão?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        new PacienteService().Delete(Convert.ToInt32(this.TxtId.Text));
                        this.Service.ClearForm(this);
                        this.Service.DisableControls(this);

                        Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

                        MessageBox.Show("Exclusão Confirmada!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        //Realiza cópia dos dados em tela
        private void Copiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Deseja Copiar este Cliente / Paciente ?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var Paciente = new PacienteService().GetById(Convert.ToInt32(this.TxtId.Text));
                        Paciente.Nome += " Cópia";
                        Paciente.Id = 0;
                        Paciente.CidadeId = Convert.ToInt32(this.TxtCidadeId.Text);

                        new PacienteService().Save(Paciente);

                        this.TxtId.Text = Paciente.Id.ToString();

                        this.Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

                        LoadForm();

                        MessageBox.Show("Cópia efetuada com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnIndicações_Click(object sender, EventArgs e)
        {
            if (this.TxtId.Text != string.Empty)
            {


                var Indicacoes = new PacienteService().GetIndicacoes(Convert.ToInt32(this.TxtId.Text));

                if (Indicacoes.Count != 0)
                {
                    var form = new FormIndicacoes();

                    form.DtgViewIndicacoes.AutoGenerateColumns = false;
                    form.DtgViewIndicacoes.DataSource = Indicacoes;
                    form.DtgViewIndicacoes.Refresh();
                    form.LblIndicacao.Text = Indicacoes.Count.ToString();

                    form.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Esta Pessoa não possui indicações", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }

        }

        //Abre formulário para pesquisa
        private void Pesquisar_Click(object sender, EventArgs e)
        {
            var form = new FormPacienteBusca();

            form.ShowDialog();

            var Paciente = form.Paciente;

            if (Paciente != null)
            {

                this.Service.ClearForm(this);
                this.Service.DisableControls(this);
                Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                this.TxtId.Text = Paciente.Id.ToString();

                LoadForm();

            }
        }

        //Fecha Formulário
        private void Sair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Botão de pesquisa de Cidade
        private void BtnCidade_Click(object sender, EventArgs e)
        {
            var form = new FormCidadeBusca();

            form.ShowDialog();

            var cidade = form.Cidade;

            if (cidade != null)
            {
                this.TxtCidadeId.Text = cidade.Id.ToString();

                LoadCidade();

            }
        }

        //Botão de pesquisa de Sexo
        private void BtnSexo_Click(object sender, EventArgs e)
        {
            var form = new FormSexoBusca();

            form.ShowDialog();

            var sexo = form.Sexo;

            if (sexo != null)
            {
                this.TxtSexoId.Text = sexo.Id.ToString();

                LoadSexo();

            }
        }

        //Botão de pesquisa de Estado CIvil
        private void BtnEstadoCivil_Click(object sender, EventArgs e)
        {
            var form = new FormEstadoCivilBusca();

            form.ShowDialog();

            var estadocivil = form.EstadoCivil;

            if (estadocivil != null)
            {
                this.TxtEstadoCivilId.Text = estadocivil.Id.ToString();

                LoadEstadoCivil();

            }
        }

        //Botão de pesquisa de Profissão
        private void BtnProfissao_Click(object sender, EventArgs e)
        {
            var form = new FormProfissaoBusca();

            form.ShowDialog();

            var profissao = form.profissao;

            if (profissao != null)
            {
                this.TxtProfissaoId.Text = profissao.Id.ToString();

                LoadProfissao();

            }
        }

        //Botão de pesquisa de Plano
        private void BtnPlano_Click(object sender, EventArgs e)
        {
            var form = new FormPlanoBusca();

            form.ShowDialog();

            var Plano = form.Plano;

            if (Plano != null)
            {
                this.TxtPlanoId.Text = Plano.Id.ToString();

                LoadPlano();

            }
        }

        private void BtnPessoaIndicou_Click(object sender, EventArgs e)
        {
            var Tipo = (TipoIndicacaoDTO)Enum.Parse(typeof(TipoIndicacaoDTO), this.ComboTipo.SelectedItem.ToString());

            PessoaFisicaDTO pessoa = null;


            if (Tipo == TipoIndicacaoDTO.Cliente)
            {
                var form = new FormPacienteBusca();

                form.ShowDialog();

                pessoa = form.Paciente;
            }
            else if (Tipo == TipoIndicacaoDTO.Médico)
            {
                var form = new FormProfissionalBusca();

                form.ShowDialog();

                pessoa = form.Profissional;
            }

            if (pessoa != null)
            {
                this.TxtPessoaIndicouId.Text = pessoa.Id.ToString();

                LoadPessoaIndicou();

            }



        }

        private void BtnImprimir_Click(object sender, EventArgs e)
        {
            if (this.TxtId.Text != string.Empty)
            {
                Strcon = string.Empty;
                var Pacientes = new PacienteService().Ficha(Convert.ToInt32(this.TxtId.Text));
                ReportDocument report = new ReportDocument();
                string path = System.Configuration.ConfigurationManager.AppSettings["ReportsFilePath"] + "RelFichaCadastral.rpt";
                var StrConn = System.Configuration.ConfigurationManager.ConnectionStrings["Dbase1"].ToString();

                var psw = StrConn.Split(';')[3].Split('=')[1];
                var usr = StrConn.Split(';')[2].Split('=')[1];
                var serv = StrConn.Split(';')[0].Split('=')[1];
                var db = StrConn.Split(';')[1].Split('=')[1];

                report.Load(path);

                report.SetDatabaseLogon(usr, psw, serv, db);
                //report.DataSourceConnections[0].SetConnection(serv, db,usr,psw);


                report.SetDataSource(Pacientes);

                var form = new FormPacienteReport
                {
                    MdiParent = this.MdiParent
                };

                form.Report.ReportSource = report;
                form.Report.Refresh();

                form.Show();

            }
        }



        #endregion

        private void TxtCEP_Leave(object sender, EventArgs e)
        {
            if(this.TxtCEP.Text!= string.Empty)
            {
                var cep = new CepService().GetCEP(this.TxtCEP.Text);

                if (cep != null)
                {
                    this.TxtLogradouro.Text = cep[0];
                    this.TxtComplemento.Text = cep[1];
                    this.TxtBairro.Text = cep[2];
                    this.TxtCidadeId.Text = cep[3];                    
                    TxtCidadeId_Leave(sender, e);
                    this.TxtNumero.Focus();
                }
                else
                {
                    this.TxtLogradouro.Text = string.Empty;
                    this.TxtComplemento.Text = string.Empty;
                    this.TxtBairro.Text = string.Empty;
                    this.TxtCidadeId.Text = string.Empty;
                    TxtCidadeId_Leave(sender, e);
                    this.TxtCEP.Focus();
                }
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
