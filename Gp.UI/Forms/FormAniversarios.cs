﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using Gp.App.DTO;
using Gp.App.Services;

namespace Gp
{
    public partial class FormAniversarios : Form
    {
        public FormAniversarios()
        {
            InitializeComponent();
        }

        private void BtnFiltrar_Click(object sender, EventArgs e)
        {
            var Lista = new PacienteService().GetAniversariantes(this.TxtDataInicial.Value, this.TxtDataFInal.Value);

            this.DtgViewIndicacoes.DataSource = null;
            this.DtgViewIndicacoes.Refresh();
            this.DtgViewIndicacoes.Rows.Clear();
            this.DtgViewIndicacoes.AutoGenerateColumns = false;

            DtgViewIndicacoes.DataSource = Lista;
            DtgViewIndicacoes.Refresh();

            this.LblAniversariantes.Text = Lista.Count.ToString();
        }

        private void BtnImprimir_Click(object sender, EventArgs e)
        {

            var Aniversariantes = new PacienteService().ReportAniversariantes(this.TxtDataInicial.Value, this.TxtDataFInal.Value);
            ReportDocument report = new ReportDocument();
            string path = System.Configuration.ConfigurationManager.AppSettings["ReportsFilePath"] + "RelAniversariantes.rpt";
            var StrConn = System.Configuration.ConfigurationManager.ConnectionStrings["Dbase1"].ToString();

            var psw = StrConn.Split(';')[3].Split('=')[1];
            var usr = StrConn.Split(';')[2].Split('=')[1];
            var serv = StrConn.Split(';')[0].Split('=')[1];
            var db = StrConn.Split(';')[1].Split('=')[1];

            if (!System.IO.File.Exists(path))
            {
                 MessageBox.Show("Relatório não localizado :\n" +  path,"GP",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);

                return;
            }
                                                  
            report.Load(path);

            report.SetDatabaseLogon(usr, psw, serv, db);
            //report.DataSourceConnections[0].SetConnection(serv, db,usr,psw);


            report.SetDataSource(Aniversariantes);

            var form = new FormAniversariosReport();


            form.Report.ReportSource = report;
            form.Report.Refresh();

            form.ShowDialog();


        }
    }
}
