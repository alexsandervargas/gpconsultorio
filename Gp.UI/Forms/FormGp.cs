﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gp.App.DTO;
using Gp.App.Services;

namespace Gp
{
    public partial class FormGp : Form
    {


        private UsuarioDTO UsuarioLogado { get; set; }

        public FormGp(UsuarioDTO usuario)
        {
            InitializeComponent();
            UsuarioLogado = usuario;
            this.IdUsuario.Text = usuario.Id.ToString();
            this.NomeUsuario.Text = usuario.Nome;
        }

        public FormGp()
        {
            InitializeComponent();
        }


        //Verifica se formulario chamado esta aberto, 
        //caso esteja o traz para frente e caso nao esteja abre um novo form
        private bool ShowForm<T>() where T : Form, new()
        {
            // verifica se o form já foi criado...
            T form = null;

            foreach (Form childForm in this.MdiChildren)
                if (childForm is T)
                {
                    form = (T)childForm;
                    break;
                }

            // se encontrou, traz para frente...
            if (form != null)
            {
                form.BringToFront();
                form.Activate();
                return false;
            }
            else
            {
                return true;

            }
        }
             
        private void FormGp_Load(object sender, EventArgs e)
        {

        }

        private void usuárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ShowForm<FormUsuario>())
            {
                var form = new FormUsuario
                {
                    UsuarioLogado = UsuarioLogado,
                    MdiParent = this
                };
                form.Show();
            }
        }

        private void estadoCivilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ShowForm<FormEstadoCivil>())
            {
                var form = new FormEstadoCivil
                {
                    UsuarioLogado = UsuarioLogado,
                    MdiParent = this
                };
                form.Show();
            }
        }

        private void paísToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ShowForm<FormPaises>())
            {
                var form = new FormPaises
                {
                    UsuarioLogado = UsuarioLogado,
                    MdiParent = this
                };
                form.Show();
            }
        }

        private void generoSexoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ShowForm<FormSexo>())
            {
                var form = new FormSexo
                {
                    UsuarioLogado = UsuarioLogado,
                    MdiParent = this
                };
                form.Show();
            }
        }

        private void estadoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (this.ShowForm<FormEstado>())
            {
                var form = new FormEstado
                {
                    UsuarioLogado = UsuarioLogado,
                    MdiParent = this
                };
                form.Show();
            }
        }

        private void cidadeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (this.ShowForm<FormCidade>())
            {
                var form = new FormCidade
                {
                    UsuarioLogado = UsuarioLogado,
                    MdiParent = this
                };
                form.Show();
            }
        }

        private void conselhoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ShowForm<FormConselho>())
            {
                var form = new FormConselho
                {
                    UsuarioLogado = UsuarioLogado,
                    MdiParent = this
                };
                form.Show();
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            conselhoToolStripMenuItem.PerformClick();
        }

        private void empresaDeSaúdeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ShowForm<FormEmpresa>())
            {
                var form = new FormEmpresa
                {
                    UsuarioLogado = UsuarioLogado,
                    MdiParent = this
                };
                form.Show();
            }

        }

        private void BtnEmpresa_Click(object sender, EventArgs e)
        {
            empresaDeSaúdeToolStripMenuItem.PerformClick();
        }

        private void profissãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ShowForm<FormProfissao>())
            {
                var form = new FormProfissao
                {
                    UsuarioLogado = UsuarioLogado,
                    MdiParent = this
                };
                form.Show();
            }

        }

        private void BtnProfissional_Click(object sender, EventArgs e)
        {
            dentistaMédicoToolStripMenuItem.PerformClick();
        }

        private void dentistaMédicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ShowForm<FormProfissional>())
            {
                var form = new FormProfissional
                {
                    UsuarioLogado = UsuarioLogado,
                    MdiParent = this
                };
                form.Show();
            }
        }

        private void planoDeSaúdeOdontológicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ShowForm<FormPlano>())
            {
                var form = new FormPlano
                {
                    UsuarioLogado = UsuarioLogado,
                    MdiParent = this
                };
                form.Show();
            }
        }

        private void BtnPlano_Click(object sender, EventArgs e)
        {
            planoDeSaúdeOdontológicoToolStripMenuItem.PerformClick();
        }

        private void clientePacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ShowForm<FormPaciente>())
            {
                var form = new FormPaciente
                {
                    UsuarioLogado = UsuarioLogado,
                    MdiParent = this
                };
                form.Show();
            }
        }

        private void BtnPaciente_Click(object sender, EventArgs e)
        {
            clientePacienteToolStripMenuItem.PerformClick();
        }

        private void anivesariantesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ShowForm<FormAniversarios>())
            {
                var form = new FormAniversarios
                {
                    MdiParent = this
                };
                form.Show();
            }
        }
    }
}
