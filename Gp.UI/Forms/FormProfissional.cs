﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gp.App.DTO;
using Gp.App.Services;

namespace Gp
{
    public partial class FormProfissional : Form
    {
        FormsService Service;
        public UsuarioDTO UsuarioLogado { get; set; }

        public FormProfissional()
        {
            InitializeComponent();

            //Instancia FormService
            Service = new FormsService();

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.DisableControls(this);

            this.WindowState = FormWindowState.Maximized;
            this.Refresh();
        }

        //Gerencia as teclas de atalho
        private void FormProfissional_KeyDown(object sender, KeyEventArgs e)
        {
            Service.Keys(keyEvent: e, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar, BtnPesquisar: this.Pesquisar,
                BtnSair: this.Sair, BtnExcluir: this.Excluir);

        }

        //Inicialização do form com ajustes de botões
        private void FormProfissional_Load(object sender, EventArgs e)
        {
            //Ajusta os botoes
            Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

            this.AutoScroll = true;


        }

        #region LoadsRegion

        //Preenche o formulário com os dados
        public void LoadForm()
        {
            try
            {
                var profissional = new ProfissionalService().GetById(Convert.ToInt32(this.TxtId.Text));

                this.TxtNome.Text = profissional.Nome;
                this.TxtCPF.Text = profissional.CPF;
                this.TxtLogradouro.Text = profissional.Logradouro;
                this.TxtNumero.Text = profissional.Numero;
                this.TxtComplemento.Text = profissional.Complemento;
                this.TxtBairro.Text = profissional.Bairro;
                this.TxtCidadeId.Text = profissional.CidadeId != 0 ? Convert.ToString(profissional.CidadeId) : string.Empty;
                this.TxtCEP.Text = profissional.CEP;
                this.TxtObservacao.Text = profissional.Observacao;
                this.TxtConselhoId.Text = Convert.ToString(profissional.ConselhoId);
                this.TxtEstadoCivilId.Text = profissional.EstadoCivilId != 0 ? Convert.ToString(profissional.EstadoCivilId) : string.Empty;
                this.TxtRegistro.Text = profissional.MatriculaConselho;
                this.TxtDataNascimento.Value = profissional.Nascimento;
                this.TxtProfissaoId.Text = profissional.ProfissaoId != 0 ? Convert.ToString(profissional.ProfissaoId) : string.Empty;
                this.TxtRG.Text = profissional.RG;
                this.TxtSexoId.Text = profissional.SexoId != 0 ? Convert.ToString(profissional.SexoId) : string.Empty;
                this.TxtDDDCelular.Text = profissional.CodigoAreaCelular == 0 ? string.Empty : Convert.ToString(profissional.CodigoAreaCelular);
                this.TxtFoneCelular.Text = profissional.TelefoneCelular;
                this.TxtDDDComercial.Text = profissional.CodigoAreaComercial == 0 ? string.Empty : Convert.ToString(profissional.CodigoAreaComercial);
                this.TxtFoneComercial.Text = profissional.TelefoneComercial;
                this.TxtDDDResidencial.Text = profissional.CodigoAreaResidencial == 0 ? string.Empty : Convert.ToString(profissional.CodigoAreaResidencial);
                this.TxtFoneResidencial.Text = profissional.TelefoneResidencial;
                this.TxtEmail.Text = profissional.Email;
                this.TxtLog.Text = "Data de Cadastro: " + profissional.DataCadastro + " - Data de Atualização: " + profissional.DataAtualizacao;

                LoadSexo();

                LoadConselho();

                LoadEstadoCivil();

                LoadProfissao();

                LoadCidade();

                ValidateForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Carrega a cidade nos seus devidos campos do form
        public void LoadCidade()
        {
            if (this.TxtCidadeId.Text != string.Empty)
            {

                var cidade = new CidadeService().GetById(Convert.ToInt32(this.TxtCidadeId.Text));
                if (cidade != null)
                {
                    var estado = new EstadoService().GetById(cidade.EstadoId);
                    this.TxtCidadeId.Text = Convert.ToString(cidade.Id);
                    this.LblCidadeDescricao.Text = cidade.Descricao + "/" + estado.Sigla;
                    this.Errors.SetError(TxtCidadeId, "");
                }
                else
                {
                    this.LblCidadeDescricao.Text = "";
                    this.Errors.SetError(TxtCidadeId, "Código de Cidade Inválido.");
                }
            }

        }

        //Carrega o Sexo nos seus devidos campos do form
        public void LoadSexo()
        {

            if (this.TxtSexoId.Text != string.Empty)
            {

                var sexo = new SexoService().GetById(Convert.ToInt32(this.TxtSexoId.Text));
                if (sexo != null)
                {
                    this.TxtSexoId.Text = Convert.ToString(sexo.Id);
                    this.LblSexo.Text = sexo.Descricao;
                    this.Errors.SetError(TxtSexoId, "");
                }
                else
                {
                    this.LblSexo.Text = "";
                    this.Errors.SetError(TxtSexoId, "Código do Gênero/Sexo Inválido.");
                }

            }
        }

        //Carrega o conselho nos seus devidos campos do form
        public void LoadConselho()
        {
            if (this.TxtConselhoId.Text != string.Empty)
            {

                var conselho = new ConselhoService().GetById(Convert.ToInt32(this.TxtConselhoId.Text));
                if (conselho != null)
                {
                    this.TxtConselhoId.Text = Convert.ToString(conselho.Id);
                    this.LblConselho.Text = conselho.Descricao;
                    this.Errors.SetError(TxtConselhoId, "");
                }
                else
                {
                    this.LblConselho.Text = "";
                    this.Errors.SetError(TxtConselhoId, "Código do Conselho Inválido.");
                }
            }

        }

        //Carrega o estado civil nos seus devidos campos do form
        public void LoadEstadoCivil()
        {
            if (this.TxtEstadoCivilId.Text != string.Empty)
            {

                var estadocivil = new EstadoCivilService().GetById(Convert.ToInt32(this.TxtEstadoCivilId.Text));
                if (estadocivil != null)
                {

                    this.TxtEstadoCivilId.Text = Convert.ToString(estadocivil.Id);
                    this.LblEstadoCivil.Text = estadocivil.Descricao;
                    this.Errors.SetError(TxtEstadoCivilId, "");
                }
                else
                {
                    this.LblEstadoCivil.Text = "";
                    this.Errors.SetError(TxtEstadoCivilId, "Código do Estado Civil Inválido.");
                }

            }
        }

        //Carrega a profissão nos seus devidos campos do form
        public void LoadProfissao()
        {
            if (this.TxtProfissaoId.Text != string.Empty)
            {

                var profissao = new ProfissaoService().GetById(Convert.ToInt32(this.TxtProfissaoId.Text));
                if (profissao != null)
                {

                    this.TxtProfissaoId.Text = Convert.ToString(profissao.Id);
                    this.LblProfissao.Text = profissao.Descricao;
                    this.Errors.SetError(TxtProfissaoId, "");
                }
                else
                {
                    this.LblProfissao.Text = "";
                    this.Errors.SetError(TxtProfissaoId, "Código da Profissão Inválido.");
                }

            }

        }


        #endregion

        #region CamposForm
        private void TxtCidadeId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtCidadeId, "");
        }

        private void TxtCidadeId_Leave(object sender, EventArgs e)
        {
            if (this.TxtCidadeId.Text != string.Empty)
            {
                LoadCidade();
            }
            else
            {
                LblCidadeDescricao.Text = string.Empty;
            }
        }

        private void TxtSexoId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtSexoId, "");
        }

        private void TxtSexoId_Leave(object sender, EventArgs e)
        {
            if (this.TxtSexoId.Text != string.Empty)
            {
                LoadSexo();
            }
            else
            {
                LblSexo.Text = string.Empty;
            }
        }

        private void TxtEstadoCivilId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtEstadoCivilId, "");
        }

        private void TxtEstadoCivilId_Leave(object sender, EventArgs e)
        {
            if (this.TxtEstadoCivilId.Text != string.Empty)
            {
                LoadEstadoCivil();
            }
            else
            {
                LblEstadoCivil.Text = string.Empty;
            }
        }

        private void TxtProfissaoId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtProfissaoId, "");
        }

        private void TxtProfissaoId_Leave(object sender, EventArgs e)
        {
            if (this.TxtProfissaoId.Text != string.Empty)
            {
                LoadProfissao();
            }
            else
            {
                LblProfissao.Text = string.Empty;
            }
        }

        private void TxtConselhoId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtConselhoId, "");
        }

        private void TxtConselhoId_Leave(object sender, EventArgs e)
        {
            if (this.TxtConselhoId.Text != string.Empty)
            {
                LoadConselho();
            }
        }

        #endregion

        #region ValidationsForm
        //Validações do Formulário Usuário
        public bool ValidateForm()
        {
            bool Validate = true;

            try
            {
                //Veryfy Text Nome
                if (this.TxtNome.Text == string.Empty)
                {
                    this.Errors.SetError(TxtNome, "Informe o Nome do Profissional");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtNome, "");
                }



                //Veryfy Text Data de Nascimento
                if (this.TxtDataNascimento.Text == string.Empty)
                {
                    this.Errors.SetError(TxtDataNascimento, "Informe a Data de Nascimento do Profissional");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtDataNascimento, "");
                }


                //Veryfy Text Conselho
                if (this.TxtConselhoId.Text == string.Empty || this.LblConselho.Text == string.Empty)
                {
                    this.Errors.SetError(TxtConselhoId, "Informe o Conselho do Profissional");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtConselhoId, "");
                }




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return Validate;

        }

        #endregion

        #region ButtonsMenu

        //Habilita nova inserção de empresa no formulário
        private void Novo_Click(object sender, EventArgs e)
        {
            //Verifica se formulário esta preenchido e em edição de algum estado 
            //Caso esteja questiona o usuário se deseja salvar as alterações realizadas ou não
            if (this.TxtId.Text != "" && GpProfissional.Enabled == true)
            {
                if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Salvar.PerformClick();
                }
            }

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.EnableControls(this);

            //Desabilita os Botoes do form
            Service.ButtonsNewInsert(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

        }

        //Salva dados preenchido no formulário
        private void Salvar_Click(object sender, EventArgs e)
        {

            if (ValidateForm())
            {
                try
                {

                    ProfissionalDTO profissional = new ProfissionalDTO
                    {
                        Nome = this.TxtNome.Text,
                        CPF = this.TxtCPF.Text,
                        Logradouro = this.TxtLogradouro.Text,
                        Numero = this.TxtNumero.Text,
                        Complemento = this.TxtComplemento.Text,
                        Bairro = this.TxtBairro.Text,
                        CidadeId = this.TxtCidadeId.Text != string.Empty ? Convert.ToInt32(this.TxtCidadeId.Text) : 0,
                        CEP = this.TxtCEP.Text,
                        Observacao = this.TxtObservacao.Text,
                        Email = this.TxtEmail.Text,
                        TelefoneCelular = this.TxtFoneCelular.Text,
                        TelefoneComercial = this.TxtFoneComercial.Text,
                        TelefoneResidencial = this.TxtFoneResidencial.Text,
                        ConselhoId = Convert.ToInt32(this.TxtConselhoId.Text),
                        EstadoCivilId = this.TxtEstadoCivilId.Text != string.Empty ? Convert.ToInt32(this.TxtEstadoCivilId.Text) : 0,
                        MatriculaConselho = this.TxtRegistro.Text,
                        Nascimento = this.TxtDataNascimento.Value,
                        ProfissaoId = this.TxtProfissaoId.Text != string.Empty ? Convert.ToInt32(this.TxtProfissaoId.Text) : 0,
                        RG = this.TxtRG.Text,
                        SexoId = this.TxtSexoId.Text != string.Empty ? Convert.ToInt32(this.TxtSexoId.Text) : 0,
                        Id = this.TxtId.Text != string.Empty ? Convert.ToInt32(this.TxtId.Text) : 0,
                        CodigoAreaCelular = this.TxtDDDCelular.Text != string.Empty ? Convert.ToInt32(this.TxtDDDCelular.Text) : 0,
                        CodigoAreaComercial = this.TxtDDDComercial.Text != string.Empty ? Convert.ToInt32(this.TxtDDDComercial.Text) : 0,
                        CodigoAreaResidencial = this.TxtDDDResidencial.Text != string.Empty ? Convert.ToInt32(this.TxtDDDResidencial.Text) : 0,
                    };

                    if (profissional.Id != 0)
                    {
                        if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                    }

                    new ProfissionalService().Save(profissional);

                    if (profissional.Id != 0)
                    {
                        this.TxtId.Text = Convert.ToString(profissional.Id);

                        Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                        MessageBox.Show("Profissional [" + this.TxtNome.Text + "] salvo com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        //Habilita controle para edição
        private void Editar_Click(object sender, EventArgs e)
        {
            Service.EnableControls(this);

            Service.AdjutsButtonsToEdition(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

        }

        //Exclui 
        private void Excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Confirma Exclusão?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        new ProfissionalService().Delete(Convert.ToInt32(this.TxtId.Text));
                        this.Service.ClearForm(this);
                        this.Service.DisableControls(this);

                        Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

                        MessageBox.Show("Exclusão Confirmada!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        //Realiza cópia dos dados em tela
        private void Copiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Deseja Copiar este Profissional ?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var profissional = new ProfissionalService().GetById(Convert.ToInt32(this.TxtId.Text));
                        profissional.Nome += " Cópia";
                        profissional.Id = 0;
                        profissional.CidadeId = Convert.ToInt32(this.TxtCidadeId.Text);

                        new ProfissionalService().Save(profissional);

                        this.TxtId.Text = profissional.Id.ToString();

                        this.Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

                        LoadForm();

                        MessageBox.Show("Cópia efetuada com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Abre formulário para pesquisa
        private void Pesquisar_Click(object sender, EventArgs e)
        {
            var form = new FormProfissionalBusca();

            form.ShowDialog();

            var profissional = form.Profissional;

            if (profissional != null)
            {

                this.Service.ClearForm(this);
                this.Service.DisableControls(this);
                Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                this.TxtId.Text = profissional.Id.ToString();

                LoadForm();

            }
        }

        //Fecha Formulário
        private void Sair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Botão de pesquisa de Cidade
        private void BtnCidade_Click(object sender, EventArgs e)
        {
            var form = new FormCidadeBusca();

            form.ShowDialog();

            var cidade = form.Cidade;

            if (cidade != null)
            {
                this.TxtCidadeId.Text = cidade.Id.ToString();

                LoadCidade();

            }
        }

        //Botão de pesquisa de Sexo
        private void BtnSexo_Click(object sender, EventArgs e)
        {
            var form = new FormSexoBusca();

            form.ShowDialog();

            var sexo = form.Sexo;

            if (sexo != null)
            {
                this.TxtSexoId.Text = sexo.Id.ToString();

                LoadSexo();

            }
        }

        //Botão de pesquisa de Estado CIvil
        private void BtnEstadoCivil_Click(object sender, EventArgs e)
        {
            var form = new FormEstadoCivilBusca();

            form.ShowDialog();

            var estadocivil = form.EstadoCivil;

            if (estadocivil != null)
            {
                this.TxtEstadoCivilId.Text = estadocivil.Id.ToString();

                LoadEstadoCivil();

            }
        }

        //Botão de pesquisa de Profissão
        private void BtnProfissao_Click(object sender, EventArgs e)
        {
            var form = new FormProfissaoBusca();

            form.ShowDialog();

            var profissao = form.profissao;

            if (profissao != null)
            {
                this.TxtProfissaoId.Text = profissao.Id.ToString();

                LoadProfissao();

            }
        }

        //Botão de pesquisa de Conselho
        private void BtnConselho_Click(object sender, EventArgs e)
        {
            var form = new FormConselhoBusca();

            form.ShowDialog();

            var conselho = form.Conselho;

            if (conselho != null)
            {
                this.TxtConselhoId.Text = conselho.Id.ToString();

                LoadConselho();

            }
        }


        private void BtnIndicacoes_Click(object sender, EventArgs e)
        {
            if (this.TxtId.Text != string.Empty)
            {


                var Indicacoes = new PacienteService().GetIndicacoes(Convert.ToInt32(this.TxtId.Text));

                if (Indicacoes.Count != 0)
                {
                    var form = new FormIndicacoes();

                    form.DtgViewIndicacoes.AutoGenerateColumns = false;
                    form.DtgViewIndicacoes.DataSource = Indicacoes;
                    form.DtgViewIndicacoes.Refresh();
                    form.LblIndicacao.Text = Indicacoes.Count.ToString();

                    form.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Esta Pessoa não possui indicações", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
        }


        #endregion

        private void TxtCEP_Leave(object sender, EventArgs e)
        {
            if (this.TxtCEP.Text != string.Empty)
            {
                var cep = new CepService().GetCEP(this.TxtCEP.Text);

                if (cep != null)
                {
                    this.TxtLogradouro.Text = cep[0];
                    this.TxtComplemento.Text = cep[1];
                    this.TxtBairro.Text = cep[2];
                    this.TxtCidadeId.Text = cep[3];
                    TxtCidadeId_Leave(sender, e);
                    this.TxtNumero.Focus();
                }
                else
                {
                    this.TxtLogradouro.Text = string.Empty;
                    this.TxtComplemento.Text = string.Empty;
                    this.TxtBairro.Text = string.Empty;
                    this.TxtCidadeId.Text = string.Empty;
                    TxtCidadeId_Leave(sender, e);
                    this.TxtCEP.Focus();
                }
            }
        }
    }
}
