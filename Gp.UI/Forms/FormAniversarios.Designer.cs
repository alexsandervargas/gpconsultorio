﻿namespace Gp
{
    partial class FormAniversarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAniversarios));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BtnImprimir = new System.Windows.Forms.Button();
            this.BtnFiltrar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtDataFInal = new System.Windows.Forms.DateTimePicker();
            this.TxtDataInicial = new System.Windows.Forms.DateTimePicker();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.LblAniversariantes = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DtgViewIndicacoes = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nascimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FIcha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TelefoneCelular = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Residencial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comercial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgViewIndicacoes)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.BtnImprimir);
            this.groupBox1.Controls.Add(this.BtnFiltrar);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TxtDataFInal);
            this.groupBox1.Controls.Add(this.TxtDataInicial);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1021, 67);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtro";
            // 
            // BtnImprimir
            // 
            this.BtnImprimir.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir.Image")));
            this.BtnImprimir.Location = new System.Drawing.Point(458, 20);
            this.BtnImprimir.Name = "BtnImprimir";
            this.BtnImprimir.Size = new System.Drawing.Size(95, 41);
            this.BtnImprimir.TabIndex = 5;
            this.BtnImprimir.Text = "Imprimir";
            this.BtnImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnImprimir.UseVisualStyleBackColor = true;
            this.BtnImprimir.Click += new System.EventHandler(this.BtnImprimir_Click);
            // 
            // BtnFiltrar
            // 
            this.BtnFiltrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtnFiltrar.Image = ((System.Drawing.Image)(resources.GetObject("BtnFiltrar.Image")));
            this.BtnFiltrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnFiltrar.Location = new System.Drawing.Point(360, 20);
            this.BtnFiltrar.Name = "BtnFiltrar";
            this.BtnFiltrar.Size = new System.Drawing.Size(92, 41);
            this.BtnFiltrar.TabIndex = 4;
            this.BtnFiltrar.Text = "Filtrar";
            this.BtnFiltrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnFiltrar.UseVisualStyleBackColor = true;
            this.BtnFiltrar.Click += new System.EventHandler(this.BtnFiltrar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(196, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Dt. Final";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Dt. Inicial";
            // 
            // TxtDataFInal
            // 
            this.TxtDataFInal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.TxtDataFInal.Location = new System.Drawing.Point(257, 30);
            this.TxtDataFInal.Name = "TxtDataFInal";
            this.TxtDataFInal.Size = new System.Drawing.Size(97, 20);
            this.TxtDataFInal.TabIndex = 1;
            // 
            // TxtDataInicial
            // 
            this.TxtDataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.TxtDataInicial.Location = new System.Drawing.Point(83, 30);
            this.TxtDataInicial.Name = "TxtDataInicial";
            this.TxtDataInicial.Size = new System.Drawing.Size(97, 20);
            this.TxtDataInicial.TabIndex = 0;
            // 
            // LblAniversariantes
            // 
            this.LblAniversariantes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LblAniversariantes.AutoSize = true;
            this.LblAniversariantes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAniversariantes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.LblAniversariantes.Location = new System.Drawing.Point(173, 466);
            this.LblAniversariantes.Name = "LblAniversariantes";
            this.LblAniversariantes.Size = new System.Drawing.Size(19, 20);
            this.LblAniversariantes.TabIndex = 49;
            this.LblAniversariantes.Text = "0";
            this.LblAniversariantes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 466);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 20);
            this.label3.TabIndex = 48;
            this.label3.Text = "Nº  Anivesariantes:";
            // 
            // DtgViewIndicacoes
            // 
            this.DtgViewIndicacoes.AllowUserToAddRows = false;
            this.DtgViewIndicacoes.AllowUserToDeleteRows = false;
            this.DtgViewIndicacoes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DtgViewIndicacoes.BackgroundColor = System.Drawing.Color.White;
            this.DtgViewIndicacoes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgViewIndicacoes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Nascimento,
            this.FIcha,
            this.Nome,
            this.TelefoneCelular,
            this.Residencial,
            this.Comercial,
            this.Email});
            this.DtgViewIndicacoes.Location = new System.Drawing.Point(0, 85);
            this.DtgViewIndicacoes.MultiSelect = false;
            this.DtgViewIndicacoes.Name = "DtgViewIndicacoes";
            this.DtgViewIndicacoes.ReadOnly = true;
            this.DtgViewIndicacoes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DtgViewIndicacoes.Size = new System.Drawing.Size(1046, 367);
            this.DtgViewIndicacoes.TabIndex = 47;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Código";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Width = 80;
            // 
            // Nascimento
            // 
            this.Nascimento.DataPropertyName = "Nascimento";
            this.Nascimento.HeaderText = "Nascimento";
            this.Nascimento.Name = "Nascimento";
            this.Nascimento.ReadOnly = true;
            // 
            // FIcha
            // 
            this.FIcha.DataPropertyName = "Ficha";
            this.FIcha.HeaderText = "Ficha";
            this.FIcha.Name = "FIcha";
            this.FIcha.ReadOnly = true;
            this.FIcha.Width = 80;
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "Nome";
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            // 
            // TelefoneCelular
            // 
            this.TelefoneCelular.DataPropertyName = "TelefoneCelular";
            this.TelefoneCelular.HeaderText = "Celular";
            this.TelefoneCelular.Name = "TelefoneCelular";
            this.TelefoneCelular.ReadOnly = true;
            // 
            // Residencial
            // 
            this.Residencial.DataPropertyName = "TelefoneResidencial";
            this.Residencial.HeaderText = "Residencial";
            this.Residencial.Name = "Residencial";
            this.Residencial.ReadOnly = true;
            // 
            // Comercial
            // 
            this.Comercial.DataPropertyName = "TelefoneComercial";
            this.Comercial.HeaderText = "Comercial";
            this.Comercial.Name = "Comercial";
            this.Comercial.ReadOnly = true;
            // 
            // Email
            // 
            this.Email.DataPropertyName = "Email";
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            this.Email.Width = 200;
            // 
            // FormAniversarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 493);
            this.Controls.Add(this.LblAniversariantes);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DtgViewIndicacoes);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAniversarios";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Anivesáriantes";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgViewIndicacoes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button BtnFiltrar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker TxtDataFInal;
        private System.Windows.Forms.DateTimePicker TxtDataInicial;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        public System.Windows.Forms.Label LblAniversariantes;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.DataGridView DtgViewIndicacoes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nascimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn FIcha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn TelefoneCelular;
        private System.Windows.Forms.DataGridViewTextBoxColumn Residencial;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comercial;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.Button BtnImprimir;
    }
}