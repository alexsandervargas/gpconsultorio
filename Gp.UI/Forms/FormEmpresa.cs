﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gp.App.DTO;
using Gp.App.Services;

namespace Gp
{
    public partial class FormEmpresa : Form
    {
        FormsService Service;
        public UsuarioDTO UsuarioLogado { get; set; }

        public FormEmpresa()
        {
            InitializeComponent();

            //Instancia FormService
            Service = new FormsService();

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.DisableControls(this);

            this.WindowState = FormWindowState.Maximized;
            this.Refresh();
        }

        //Gerencia as teclas de atalho
        private void FormCidade_KeyDown(object sender, KeyEventArgs e)
        {
            Service.Keys(keyEvent: e, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar, BtnPesquisar: this.Pesquisar,
                BtnSair: this.Sair, BtnExcluir: this.Excluir);

        }

        //Inicialização do form com ajustes de botões
        private void FormEmpresa_Load(object sender, EventArgs e)
        {
            //Ajusta os botoes
            Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);
            this.AutoScroll = true;
        }

        //Preenche o formulário com os dados
        public void LoadForm()
        {
            try
            {
                var empresa = new PessoaJuridicaService().GetById(Convert.ToInt32(this.TxtId.Text));


                this.TxtNome.Text = empresa.Nome;
                this.TxtNomeFantasia.Text = empresa.NomeFantasia;
                this.TxtCNPJ.Text = empresa.CNPJ;
                this.TxtInscricaoEstadual.Text = empresa.InscricaoEstadual;
                this.TxtLogradouro.Text = empresa.Logradouro;
                this.TxtNumero.Text = empresa.Numero;
                this.TxtComplemento.Text = empresa.Complemento;
                this.TxtBairro.Text = empresa.Bairro;
                this.TxtCidadeId.Text = empresa.CidadeId != 0 ? Convert.ToString(empresa.CidadeId) : string.Empty;
                this.TxtCEP.Text = empresa.CEP;
                this.TxtObservacao.Text = empresa.Observacao;
                this.TxtDDDCelular.Text = empresa.CodigoAreaCelular != 0 ? Convert.ToString(empresa.CodigoAreaCelular) : string.Empty;
                this.TxtFoneCelular.Text = empresa.TelefoneCelular;
                this.TxtDDDComercial.Text = empresa.CodigoAreaComercial != 0 ? Convert.ToString(empresa.CodigoAreaComercial) : string.Empty;
                this.TxtFoneComercial.Text = empresa.TelefoneComercial;
                this.TxtDDDResidencial.Text = empresa.CodigoAreaResidencial != 0 ? Convert.ToString(empresa.CodigoAreaResidencial) : string.Empty;
                this.TxtFoneResidencial.Text = empresa.TelefoneResidencial;
                this.TxtEmail.Text = empresa.Email;

                LoadCidade();

                ValidateForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Carrega a cidade nos seus devidos campos do form
        public void LoadCidade()
        {
            if (this.TxtCidadeId.Text != string.Empty)
            {
                var cidade = new CidadeService().GetById(Convert.ToInt32(this.TxtCidadeId.Text));
                if (cidade != null)
                {
                    var estado = new EstadoService().GetById(cidade.EstadoId);
                    this.TxtCidadeId.Text = Convert.ToString(cidade.Id);
                    this.LblCidadeDescricao.Text = cidade.Descricao + "/" + estado.Sigla;
                    this.Errors.SetError(TxtCidadeId, "");
                }
                else
                {
                    this.LblCidadeDescricao.Text = "";
                    this.Errors.SetError(TxtCidadeId, "Código de Cidade Inválido.");
                }
            }

        }


        #region CamposForm
        private void TxtCidadeId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtCidadeId, "");
        }

        private void TxtCidadeId_Leave(object sender, EventArgs e)
        {
            if (this.TxtCidadeId.Text != string.Empty)
            {
                LoadCidade();
            }
        }
        #endregion

        #region ValidationsForm
        //Validações do Formulário Usuário
        public bool ValidateForm()
        {
            bool Validate = true;

            try
            {
                //Veryfy Text Nome
                if (this.TxtNome.Text == string.Empty)
                {
                    this.Errors.SetError(TxtNome, "Informe o Nome da Empresa");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtNome, "");
                }


                //Veryfy Text Nome Fantasia
                if (this.TxtNomeFantasia.Text == string.Empty)
                {
                    this.Errors.SetError(TxtNomeFantasia, "Informe o Nome Fantasia da Empresa");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtNomeFantasia, "");
                }

                //Veryfy Text CNPJ
                if (this.TxtCNPJ.Text == string.Empty)
                {
                    this.Errors.SetError(TxtCNPJ, "Informe o CNPJ da Empresa");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtCNPJ, "");
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return Validate;

        }

        #endregion

        #region ButtonsMenu

        //Habilita nova inserção de empresa no formulário
        private void Novo_Click(object sender, EventArgs e)
        {
            //Verifica se formulário esta preenchido e em edição de algum estado 
            //Caso esteja questiona o usuário se deseja salvar as alterações realizadas ou não
            if (this.TxtId.Text != "" && GpCidade.Enabled == true)
            {
                if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Salvar.PerformClick();
                }
            }

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.EnableControls(this);

            //Desabilita os Botoes do form
            Service.ButtonsNewInsert(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

        }

        //Salva dados preenchido no formulário
        private void Salvar_Click(object sender, EventArgs e)
        {

            if (ValidateForm())
            {
                try
                {

                    PessoaJuridicaDTO empresa = new PessoaJuridicaDTO
                    {
                        Nome = this.TxtNome.Text,
                        NomeFantasia = this.TxtNomeFantasia.Text,
                        CNPJ = this.TxtCNPJ.Text,
                        InscricaoEstadual = this.TxtInscricaoEstadual.Text,
                        Logradouro = this.TxtLogradouro.Text,
                        Numero = this.TxtNumero.Text,
                        Complemento = this.TxtComplemento.Text,
                        Bairro = this.TxtBairro.Text,
                        CidadeId = this.TxtCidadeId.Text != string.Empty ? Convert.ToInt32(this.TxtCidadeId.Text) : 0,
                        CEP = this.TxtCEP.Text,
                        Observacao = this.TxtObservacao.Text,
                        Email = this.TxtEmail.Text,
                        TelefoneCelular = this.TxtFoneCelular.Text,
                        TelefoneComercial = this.TxtFoneComercial.Text,
                        TelefoneResidencial = this.TxtFoneResidencial.Text,
                        Id = this.TxtId.Text != string.Empty ? Convert.ToInt32(this.TxtId.Text) : 0,
                        CodigoAreaCelular = this.TxtDDDCelular.Text != string.Empty ? Convert.ToInt32(this.TxtDDDCelular.Text) : 0,
                        CodigoAreaComercial = this.TxtDDDComercial.Text != string.Empty ? Convert.ToInt32(this.TxtDDDComercial.Text) : 0,
                        CodigoAreaResidencial = this.TxtDDDResidencial.Text != string.Empty ? Convert.ToInt32(this.TxtDDDResidencial.Text) : 0,

                    };


                    if (empresa.Id != 0)
                    {
                        if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                    }

                    new PessoaJuridicaService().Save(empresa);

                    if (empresa.Id != 0)
                    {
                        this.TxtId.Text = Convert.ToString(empresa.Id);

                        Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                        MessageBox.Show("Empresa [" + this.TxtNome.Text + "] salva com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        //Habilita controle para edição
        private void Editar_Click(object sender, EventArgs e)
        {
            Service.EnableControls(this);

            Service.AdjutsButtonsToEdition(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

        }

        //Exclui 
        private void Excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Confirma Exclusão?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        new PessoaJuridicaService().Delete(Convert.ToInt32(this.TxtId.Text));
                        this.Service.ClearForm(this);
                        this.Service.DisableControls(this);

                        Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

                        MessageBox.Show("Exclusão Confirmada!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        //Realiza cópia dos dados em tela
        private void Copiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Deseja Copiar esta Empresa ?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var empresa = new PessoaJuridicaService().GetById(Convert.ToInt32(this.TxtId.Text));
                        empresa.Nome += " Cópia";
                        empresa.NomeFantasia += " Cópia";
                        empresa.Id = 0;
                        empresa.CidadeId = Convert.ToInt32(this.TxtCidadeId.Text);

                        new PessoaJuridicaService().Save(empresa);

                        this.TxtId.Text = empresa.Id.ToString();

                        this.Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

                        LoadForm();

                        MessageBox.Show("Cópia efetuada com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Abre formulário para pesquisa
        private void Pesquisar_Click(object sender, EventArgs e)
        {
            var form = new FormEmpresaBusca();

            form.ShowDialog();

            var empresa = form.Empresa;

            if (empresa != null)
            {

                this.Service.ClearForm(this);
                this.Service.DisableControls(this);
                Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                this.TxtId.Text = empresa.Id.ToString();

                LoadForm();

            }
        }

        //Fecha Formulário
        private void Sair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Botão de pesquisa de país
        private void BtnCiade_Click(object sender, EventArgs e)
        {
            var form = new FormCidadeBusca();

            form.ShowDialog();

            var cidade = form.Cidade;

            if (cidade != null)
            {
                this.TxtCidadeId.Text = cidade.Id.ToString();

                LoadCidade();

            }
        }

        #endregion

        private void TxtCEP_Leave(object sender, EventArgs e)
        {
            if (this.TxtCEP.Text != string.Empty)
            {
                var cep = new CepService().GetCEP(this.TxtCEP.Text);

                if (cep != null)
                {
                    this.TxtLogradouro.Text = cep[0];
                    this.TxtComplemento.Text = cep[1];
                    this.TxtBairro.Text = cep[2];
                    this.TxtCidadeId.Text = cep[3];
                    TxtCidadeId_Leave(sender, e);
                    this.TxtNumero.Focus();
                }
                else
                {
                    this.TxtLogradouro.Text = string.Empty;
                    this.TxtComplemento.Text = string.Empty;
                    this.TxtBairro.Text = string.Empty;
                    this.TxtCidadeId.Text = string.Empty;
                    TxtCidadeId_Leave(sender, e);
                    this.TxtCEP.Focus();
                }               
            }
        }
    }
}
