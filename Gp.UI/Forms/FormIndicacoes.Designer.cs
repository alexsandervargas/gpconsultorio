﻿namespace Gp
{
    partial class FormIndicacoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DtgViewIndicacoes = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FIcha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CPF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.LblIndicacao = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DtgViewIndicacoes)).BeginInit();
            this.SuspendLayout();
            // 
            // DtgViewIndicacoes
            // 
            this.DtgViewIndicacoes.AllowUserToAddRows = false;
            this.DtgViewIndicacoes.AllowUserToDeleteRows = false;
            this.DtgViewIndicacoes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DtgViewIndicacoes.BackgroundColor = System.Drawing.Color.White;
            this.DtgViewIndicacoes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgViewIndicacoes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.FIcha,
            this.Nome,
            this.CPF});
            this.DtgViewIndicacoes.Location = new System.Drawing.Point(12, 12);
            this.DtgViewIndicacoes.MultiSelect = false;
            this.DtgViewIndicacoes.Name = "DtgViewIndicacoes";
            this.DtgViewIndicacoes.ReadOnly = true;
            this.DtgViewIndicacoes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DtgViewIndicacoes.Size = new System.Drawing.Size(776, 396);
            this.DtgViewIndicacoes.TabIndex = 44;
            this.DtgViewIndicacoes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgViewIndicacoes_CellContentClick);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Código";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Width = 80;
            // 
            // FIcha
            // 
            this.FIcha.DataPropertyName = "Ficha";
            this.FIcha.HeaderText = "Ficha";
            this.FIcha.Name = "FIcha";
            this.FIcha.ReadOnly = true;
            this.FIcha.Width = 80;
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "Nome";
            this.Nome.HeaderText = "Nome do Indicado";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            // 
            // CPF
            // 
            this.CPF.DataPropertyName = "CPF";
            this.CPF.HeaderText = "CPF do Indicado";
            this.CPF.Name = "CPF";
            this.CPF.ReadOnly = true;
            this.CPF.Width = 150;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 421);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 20);
            this.label1.TabIndex = 45;
            this.label1.Text = "Nº Indicações:";
            // 
            // LblIndicacao
            // 
            this.LblIndicacao.AutoSize = true;
            this.LblIndicacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblIndicacao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.LblIndicacao.Location = new System.Drawing.Point(143, 421);
            this.LblIndicacao.Name = "LblIndicacao";
            this.LblIndicacao.Size = new System.Drawing.Size(19, 20);
            this.LblIndicacao.TabIndex = 46;
            this.LblIndicacao.Text = "0";
            this.LblIndicacao.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FormIndicacoes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.LblIndicacao);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DtgViewIndicacoes);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormIndicacoes";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Relação de Indicações";
            ((System.ComponentModel.ISupportInitialize)(this.DtgViewIndicacoes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn FIcha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn CPF;
        public System.Windows.Forms.DataGridView DtgViewIndicacoes;
        public System.Windows.Forms.Label LblIndicacao;
    }
}