﻿namespace Gp
{
    partial class FormUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUsuario));
            this.Errors = new System.Windows.Forms.ErrorProvider(this.components);
            this.MenuForm = new System.Windows.Forms.ToolStrip();
            this.Novo = new System.Windows.Forms.ToolStripButton();
            this.Salvar = new System.Windows.Forms.ToolStripButton();
            this.Editar = new System.Windows.Forms.ToolStripButton();
            this.Excluir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.Copiar = new System.Windows.Forms.ToolStripButton();
            this.Pesquisar = new System.Windows.Forms.ToolStripButton();
            this.Sair = new System.Windows.Forms.ToolStripButton();
            this.GpUsuario = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ComboAtivo = new System.Windows.Forms.ComboBox();
            this.TxtConfirmaSenha = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtSenha = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtLogin = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtLog = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Errors)).BeginInit();
            this.MenuForm.SuspendLayout();
            this.GpUsuario.SuspendLayout();
            this.SuspendLayout();
            // 
            // Errors
            // 
            this.Errors.ContainerControl = this;
            this.Errors.RightToLeft = true;
            // 
            // MenuForm
            // 
            this.MenuForm.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.MenuForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Novo,
            this.Salvar,
            this.Editar,
            this.Excluir,
            this.toolStripSeparator,
            this.Copiar,
            this.Pesquisar,
            this.Sair});
            this.MenuForm.Location = new System.Drawing.Point(0, 0);
            this.MenuForm.Name = "MenuForm";
            this.MenuForm.Size = new System.Drawing.Size(660, 25);
            this.MenuForm.TabIndex = 0;
            this.MenuForm.Text = "toolStrip1";
            // 
            // Novo
            // 
            this.Novo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Novo.Image = ((System.Drawing.Image)(resources.GetObject("Novo.Image")));
            this.Novo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Novo.Name = "Novo";
            this.Novo.Size = new System.Drawing.Size(23, 22);
            this.Novo.Text = "Novo - F1";
            this.Novo.Click += new System.EventHandler(this.Novo_Click);
            // 
            // Salvar
            // 
            this.Salvar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Salvar.Enabled = false;
            this.Salvar.Image = ((System.Drawing.Image)(resources.GetObject("Salvar.Image")));
            this.Salvar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Salvar.Name = "Salvar";
            this.Salvar.Size = new System.Drawing.Size(23, 22);
            this.Salvar.Text = "Salvar - F6";
            this.Salvar.Click += new System.EventHandler(this.Salvar_Click);
            // 
            // Editar
            // 
            this.Editar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Editar.Enabled = false;
            this.Editar.Image = ((System.Drawing.Image)(resources.GetObject("Editar.Image")));
            this.Editar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Editar.Name = "Editar";
            this.Editar.Size = new System.Drawing.Size(23, 22);
            this.Editar.Text = "Editar - F2";
            this.Editar.Click += new System.EventHandler(this.Editar_Click);
            // 
            // Excluir
            // 
            this.Excluir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Excluir.Enabled = false;
            this.Excluir.Image = ((System.Drawing.Image)(resources.GetObject("Excluir.Image")));
            this.Excluir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Excluir.Name = "Excluir";
            this.Excluir.Size = new System.Drawing.Size(23, 22);
            this.Excluir.Text = "Excluir - F3";
            this.Excluir.Click += new System.EventHandler(this.Excluir_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // Copiar
            // 
            this.Copiar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Copiar.Enabled = false;
            this.Copiar.Image = ((System.Drawing.Image)(resources.GetObject("Copiar.Image")));
            this.Copiar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Copiar.Name = "Copiar";
            this.Copiar.Size = new System.Drawing.Size(23, 22);
            this.Copiar.Text = "Copiar";
            this.Copiar.Click += new System.EventHandler(this.Copiar_Click);
            // 
            // Pesquisar
            // 
            this.Pesquisar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Pesquisar.Image = ((System.Drawing.Image)(resources.GetObject("Pesquisar.Image")));
            this.Pesquisar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Pesquisar.Name = "Pesquisar";
            this.Pesquisar.Size = new System.Drawing.Size(23, 22);
            this.Pesquisar.Text = "Pesquisar - F4";
            this.Pesquisar.Click += new System.EventHandler(this.Pesquisar_Click);
            // 
            // Sair
            // 
            this.Sair.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Sair.Image = ((System.Drawing.Image)(resources.GetObject("Sair.Image")));
            this.Sair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Sair.Name = "Sair";
            this.Sair.Size = new System.Drawing.Size(23, 22);
            this.Sair.Text = "Sair - Esc";
            this.Sair.Click += new System.EventHandler(this.Sair_Click);
            // 
            // GpUsuario
            // 
            this.GpUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GpUsuario.Controls.Add(this.label9);
            this.GpUsuario.Controls.Add(this.ComboAtivo);
            this.GpUsuario.Controls.Add(this.TxtConfirmaSenha);
            this.GpUsuario.Controls.Add(this.label6);
            this.GpUsuario.Controls.Add(this.TxtSenha);
            this.GpUsuario.Controls.Add(this.label5);
            this.GpUsuario.Controls.Add(this.TxtLogin);
            this.GpUsuario.Controls.Add(this.label4);
            this.GpUsuario.Controls.Add(this.TxtEmail);
            this.GpUsuario.Controls.Add(this.label3);
            this.GpUsuario.Controls.Add(this.TxtNome);
            this.GpUsuario.Controls.Add(this.label2);
            this.GpUsuario.Controls.Add(this.TxtId);
            this.GpUsuario.Controls.Add(this.label1);
            this.GpUsuario.Location = new System.Drawing.Point(12, 55);
            this.GpUsuario.Name = "GpUsuario";
            this.GpUsuario.Size = new System.Drawing.Size(636, 217);
            this.GpUsuario.TabIndex = 1;
            this.GpUsuario.TabStop = false;
            this.GpUsuario.Text = "Dados Usuário";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(81, 180);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 15);
            this.label9.TabIndex = 21;
            this.label9.Text = "Ativo";
            // 
            // ComboAtivo
            // 
            this.ComboAtivo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboAtivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboAtivo.FormattingEnabled = true;
            this.ComboAtivo.Location = new System.Drawing.Point(129, 177);
            this.ComboAtivo.Name = "ComboAtivo";
            this.ComboAtivo.Size = new System.Drawing.Size(100, 23);
            this.ComboAtivo.TabIndex = 10;
            // 
            // TxtConfirmaSenha
            // 
            this.TxtConfirmaSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtConfirmaSenha.Location = new System.Drawing.Point(129, 150);
            this.TxtConfirmaSenha.MaxLength = 100;
            this.TxtConfirmaSenha.Name = "TxtConfirmaSenha";
            this.TxtConfirmaSenha.Size = new System.Drawing.Size(209, 21);
            this.TxtConfirmaSenha.TabIndex = 5;
            this.TxtConfirmaSenha.UseSystemPasswordChar = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 153);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 15);
            this.label6.TabIndex = 12;
            this.label6.Text = "Confirma Senha";
            // 
            // TxtSenha
            // 
            this.TxtSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSenha.Location = new System.Drawing.Point(129, 124);
            this.TxtSenha.MaxLength = 100;
            this.TxtSenha.Name = "TxtSenha";
            this.TxtSenha.Size = new System.Drawing.Size(209, 21);
            this.TxtSenha.TabIndex = 4;
            this.TxtSenha.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(70, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "Senha";
            // 
            // TxtLogin
            // 
            this.TxtLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLogin.Location = new System.Drawing.Point(129, 98);
            this.TxtLogin.MaxLength = 100;
            this.TxtLogin.Name = "TxtLogin";
            this.TxtLogin.Size = new System.Drawing.Size(209, 21);
            this.TxtLogin.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(75, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "Login";
            // 
            // TxtEmail
            // 
            this.TxtEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Location = new System.Drawing.Point(129, 72);
            this.TxtEmail.MaxLength = 100;
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(361, 21);
            this.TxtEmail.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(74, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Email";
            // 
            // TxtNome
            // 
            this.TxtNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNome.Location = new System.Drawing.Point(129, 46);
            this.TxtNome.MaxLength = 100;
            this.TxtNome.Name = "TxtNome";
            this.TxtNome.Size = new System.Drawing.Size(361, 21);
            this.TxtNome.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(73, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nome";
            // 
            // TxtId
            // 
            this.TxtId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtId.Location = new System.Drawing.Point(129, 20);
            this.TxtId.Name = "TxtId";
            this.TxtId.ReadOnly = true;
            this.TxtId.Size = new System.Drawing.Size(100, 21);
            this.TxtId.TabIndex = 0;
            this.TxtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(72, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Código";
            // 
            // TxtLog
            // 
            this.TxtLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtLog.Enabled = false;
            this.TxtLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLog.Location = new System.Drawing.Point(0, 28);
            this.TxtLog.Name = "TxtLog";
            this.TxtLog.ReadOnly = true;
            this.TxtLog.Size = new System.Drawing.Size(660, 21);
            this.TxtLog.TabIndex = 4;
            // 
            // FormUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 284);
            this.Controls.Add(this.TxtLog);
            this.Controls.Add(this.GpUsuario);
            this.Controls.Add(this.MenuForm);
            this.KeyPreview = true;
            this.Name = "FormUsuario";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Usuário";
            this.Load += new System.EventHandler(this.FormUsuario_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormUsuario_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.Errors)).EndInit();
            this.MenuForm.ResumeLayout(false);
            this.MenuForm.PerformLayout();
            this.GpUsuario.ResumeLayout(false);
            this.GpUsuario.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ErrorProvider Errors;
        private System.Windows.Forms.GroupBox GpUsuario;
        private System.Windows.Forms.ToolStripButton Novo;
        private System.Windows.Forms.ToolStripButton Salvar;
        private System.Windows.Forms.ToolStripButton Editar;
        private System.Windows.Forms.ToolStripButton Excluir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton Copiar;
        private System.Windows.Forms.ToolStripButton Pesquisar;
        private System.Windows.Forms.ToolStripButton Sair;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtId;
        private System.Windows.Forms.TextBox TxtSenha;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtLogin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtConfirmaSenha;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox ComboAtivo;
        private System.Windows.Forms.TextBox TxtLog;
        public System.Windows.Forms.ToolStrip MenuForm;
    }
}