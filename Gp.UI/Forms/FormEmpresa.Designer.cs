﻿namespace Gp
{
    partial class FormEmpresa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEmpresa));
            this.TxtLog = new System.Windows.Forms.TextBox();
            this.GpCidade = new System.Windows.Forms.GroupBox();
            this.TxtCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.TxtObservacao = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtDDDComercial = new Gp.TextBoxForceNumber();
            this.TxtFoneComercial = new Gp.TextBoxForceNumber();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtDDDCelular = new Gp.TextBoxForceNumber();
            this.TxtFoneCelular = new Gp.TextBoxForceNumber();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtDDDResidencial = new Gp.TextBoxForceNumber();
            this.TxtFoneResidencial = new Gp.TextBoxForceNumber();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TxtCEP = new System.Windows.Forms.MaskedTextBox();
            this.TxtCidadeId = new Gp.TextBoxForceNumber();
            this.LblCidadeDescricao = new System.Windows.Forms.Label();
            this.BtnCidade = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtBairro = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtComplemento = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtNumero = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtLogradouro = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtInscricaoEstadual = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtNomeFantasia = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.MenuForm = new System.Windows.Forms.ToolStrip();
            this.Novo = new System.Windows.Forms.ToolStripButton();
            this.Salvar = new System.Windows.Forms.ToolStripButton();
            this.Editar = new System.Windows.Forms.ToolStripButton();
            this.Excluir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.Copiar = new System.Windows.Forms.ToolStripButton();
            this.Pesquisar = new System.Windows.Forms.ToolStripButton();
            this.Sair = new System.Windows.Forms.ToolStripButton();
            this.Errors = new System.Windows.Forms.ErrorProvider(this.components);
            this.GpCidade.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.MenuForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Errors)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtLog
            // 
            this.TxtLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtLog.Enabled = false;
            this.TxtLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLog.Location = new System.Drawing.Point(0, 28);
            this.TxtLog.Name = "TxtLog";
            this.TxtLog.ReadOnly = true;
            this.TxtLog.Size = new System.Drawing.Size(933, 21);
            this.TxtLog.TabIndex = 16;
            // 
            // GpCidade
            // 
            this.GpCidade.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GpCidade.Controls.Add(this.TxtCNPJ);
            this.GpCidade.Controls.Add(this.TxtObservacao);
            this.GpCidade.Controls.Add(this.label12);
            this.GpCidade.Controls.Add(this.groupBox1);
            this.GpCidade.Controls.Add(this.groupBox2);
            this.GpCidade.Controls.Add(this.TxtInscricaoEstadual);
            this.GpCidade.Controls.Add(this.label5);
            this.GpCidade.Controls.Add(this.label4);
            this.GpCidade.Controls.Add(this.TxtNomeFantasia);
            this.GpCidade.Controls.Add(this.label3);
            this.GpCidade.Controls.Add(this.TxtNome);
            this.GpCidade.Controls.Add(this.label2);
            this.GpCidade.Controls.Add(this.TxtId);
            this.GpCidade.Controls.Add(this.label1);
            this.GpCidade.Location = new System.Drawing.Point(12, 55);
            this.GpCidade.Name = "GpCidade";
            this.GpCidade.Size = new System.Drawing.Size(909, 879);
            this.GpCidade.TabIndex = 15;
            this.GpCidade.TabStop = false;
            this.GpCidade.Text = "Dados da Empresa";
            // 
            // TxtCNPJ
            // 
            this.TxtCNPJ.Location = new System.Drawing.Point(129, 103);
            this.TxtCNPJ.Mask = "00,000,000/0000-00";
            this.TxtCNPJ.Name = "TxtCNPJ";
            this.TxtCNPJ.Size = new System.Drawing.Size(199, 20);
            this.TxtCNPJ.TabIndex = 3;
            this.TxtCNPJ.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TxtObservacao
            // 
            this.TxtObservacao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtObservacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtObservacao.Location = new System.Drawing.Point(129, 463);
            this.TxtObservacao.MaxLength = 1500;
            this.TxtObservacao.Multiline = true;
            this.TxtObservacao.Name = "TxtObservacao";
            this.TxtObservacao.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TxtObservacao.Size = new System.Drawing.Size(747, 97);
            this.TxtObservacao.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(34, 466);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 15);
            this.label12.TabIndex = 48;
            this.label12.Text = "Observações";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.TxtEmail);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.TxtDDDComercial);
            this.groupBox1.Controls.Add(this.TxtFoneComercial);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.TxtDDDCelular);
            this.groupBox1.Controls.Add(this.TxtFoneCelular);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.TxtDDDResidencial);
            this.groupBox1.Controls.Add(this.TxtFoneResidencial);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Location = new System.Drawing.Point(6, 319);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(896, 138);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Contatos";
            // 
            // TxtEmail
            // 
            this.TxtEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Location = new System.Drawing.Point(123, 97);
            this.TxtEmail.MaxLength = 100;
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(321, 21);
            this.TxtEmail.TabIndex = 18;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(69, 100);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 15);
            this.label19.TabIndex = 47;
            this.label19.Text = "E-mail";
            // 
            // TxtDDDComercial
            // 
            this.TxtDDDComercial.Location = new System.Drawing.Point(123, 71);
            this.TxtDDDComercial.MaxLength = 2;
            this.TxtDDDComercial.Name = "TxtDDDComercial";
            this.TxtDDDComercial.Size = new System.Drawing.Size(41, 20);
            this.TxtDDDComercial.TabIndex = 16;
            this.TxtDDDComercial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxtFoneComercial
            // 
            this.TxtFoneComercial.Location = new System.Drawing.Point(165, 71);
            this.TxtFoneComercial.MaxLength = 9;
            this.TxtFoneComercial.Name = "TxtFoneComercial";
            this.TxtFoneComercial.Size = new System.Drawing.Size(157, 20);
            this.TxtFoneComercial.TabIndex = 17;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(6, 72);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(106, 15);
            this.label18.TabIndex = 45;
            this.label18.Text = "Tel. Administrativo";
            // 
            // TxtDDDCelular
            // 
            this.TxtDDDCelular.Location = new System.Drawing.Point(123, 45);
            this.TxtDDDCelular.MaxLength = 2;
            this.TxtDDDCelular.Name = "TxtDDDCelular";
            this.TxtDDDCelular.Size = new System.Drawing.Size(41, 20);
            this.TxtDDDCelular.TabIndex = 14;
            this.TxtDDDCelular.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxtFoneCelular
            // 
            this.TxtFoneCelular.Location = new System.Drawing.Point(165, 45);
            this.TxtFoneCelular.MaxLength = 9;
            this.TxtFoneCelular.Name = "TxtFoneCelular";
            this.TxtFoneCelular.Size = new System.Drawing.Size(157, 20);
            this.TxtFoneCelular.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(24, 46);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 15);
            this.label17.TabIndex = 42;
            this.label17.Text = "Tel. Financeiro";
            // 
            // TxtDDDResidencial
            // 
            this.TxtDDDResidencial.Location = new System.Drawing.Point(123, 19);
            this.TxtDDDResidencial.MaxLength = 2;
            this.TxtDDDResidencial.Name = "TxtDDDResidencial";
            this.TxtDDDResidencial.Size = new System.Drawing.Size(41, 20);
            this.TxtDDDResidencial.TabIndex = 12;
            this.TxtDDDResidencial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxtFoneResidencial
            // 
            this.TxtFoneResidencial.Location = new System.Drawing.Point(165, 19);
            this.TxtFoneResidencial.MaxLength = 9;
            this.TxtFoneResidencial.Name = "TxtFoneResidencial";
            this.TxtFoneResidencial.Size = new System.Drawing.Size(157, 20);
            this.TxtFoneResidencial.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(34, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 15);
            this.label14.TabIndex = 39;
            this.label14.Text = "Tel. Principal";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.TxtCEP);
            this.groupBox2.Controls.Add(this.TxtCidadeId);
            this.groupBox2.Controls.Add(this.LblCidadeDescricao);
            this.groupBox2.Controls.Add(this.BtnCidade);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.TxtBairro);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.TxtComplemento);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.TxtNumero);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.TxtLogradouro);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(6, 154);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(896, 159);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Endereço";
            // 
            // TxtCEP
            // 
            this.TxtCEP.Location = new System.Drawing.Point(123, 102);
            this.TxtCEP.Mask = "00000-000";
            this.TxtCEP.Name = "TxtCEP";
            this.TxtCEP.Size = new System.Drawing.Size(100, 20);
            this.TxtCEP.TabIndex = 10;
            this.TxtCEP.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.TxtCEP.Leave += new System.EventHandler(this.TxtCEP_Leave);
            // 
            // TxtCidadeId
            // 
            this.TxtCidadeId.Location = new System.Drawing.Point(123, 127);
            this.TxtCidadeId.Name = "TxtCidadeId";
            this.TxtCidadeId.Size = new System.Drawing.Size(100, 20);
            this.TxtCidadeId.TabIndex = 10;
            this.TxtCidadeId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtCidadeId.TextChanged += new System.EventHandler(this.TxtCidadeId_TextChanged);
            this.TxtCidadeId.Leave += new System.EventHandler(this.TxtCidadeId_Leave);
            // 
            // LblCidadeDescricao
            // 
            this.LblCidadeDescricao.AutoSize = true;
            this.LblCidadeDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCidadeDescricao.Location = new System.Drawing.Point(259, 130);
            this.LblCidadeDescricao.Name = "LblCidadeDescricao";
            this.LblCidadeDescricao.Size = new System.Drawing.Size(52, 15);
            this.LblCidadeDescricao.TabIndex = 46;
            this.LblCidadeDescricao.Text = "Cidade";
            // 
            // BtnCidade
            // 
            this.BtnCidade.Location = new System.Drawing.Point(224, 127);
            this.BtnCidade.Name = "BtnCidade";
            this.BtnCidade.Size = new System.Drawing.Size(29, 21);
            this.BtnCidade.TabIndex = 11;
            this.BtnCidade.Text = "...";
            this.BtnCidade.UseVisualStyleBackColor = true;
            this.BtnCidade.Click += new System.EventHandler(this.BtnCiade_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(66, 130);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 15);
            this.label11.TabIndex = 45;
            this.label11.Text = "Cidade";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(81, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 15);
            this.label10.TabIndex = 42;
            this.label10.Text = "CEP";
            // 
            // TxtBairro
            // 
            this.TxtBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBairro.Location = new System.Drawing.Point(123, 73);
            this.TxtBairro.MaxLength = 500;
            this.TxtBairro.Name = "TxtBairro";
            this.TxtBairro.Size = new System.Drawing.Size(304, 21);
            this.TxtBairro.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(72, 76);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 15);
            this.label9.TabIndex = 40;
            this.label9.Text = "Bairro";
            // 
            // TxtComplemento
            // 
            this.TxtComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtComplemento.Location = new System.Drawing.Point(123, 46);
            this.TxtComplemento.MaxLength = 500;
            this.TxtComplemento.Name = "TxtComplemento";
            this.TxtComplemento.Size = new System.Drawing.Size(304, 21);
            this.TxtComplemento.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(27, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 15);
            this.label8.TabIndex = 38;
            this.label8.Text = "Complemento";
            // 
            // TxtNumero
            // 
            this.TxtNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNumero.Location = new System.Drawing.Point(780, 19);
            this.TxtNumero.MaxLength = 500;
            this.TxtNumero.Name = "TxtNumero";
            this.TxtNumero.Size = new System.Drawing.Size(90, 21);
            this.TxtNumero.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(722, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 15);
            this.label7.TabIndex = 36;
            this.label7.Text = "Número";
            // 
            // TxtLogradouro
            // 
            this.TxtLogradouro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLogradouro.Location = new System.Drawing.Point(123, 19);
            this.TxtLogradouro.MaxLength = 500;
            this.TxtLogradouro.Name = "TxtLogradouro";
            this.TxtLogradouro.Size = new System.Drawing.Size(557, 21);
            this.TxtLogradouro.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(41, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 15);
            this.label6.TabIndex = 34;
            this.label6.Text = "Logradouro";
            // 
            // TxtInscricaoEstadual
            // 
            this.TxtInscricaoEstadual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInscricaoEstadual.Location = new System.Drawing.Point(129, 127);
            this.TxtInscricaoEstadual.MaxLength = 500;
            this.TxtInscricaoEstadual.Name = "TxtInscricaoEstadual";
            this.TxtInscricaoEstadual.Size = new System.Drawing.Size(199, 21);
            this.TxtInscricaoEstadual.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(35, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "Insc. Estadual";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(76, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "CNPJ";
            // 
            // TxtNomeFantasia
            // 
            this.TxtNomeFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNomeFantasia.Location = new System.Drawing.Point(129, 73);
            this.TxtNomeFantasia.MaxLength = 500;
            this.TxtNomeFantasia.Name = "TxtNomeFantasia";
            this.TxtNomeFantasia.Size = new System.Drawing.Size(557, 21);
            this.TxtNomeFantasia.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Nome Fantasia";
            // 
            // TxtNome
            // 
            this.TxtNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNome.Location = new System.Drawing.Point(129, 46);
            this.TxtNome.MaxLength = 500;
            this.TxtNome.Name = "TxtNome";
            this.TxtNome.Size = new System.Drawing.Size(557, 21);
            this.TxtNome.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(26, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Razão Social";
            // 
            // TxtId
            // 
            this.TxtId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtId.Location = new System.Drawing.Point(129, 20);
            this.TxtId.Name = "TxtId";
            this.TxtId.ReadOnly = true;
            this.TxtId.Size = new System.Drawing.Size(100, 21);
            this.TxtId.TabIndex = 0;
            this.TxtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(72, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Código";
            // 
            // MenuForm
            // 
            this.MenuForm.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.MenuForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Novo,
            this.Salvar,
            this.Editar,
            this.Excluir,
            this.toolStripSeparator,
            this.Copiar,
            this.Pesquisar,
            this.Sair});
            this.MenuForm.Location = new System.Drawing.Point(0, 0);
            this.MenuForm.Name = "MenuForm";
            this.MenuForm.Size = new System.Drawing.Size(967, 25);
            this.MenuForm.TabIndex = 14;
            this.MenuForm.Text = "toolStrip1";
            // 
            // Novo
            // 
            this.Novo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Novo.Image = ((System.Drawing.Image)(resources.GetObject("Novo.Image")));
            this.Novo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Novo.Name = "Novo";
            this.Novo.Size = new System.Drawing.Size(23, 22);
            this.Novo.Text = "Novo - F1";
            this.Novo.Click += new System.EventHandler(this.Novo_Click);
            // 
            // Salvar
            // 
            this.Salvar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Salvar.Enabled = false;
            this.Salvar.Image = ((System.Drawing.Image)(resources.GetObject("Salvar.Image")));
            this.Salvar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Salvar.Name = "Salvar";
            this.Salvar.Size = new System.Drawing.Size(23, 22);
            this.Salvar.Text = "Salvar - F6";
            this.Salvar.Click += new System.EventHandler(this.Salvar_Click);
            // 
            // Editar
            // 
            this.Editar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Editar.Enabled = false;
            this.Editar.Image = ((System.Drawing.Image)(resources.GetObject("Editar.Image")));
            this.Editar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Editar.Name = "Editar";
            this.Editar.Size = new System.Drawing.Size(23, 22);
            this.Editar.Text = "Editar - F2";
            this.Editar.Click += new System.EventHandler(this.Editar_Click);
            // 
            // Excluir
            // 
            this.Excluir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Excluir.Enabled = false;
            this.Excluir.Image = ((System.Drawing.Image)(resources.GetObject("Excluir.Image")));
            this.Excluir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Excluir.Name = "Excluir";
            this.Excluir.Size = new System.Drawing.Size(23, 22);
            this.Excluir.Text = "Excluir - F3";
            this.Excluir.Click += new System.EventHandler(this.Excluir_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // Copiar
            // 
            this.Copiar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Copiar.Enabled = false;
            this.Copiar.Image = ((System.Drawing.Image)(resources.GetObject("Copiar.Image")));
            this.Copiar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Copiar.Name = "Copiar";
            this.Copiar.Size = new System.Drawing.Size(23, 22);
            this.Copiar.Text = "&Copiar";
            this.Copiar.Click += new System.EventHandler(this.Copiar_Click);
            // 
            // Pesquisar
            // 
            this.Pesquisar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Pesquisar.Image = ((System.Drawing.Image)(resources.GetObject("Pesquisar.Image")));
            this.Pesquisar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Pesquisar.Name = "Pesquisar";
            this.Pesquisar.Size = new System.Drawing.Size(23, 22);
            this.Pesquisar.Text = "Pesquisar - F4";
            this.Pesquisar.Click += new System.EventHandler(this.Pesquisar_Click);
            // 
            // Sair
            // 
            this.Sair.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Sair.Image = ((System.Drawing.Image)(resources.GetObject("Sair.Image")));
            this.Sair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Sair.Name = "Sair";
            this.Sair.Size = new System.Drawing.Size(23, 22);
            this.Sair.Text = "Sair - Esc";
            this.Sair.Click += new System.EventHandler(this.Sair_Click);
            // 
            // Errors
            // 
            this.Errors.ContainerControl = this;
            this.Errors.RightToLeft = true;
            // 
            // FormEmpresa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(0, 788);
            this.ClientSize = new System.Drawing.Size(984, 638);
            this.Controls.Add(this.TxtLog);
            this.Controls.Add(this.GpCidade);
            this.Controls.Add(this.MenuForm);
            this.KeyPreview = true;
            this.Name = "FormEmpresa";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Cadastro de Empresas de Saúde";
            this.Load += new System.EventHandler(this.FormEmpresa_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormCidade_KeyDown);
            this.GpCidade.ResumeLayout(false);
            this.GpCidade.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.MenuForm.ResumeLayout(false);
            this.MenuForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Errors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtLog;
        private System.Windows.Forms.GroupBox GpCidade;
        private System.Windows.Forms.TextBox TxtNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtId;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ToolStrip MenuForm;
        private System.Windows.Forms.ToolStripButton Novo;
        private System.Windows.Forms.ToolStripButton Salvar;
        private System.Windows.Forms.ToolStripButton Editar;
        private System.Windows.Forms.ToolStripButton Excluir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton Copiar;
        private System.Windows.Forms.ToolStripButton Pesquisar;
        private System.Windows.Forms.ToolStripButton Sair;
        private System.Windows.Forms.TextBox TxtInscricaoEstadual;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtNomeFantasia;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox TxtObservacao;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label19;
        private TextBoxForceNumber TxtDDDComercial;
        private TextBoxForceNumber TxtFoneComercial;
        private System.Windows.Forms.Label label18;
        private TextBoxForceNumber TxtDDDCelular;
        private TextBoxForceNumber TxtFoneCelular;
        private System.Windows.Forms.Label label17;
        private TextBoxForceNumber TxtDDDResidencial;
        private TextBoxForceNumber TxtFoneResidencial;
        private System.Windows.Forms.Label label14;
        private TextBoxForceNumber TxtCidadeId;
        private System.Windows.Forms.Label LblCidadeDescricao;
        private System.Windows.Forms.Button BtnCidade;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TxtBairro;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtComplemento;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtNumero;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtLogradouro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.ErrorProvider Errors;
        private System.Windows.Forms.MaskedTextBox TxtCNPJ;
        private System.Windows.Forms.MaskedTextBox TxtCEP;
    }
}