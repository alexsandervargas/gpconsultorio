﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gp.App.DTO;
using Gp.App.Services;

namespace Gp
{
    public partial class FormPlano : Form
    {
        FormsService Service;
        public UsuarioDTO UsuarioLogado { get; set; }

        public FormPlano()
        {
            InitializeComponent();

            //Instancia FormService
            Service = new FormsService();

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.DisableControls(this);

            this.WindowState = FormWindowState.Maximized;
            this.Refresh();

            this.ComboAtivo.Items.Add("Não");
            this.ComboAtivo.Items.Add("Sim");

            this.ComboCooparticipacao.Items.Add("Não");
            this.ComboCooparticipacao.Items.Add("Sim");

            this.ComboAtivo.SelectedIndex = 1;
            this.ComboCooparticipacao.SelectedIndex = 0;
        }

        //Gerencia as teclas de atalho
        private void FormPlano_KeyDown(object sender, KeyEventArgs e)
        {
            Service.Keys(keyEvent: e, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar, BtnPesquisar: this.Pesquisar,
                BtnSair: this.Sair, BtnExcluir: this.Excluir);

        }

        //Inicialização do form com ajustes de botões
        private void FormPlano_Load(object sender, EventArgs e)
        {
            //Ajusta os botoes
            Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

        }

        //Preenche o formulário com os dados
        public void LoadForm()
        {
            try
            {
                var plano = new PlanoService().GetById(Convert.ToInt32(this.TxtId.Text));


                this.TxtDescricao.Text = plano.Descricao;
                this.TxtEmpresaId.Text = plano.PessoaJuridicaId.ToString();
                this.TxtObservacao.Text = plano.Observacao;
                this.ComboAtivo.SelectedIndex = Convert.ToInt32(plano.Ativo);
                this.ComboCooparticipacao.SelectedIndex = Convert.ToInt32(plano.Cooparticipacao);

                LoadEmpresa();

                ValidateForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Carrega o pais nos seus devidos campos do form
        public void LoadEmpresa()
        {
            var empresa = new PessoaJuridicaService().GetById(Convert.ToInt32(this.TxtEmpresaId.Text));
            if (empresa != null)
            {
                this.TxtEmpresaId.Text = Convert.ToString(empresa.Id);
                this.LblEmpresaDescricao.Text = empresa.Nome;
                this.Errors.SetError(TxtEmpresaId, "");
            }
            else
            {
                this.LblEmpresaDescricao.Text = "";
                this.Errors.SetError(TxtEmpresaId, "Código de Empresa Inválido.");
            }


        }


        #region CamposForm
        private void TxtEmpresaId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtEmpresaId, "");
        }

        private void TxtEmpresaId_Leave(object sender, EventArgs e)
        {
            if (this.TxtEmpresaId.Text != string.Empty)
            {
                LoadEmpresa();
            }
        }
        #endregion

        #region ValidationsForm
        //Validações do Formulário Usuário
        public bool ValidateForm()
        {
            bool Validate = true;

            try
            {
                //Veryfy Text Nome
                if (this.TxtDescricao.Text == string.Empty)
                {
                    this.Errors.SetError(TxtDescricao, "Informe a Descrição do Plano");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtDescricao, "");
                }


                //Veryfy Empresa
                if (this.TxtEmpresaId.Text == string.Empty || this.LblEmpresaDescricao.Text == string.Empty)
                {
                    this.Errors.SetError(TxtEmpresaId, "Informe a Empresa");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtEmpresaId, "");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return Validate;

        }

        #endregion

        #region ButtonsMenu

        //Habilita nova inserção de usuário no formulário
        private void Novo_Click(object sender, EventArgs e)
        {
            //Verifica se formulário esta preenchido e em edição de algum estado 
            //Caso esteja questiona o usuário se deseja salvar as alterações realizadas ou não
            if (this.TxtId.Text != "" && GpPlano.Enabled == true)
            {
                if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Salvar.PerformClick();
                }
            }

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            this.ComboAtivo.SelectedIndex = 1;
            this.ComboCooparticipacao.SelectedIndex = 0;

            //Desabilita os Campos do Formulário
            Service.EnableControls(this);

            //Desabilita os Botoes do form
            Service.ButtonsNewInsert(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);


        }

        //Salva dados preenchido no formulário
        private void Salvar_Click(object sender, EventArgs e)
        {

            if (ValidateForm())
            {
                try
                {

                    PlanoDTO plano = new PlanoDTO
                    {
                        Descricao = this.TxtDescricao.Text,
                        Ativo = Convert.ToBoolean(this.ComboAtivo.SelectedIndex),
                        Cooparticipacao = Convert.ToBoolean(this.ComboCooparticipacao.SelectedIndex),
                        PessoaJuridicaId = Convert.ToInt32(this.TxtEmpresaId.Text),
                        Observacao = this.TxtObservacao.Text,
                        
                    };


                    if (this.TxtId.Text != "")
                    {
                        plano.Id = Convert.ToInt32(this.TxtId.Text);
                    }



                    if (plano.Id != 0)
                    {
                        if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                    }

                    new PlanoService().Save(plano);

                    if (plano.Id != 0)
                    {
                        this.TxtId.Text = Convert.ToString(plano.Id);

                        Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                        MessageBox.Show("Plano [" + this.TxtDescricao.Text + "] salvo com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        //Habilita controle para edição
        private void Editar_Click(object sender, EventArgs e)
        {
            Service.EnableControls(this);

            Service.AdjutsButtonsToEdition(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

        }

        //Exclui 
        private void Excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Confirma Exclusão?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        new PlanoService().Delete(Convert.ToInt32(this.TxtId.Text));
                        this.Service.ClearForm(this);
                        this.Service.DisableControls(this);

                        this.ComboAtivo.SelectedIndex = 1;
                        this.ComboCooparticipacao.SelectedIndex = 0;

                        Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

                        MessageBox.Show("Exclusão Confirmada!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        //Realiza cópia dos dados em tela
        private void Copiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Deseja Copiar este Plano ?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var plano = new PlanoService().GetById(Convert.ToInt32(this.TxtId.Text));
                        plano.Descricao += " Cópia";
                        plano.Id = 0;
                        plano.PessoaJuridicaId = Convert.ToInt32(this.TxtEmpresaId.Text);

                        new PlanoService().Save(plano);

                        this.TxtId.Text = plano.Id.ToString();

                        this.Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

                        LoadForm();

                        MessageBox.Show("Cópia efetuada com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Abre formulário para pesquisa
        private void Pesquisar_Click(object sender, EventArgs e)
        {
            var form = new FormPlanoBusca();

            form.ShowDialog();

            var plano = form.Plano;

            if (plano != null)
            {

                this.Service.ClearForm(this);
                this.Service.DisableControls(this);

                this.ComboAtivo.SelectedIndex = 1;
                this.ComboCooparticipacao.SelectedIndex = 0;

                Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                this.TxtId.Text = plano.Id.ToString();

                LoadForm();

            }
        }

        //Fecha Formulário
        private void Sair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Botão de pesquisa de empresa
        private void BtnEmpresa_Click(object sender, EventArgs e)
        {
            var form = new FormEmpresaBusca();

            form.ShowDialog();

            var empresa = form.Empresa;

            if (empresa != null)
            {
                this.TxtEmpresaId.Text = empresa.Id.ToString();

                LoadEmpresa();

            }
        }

        #endregion
    }
}
