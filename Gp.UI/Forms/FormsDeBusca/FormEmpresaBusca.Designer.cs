﻿namespace Gp
{
    partial class FormEmpresaBusca
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEmpresaBusca));
            this.MenuForm = new System.Windows.Forms.ToolStrip();
            this.Selecionar = new System.Windows.Forms.ToolStripButton();
            this.Pesquisar = new System.Windows.Forms.ToolStripButton();
            this.Sair = new System.Windows.Forms.ToolStripButton();
            this.DtgViewResultado = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomeFantasia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CNPJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Bairro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtTexto = new System.Windows.Forms.TextBox();
            this.ComboCampo = new System.Windows.Forms.ComboBox();
            this.MenuForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgViewResultado)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuForm
            // 
            this.MenuForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Selecionar,
            this.Pesquisar,
            this.Sair});
            this.MenuForm.Location = new System.Drawing.Point(0, 0);
            this.MenuForm.Name = "MenuForm";
            this.MenuForm.Size = new System.Drawing.Size(921, 25);
            this.MenuForm.TabIndex = 32;
            this.MenuForm.Text = "toolStrip1";
            // 
            // Selecionar
            // 
            this.Selecionar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Selecionar.Image = ((System.Drawing.Image)(resources.GetObject("Selecionar.Image")));
            this.Selecionar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Selecionar.Name = "Selecionar";
            this.Selecionar.Size = new System.Drawing.Size(23, 22);
            this.Selecionar.Text = "Selecionar";
            this.Selecionar.Click += new System.EventHandler(this.Selecionar_Click);
            // 
            // Pesquisar
            // 
            this.Pesquisar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Pesquisar.Image = ((System.Drawing.Image)(resources.GetObject("Pesquisar.Image")));
            this.Pesquisar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Pesquisar.Name = "Pesquisar";
            this.Pesquisar.Size = new System.Drawing.Size(23, 22);
            this.Pesquisar.Text = "&Pesquisar";
            this.Pesquisar.Click += new System.EventHandler(this.Pesquisar_Click);
            // 
            // Sair
            // 
            this.Sair.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Sair.Image = ((System.Drawing.Image)(resources.GetObject("Sair.Image")));
            this.Sair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Sair.Name = "Sair";
            this.Sair.Size = new System.Drawing.Size(23, 22);
            this.Sair.Text = "Sair";
            this.Sair.Click += new System.EventHandler(this.Sair_Click);
            // 
            // DtgViewResultado
            // 
            this.DtgViewResultado.AllowUserToAddRows = false;
            this.DtgViewResultado.AllowUserToDeleteRows = false;
            this.DtgViewResultado.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DtgViewResultado.BackgroundColor = System.Drawing.Color.White;
            this.DtgViewResultado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgViewResultado.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Nome,
            this.NomeFantasia,
            this.CNPJ,
            this.Estado,
            this.Bairro});
            this.DtgViewResultado.Location = new System.Drawing.Point(0, 57);
            this.DtgViewResultado.MultiSelect = false;
            this.DtgViewResultado.Name = "DtgViewResultado";
            this.DtgViewResultado.ReadOnly = true;
            this.DtgViewResultado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DtgViewResultado.Size = new System.Drawing.Size(921, 457);
            this.DtgViewResultado.TabIndex = 31;
            this.DtgViewResultado.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgViewResultado_CellDoubleClick);
            this.DtgViewResultado.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DtgViewResultado_KeyDown);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Código";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Width = 80;
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "Nome";
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            // 
            // NomeFantasia
            // 
            this.NomeFantasia.DataPropertyName = "NomeFantasia";
            this.NomeFantasia.HeaderText = "NomeFantasia";
            this.NomeFantasia.Name = "NomeFantasia";
            this.NomeFantasia.ReadOnly = true;
            // 
            // CNPJ
            // 
            this.CNPJ.DataPropertyName = "CNPJ";
            this.CNPJ.HeaderText = "CNPJ";
            this.CNPJ.Name = "CNPJ";
            this.CNPJ.ReadOnly = true;
            // 
            // Estado
            // 
            this.Estado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Estado.DataPropertyName = "Cidade";
            this.Estado.HeaderText = "Cidade";
            this.Estado.Name = "Estado";
            this.Estado.ReadOnly = true;
            this.Estado.Width = 200;
            // 
            // Bairro
            // 
            this.Bairro.DataPropertyName = "Bairro";
            this.Bairro.HeaderText = "Bairro";
            this.Bairro.Name = "Bairro";
            this.Bairro.ReadOnly = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(244, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 15);
            this.label3.TabIndex = 30;
            this.label3.Text = "Texto";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 29;
            this.label1.Text = "Campo";
            // 
            // TxtTexto
            // 
            this.TxtTexto.Location = new System.Drawing.Point(292, 29);
            this.TxtTexto.Name = "TxtTexto";
            this.TxtTexto.Size = new System.Drawing.Size(533, 20);
            this.TxtTexto.TabIndex = 27;
            // 
            // ComboCampo
            // 
            this.ComboCampo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboCampo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboCampo.FormattingEnabled = true;
            this.ComboCampo.Location = new System.Drawing.Point(73, 28);
            this.ComboCampo.Name = "ComboCampo";
            this.ComboCampo.Size = new System.Drawing.Size(148, 23);
            this.ComboCampo.Sorted = true;
            this.ComboCampo.TabIndex = 28;
            this.ComboCampo.SelectedIndexChanged += new System.EventHandler(this.ComboCampo_SelectedIndexChanged);
            // 
            // FormEmpresaBusca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 526);
            this.Controls.Add(this.MenuForm);
            this.Controls.Add(this.DtgViewResultado);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtTexto);
            this.Controls.Add(this.ComboCampo);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormEmpresaBusca";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pesquisa de Empresa";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormEmpresaBusca_KeyDown);
            this.MenuForm.ResumeLayout(false);
            this.MenuForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgViewResultado)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ToolStrip MenuForm;
        private System.Windows.Forms.ToolStripButton Selecionar;
        private System.Windows.Forms.ToolStripButton Pesquisar;
        private System.Windows.Forms.ToolStripButton Sair;
        private System.Windows.Forms.DataGridView DtgViewResultado;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtTexto;
        private System.Windows.Forms.ComboBox ComboCampo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomeFantasia;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNPJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bairro;
    }
}