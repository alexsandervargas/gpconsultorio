﻿using Gp.App.DTO;
using Gp.App.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gp
{
    public partial class FormPacienteBusca : Form
    {
        public PacienteDTO Paciente { get; set; }
        private FormsService Service { get; set; }

        public FormPacienteBusca()
        {
            InitializeComponent();
            CarregaCombos();
            this.Service = new FormsService();
            this.TxtTexto.Focus();
        }

        //Carrega combobox
        private void CarregaCombos()
        {
            this.ComboMetodo.Items.Add("Contém");
            this.ComboMetodo.Items.Add("Inicia com");
            this.ComboMetodo.Items.Add("Termina com");

            this.ComboCampo.Items.Add("Nome");
            this.ComboCampo.Items.Add("CPF");
            this.ComboCampo.Items.Add("RG");
            this.ComboCampo.Items.Add("Celular");
            this.ComboCampo.Items.Add("Ficha");

            this.ComboCampo.SelectedIndex = 0;
            this.ComboMetodo.SelectedIndex = 0;
        }

        //Carrega informações do profissional selecionado
        private void SelectPaciente()
        {
            if (DtgViewResultado.Rows.Count > 0)
            {
                Paciente = new PacienteService().GetById(Convert.ToInt32(this.DtgViewResultado[0, DtgViewResultado.CurrentCell.RowIndex].Value.ToString()));

                this.Close();
            }
        }

        //Apaga texto de pesquisa quando o campo de pesquisa é alterado
        private void ComboCampo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.TxtTexto.Text = "";
        }

        //Fecha o formulário
        private void Sair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Carrega informações do estado civil selecionado
        private void Selecionar_Click(object sender, EventArgs e)
        {
            SelectPaciente();
        }

        //Carrega informações do estado civil selecionado
        private void DtgViewResultado_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectPaciente();
        }

        //Carrega informações do estado civil selecionado
        private void DtgViewResultado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                SelectPaciente();
            }
        }

        //Teclas de Atalho
        private void FormPacienteBusca_KeyDown(object sender, KeyEventArgs e)
        {
            if ((this.TxtTexto.ContainsFocus) && (e.KeyData == Keys.Enter))
            {
                Pesquisar.PerformClick();
            }
            else
            {
                Service.Keys(keyEvent: e, BtnPesquisar: this.Pesquisar, BtnSair: this.Sair);
            }

        }

        //Executa a pesquisa
        private void Pesquisar_Click(object sender, EventArgs e)
        {
            if (this.TxtTexto.Text != string.Empty)
            {
                this.DtgViewResultado.DataSource = null;
                this.DtgViewResultado.Refresh();
                this.DtgViewResultado.Rows.Clear();
                this.DtgViewResultado.AutoGenerateColumns = false;
                

                IList<PacienteDTO> Pesquisa = null;


                Pesquisa = new PacienteService().Search(this.ComboCampo.Text, this.TxtTexto.Text, this.ComboMetodo.Text).OrderBy(x=>x.Nome).ToList();


                if (Pesquisa.Count > 0)
                {
                    var Pacientes = Pesquisa.Select(paciente => new
                    {
                        Id = paciente.Id,
                        Nome = paciente.Nome,
                        CPF = paciente.CPF,
                        Cidade = paciente.CidadeNome,
                        Bairro = paciente.Bairro,
                        Ficha = paciente.Ficha

                    }).ToList();

                    DtgViewResultado.DataSource = Pacientes;
                    DtgViewResultado.Refresh();
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.\nRevise seus critérios de pesquisa.", "GP", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }


            }
            else
            {
                MessageBox.Show("Informe algum texto para realizar a consulta!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }
    }
}
