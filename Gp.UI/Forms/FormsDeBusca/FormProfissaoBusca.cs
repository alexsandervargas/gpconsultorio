﻿using Gp.App.DTO;
using Gp.App.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gp
{
    public partial class FormProfissaoBusca : Form
    {
        public ProfissaoDTO profissao { get; set; }
        private FormsService Service { get; set; }


        public FormProfissaoBusca()
        {
            InitializeComponent();
            CarregaCombos();
            this.Service = new FormsService();
            this.TxtTexto.Focus();
        }

        //Carrega combobox
        private void CarregaCombos()
        {
            this.ComboCampo.Items.Add("Descrição");

            this.ComboCampo.SelectedIndex = 0;
        }

        //Carrega informações do estado civil selecionado
        private void Selectprofissao()
        {
            if (DtgViewResultado.Rows.Count > 0)
            {
                profissao = new ProfissaoService().GetById(Convert.ToInt32(this.DtgViewResultado[0, DtgViewResultado.CurrentCell.RowIndex].Value.ToString()));

                this.Close();
            }
        }

        //Apaga texto de pesquisa quando o campo de pesquisa é alterado
        private void ComboCampo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.TxtTexto.Text = "";
        }

        //Fecha o formulário
        private void Sair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Carrega informações do estado civil selecionado
        private void Selecionar_Click(object sender, EventArgs e)
        {
            Selectprofissao();
        }

        //Carrega informações do estado civil selecionado
        private void DtgViewResultado_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Selectprofissao();
        }

        //Carrega informações do estado civil selecionado
        private void DtgViewResultado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                Selectprofissao();
            }
        }

        //Teclas de Atalho
        private void FormProfissaoBusca_KeyDown(object sender, KeyEventArgs e)
        {
            if ((this.TxtTexto.ContainsFocus) && (e.KeyData == Keys.Enter))
            {
                Pesquisar.PerformClick();
            }
            else
            {
                Service.Keys(keyEvent: e, BtnPesquisar: this.Pesquisar, BtnSair: this.Sair);
            }
        }

        //Executa a pesquisa
        private void Pesquisar_Click(object sender, EventArgs e)
        {
            this.DtgViewResultado.DataSource = null;
            this.DtgViewResultado.Refresh();
            this.DtgViewResultado.Rows.Clear();
            this.DtgViewResultado.AutoGenerateColumns = false;

            IList<ProfissaoDTO> Pesquisa = null;


            Pesquisa = new ProfissaoService().Search(this.ComboCampo.Text, this.TxtTexto.Text);


            if (Pesquisa.Count > 0)
            {
                var profissaos = Pesquisa.Select(profissao => new
                {
                    Id = profissao.Id,
                    Descricao = profissao.Descricao,
                }).ToList();

                DtgViewResultado.DataSource = profissaos;
                DtgViewResultado.Refresh();
            }
            else
            {
                MessageBox.Show("Nenhum registro encontrado.\nRevise seus critérios de pesquisa.", "GP", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        
    }
}
