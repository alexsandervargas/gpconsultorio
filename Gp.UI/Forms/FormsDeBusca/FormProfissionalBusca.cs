﻿using Gp.App.DTO;
using Gp.App.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gp
{
    public partial class FormProfissionalBusca : Form
    {
        public ProfissionalDTO Profissional { get; set; }
        private FormsService Service { get; set; }

        public FormProfissionalBusca()
        {
            InitializeComponent();
            CarregaCombos();
            this.Service = new FormsService();
            this.TxtTexto.Focus();
        }

        //Carrega combobox
        private void CarregaCombos()
        {
            this.ComboCampo.Items.Add("Nome");

            this.ComboCampo.SelectedIndex = 0;
        }

        //Carrega informações do profissional selecionado
        private void SelectProfissional()
        {
            if (DtgViewResultado.Rows.Count > 0)
            {
                Profissional = new ProfissionalService().GetById(Convert.ToInt32(this.DtgViewResultado[0, DtgViewResultado.CurrentCell.RowIndex].Value.ToString()));

                this.Close();
            }
        }

        //Apaga texto de pesquisa quando o campo de pesquisa é alterado
        private void ComboCampo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.TxtTexto.Text = "";
        }

        //Fecha o formulário
        private void Sair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Carrega informações do estado civil selecionado
        private void Selecionar_Click(object sender, EventArgs e)
        {
            SelectProfissional();
        }

        //Carrega informações do estado civil selecionado
        private void DtgViewResultado_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectProfissional();
        }

        //Carrega informações do estado civil selecionado
        private void DtgViewResultado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                SelectProfissional();
            }
        }

        //Teclas de Atalho
        private void FormProfissionalBusca_KeyDown(object sender, KeyEventArgs e)
        {
            if ((this.TxtTexto.ContainsFocus) && (e.KeyData == Keys.Enter))
            {
                Pesquisar.PerformClick();
            }
            else
            {
                Service.Keys(keyEvent: e, BtnPesquisar: this.Pesquisar, BtnSair: this.Sair);
            }
        }

        //Executa a pesquisa
        private void Pesquisar_Click(object sender, EventArgs e)
        {
            this.DtgViewResultado.DataSource = null;
            this.DtgViewResultado.Refresh();
            this.DtgViewResultado.Rows.Clear();
            this.DtgViewResultado.AutoGenerateColumns = false;

            IList<ProfissionalDTO> Pesquisa = null;


            Pesquisa = new ProfissionalService().Search(this.ComboCampo.Text, this.TxtTexto.Text);


            if (Pesquisa.Count > 0)
            {
                var Profissionals = Pesquisa.Select(profissional => new
                {
                    Id = profissional.Id,
                    Nome = profissional.Nome,
                    CPF = profissional.CPF,
                    Profissao = profissional.ProfissaoId != 0 ? new ProfissaoService().GetById(profissional.ProfissaoId).Descricao : string.Empty,
                    Conselho = profissional.ConselhoId != 0 ? new ConselhoService().GetById(profissional.ConselhoId).Descricao : string.Empty,
                    Registro  = profissional.MatriculaConselho,

                }).ToList();

                DtgViewResultado.DataSource = Profissionals;
                DtgViewResultado.Refresh();
            }
            else
            {
                MessageBox.Show("Nenhum registro encontrado.\nRevise seus critérios de pesquisa.", "GP", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        
    }
}
