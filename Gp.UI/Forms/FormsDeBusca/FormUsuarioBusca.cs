﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gp.App.DTO;
using Gp.App.Services;

namespace Gp
{
    public partial class FormUsuarioBusca : Form
    {
        public UsuarioDTO Usuario { get; set; }
        private FormsService Service { get; set; }

        private bool XGestor { get; set; }

        //Contrutor do formulário de busca
        //Popula o combobox com campos de pesquisa
        //set focus para campo txttexto
        public FormUsuarioBusca()
        {
            InitializeComponent();
            CarregaCombos();
            this.Service = new FormsService();
            XGestor = false;
            this.TxtTexto.Focus();
        }

        //Contrutor do formulário de busca
        //Popula o combobox com campos de pesquisa
        //set focus para campo txttexto
        public FormUsuarioBusca(bool gestor)
        {
            InitializeComponent();
            CarregaCombos();
            XGestor = true;
            this.TxtTexto.Focus();
        }


        //Carrega combobox
        private void CarregaCombos()
        {
            this.ComboCampo.Items.Add("Código");
            this.ComboCampo.Items.Add("Nome");
            this.ComboCampo.Items.Add("Login");
            this.ComboCampo.Items.Add("Email");

            this.ComboCampo.SelectedIndex = 0;

        }

        //Quando é alterado o campo de pesquisa o valor do campo texto é apagado
        private void ComboCampo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.TxtTexto.Text = "";
        }

        //Executa a pesquisa caso seja prescionado Enter e caso 
        //o campo de pesquisa seja númerico desabilita digitar letras
        private void TxtTexto_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.ComboCampo.Text == "Código")
            {
                if (e.KeyValue >= 48 && e.KeyValue <= 57 || e.KeyValue >= 96 && e.KeyValue <= 105 || e.KeyValue == 8 || e.KeyValue == 46)
                {
                    e.SuppressKeyPress = false;
                }
                else
                {
                    e.SuppressKeyPress = true;
                }
            }

            if (e.KeyData == Keys.Enter)
            {
                Pesquisar.PerformClick();
            }
        }

        //Executa a pesquisa
        private void Pesquisar_Click(object sender, EventArgs e)
        {
            this.DtgViewResultado.DataSource = null;
            this.DtgViewResultado.Refresh();
            this.DtgViewResultado.Rows.Clear();
            this.DtgViewResultado.AutoGenerateColumns = false;

            IList<UsuarioDTO> Pesquisa = null;


            Pesquisa = new UsuarioService().Search(this.ComboCampo.Text, this.TxtTexto.Text);


            if (Pesquisa.Count > 0)
            {
                DtgViewResultado.DataSource = Pesquisa;
                DtgViewResultado.Refresh();

            }
            else
            {
                MessageBox.Show("Nenhum registro encontrado.\nRevise seus critérios de pesquisa.", "GP", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        //Seleciona dados e carrega no form usuário
        private void DtgViewResultado_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectUser();
        }

        //Seleciona dados e carrega no form usuário
        private void Selecionar_Click(object sender, EventArgs e)
        {
            SelectUser();
        }

        //Carrega informações do usuário selecionado
        private void SelectUser()
        {
            if (DtgViewResultado.Rows.Count > 0)
            {
                Usuario = new UsuarioService().GetById(Convert.ToInt32(this.DtgViewResultado[0, DtgViewResultado.CurrentCell.RowIndex].Value.ToString()));

                this.Close();
            }
        }

        //Seleciona dados e carrega form usuário
        private void DtgViewResultado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                SelectUser();
            }
        }


        //Teclas de Atalho
        private void FormUsuarioBusca_KeyDown(object sender, KeyEventArgs e)
        {
            if ((this.TxtTexto.ContainsFocus) && (e.KeyData == Keys.Enter))
            {
                Pesquisar.PerformClick();
            }
            else
            {
                Service.Keys(keyEvent: e, BtnPesquisar: this.Pesquisar, BtnSair: this.Sair);
            }
        }

        private void Sair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
