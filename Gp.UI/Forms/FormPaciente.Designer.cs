﻿namespace Gp
{
    partial class FormPaciente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPaciente));
            this.TxtLog = new System.Windows.Forms.TextBox();
            this.MenuForm = new System.Windows.Forms.ToolStrip();
            this.Novo = new System.Windows.Forms.ToolStripButton();
            this.Salvar = new System.Windows.Forms.ToolStripButton();
            this.Editar = new System.Windows.Forms.ToolStripButton();
            this.Excluir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.Copiar = new System.Windows.Forms.ToolStripButton();
            this.Pesquisar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnIndicações = new System.Windows.Forms.ToolStripButton();
            this.BtnImprimir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.Sair = new System.Windows.Forms.ToolStripButton();
            this.GpPaciente = new System.Windows.Forms.GroupBox();
            this.TextEmpresa = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.GpIndicacao = new System.Windows.Forms.GroupBox();
            this.LblIndicacao = new System.Windows.Forms.Label();
            this.labelNomeIndicou = new System.Windows.Forms.Label();
            this.TxtNomePessoaIndicou = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtPessoaIndicouId = new Gp.TextBoxForceNumber();
            this.LblPessoaIndicou = new System.Windows.Forms.Label();
            this.ComboTipo = new System.Windows.Forms.ComboBox();
            this.BtnPessoaIndicou = new System.Windows.Forms.Button();
            this.labelIndicou = new System.Windows.Forms.Label();
            this.TxtFicha = new Gp.TextBoxForceNumber();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtCPF = new System.Windows.Forms.MaskedTextBox();
            this.TxtRG = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtPlanoId = new Gp.TextBoxForceNumber();
            this.LblPlano = new System.Windows.Forms.Label();
            this.BtnPlano = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtProfissaoId = new Gp.TextBoxForceNumber();
            this.LblProfissao = new System.Windows.Forms.Label();
            this.BtnProfissao = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtEstadoCivilId = new Gp.TextBoxForceNumber();
            this.LblEstadoCivil = new System.Windows.Forms.Label();
            this.BtnEstadoCivil = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtSexoId = new Gp.TextBoxForceNumber();
            this.LblSexo = new System.Windows.Forms.Label();
            this.BtnSexo = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtDataNascimento = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtMatricula = new System.Windows.Forms.TextBox();
            this.TxtObservacao = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtDDDComercial = new Gp.TextBoxForceNumber();
            this.TxtFoneComercial = new Gp.TextBoxForceNumber();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtDDDCelular = new Gp.TextBoxForceNumber();
            this.TxtFoneCelular = new Gp.TextBoxForceNumber();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtDDDResidencial = new Gp.TextBoxForceNumber();
            this.TxtFoneResidencial = new Gp.TextBoxForceNumber();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TxtCEP = new System.Windows.Forms.MaskedTextBox();
            this.TxtCidadeId = new Gp.TextBoxForceNumber();
            this.LblCidadeDescricao = new System.Windows.Forms.Label();
            this.BtnCidade = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtBairro = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtComplemento = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtNumero = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtLogradouro = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Errors = new System.Windows.Forms.ErrorProvider(this.components);
            this.MenuForm.SuspendLayout();
            this.GpPaciente.SuspendLayout();
            this.GpIndicacao.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Errors)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtLog
            // 
            this.TxtLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtLog.Enabled = false;
            this.TxtLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLog.Location = new System.Drawing.Point(0, 31);
            this.TxtLog.Name = "TxtLog";
            this.TxtLog.ReadOnly = true;
            this.TxtLog.Size = new System.Drawing.Size(866, 21);
            this.TxtLog.TabIndex = 19;
            // 
            // MenuForm
            // 
            this.MenuForm.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.MenuForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Novo,
            this.Salvar,
            this.Editar,
            this.Excluir,
            this.toolStripSeparator,
            this.Copiar,
            this.Pesquisar,
            this.toolStripSeparator1,
            this.BtnIndicações,
            this.BtnImprimir,
            this.toolStripSeparator2,
            this.Sair});
            this.MenuForm.Location = new System.Drawing.Point(0, 0);
            this.MenuForm.Name = "MenuForm";
            this.MenuForm.Size = new System.Drawing.Size(1002, 25);
            this.MenuForm.TabIndex = 17;
            this.MenuForm.Text = "toolStrip1";
            // 
            // Novo
            // 
            this.Novo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Novo.Image = ((System.Drawing.Image)(resources.GetObject("Novo.Image")));
            this.Novo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Novo.Name = "Novo";
            this.Novo.Size = new System.Drawing.Size(23, 22);
            this.Novo.Text = "Novo - F1";
            this.Novo.Click += new System.EventHandler(this.Novo_Click);
            // 
            // Salvar
            // 
            this.Salvar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Salvar.Enabled = false;
            this.Salvar.Image = ((System.Drawing.Image)(resources.GetObject("Salvar.Image")));
            this.Salvar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Salvar.Name = "Salvar";
            this.Salvar.Size = new System.Drawing.Size(23, 22);
            this.Salvar.Text = "Salvar - F6";
            this.Salvar.Click += new System.EventHandler(this.Salvar_Click);
            // 
            // Editar
            // 
            this.Editar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Editar.Enabled = false;
            this.Editar.Image = ((System.Drawing.Image)(resources.GetObject("Editar.Image")));
            this.Editar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Editar.Name = "Editar";
            this.Editar.Size = new System.Drawing.Size(23, 22);
            this.Editar.Text = "Editar - F2";
            this.Editar.Click += new System.EventHandler(this.Editar_Click);
            // 
            // Excluir
            // 
            this.Excluir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Excluir.Enabled = false;
            this.Excluir.Image = ((System.Drawing.Image)(resources.GetObject("Excluir.Image")));
            this.Excluir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Excluir.Name = "Excluir";
            this.Excluir.Size = new System.Drawing.Size(23, 22);
            this.Excluir.Text = "Excluir - F3";
            this.Excluir.Click += new System.EventHandler(this.Excluir_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // Copiar
            // 
            this.Copiar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Copiar.Enabled = false;
            this.Copiar.Image = ((System.Drawing.Image)(resources.GetObject("Copiar.Image")));
            this.Copiar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Copiar.Name = "Copiar";
            this.Copiar.Size = new System.Drawing.Size(23, 22);
            this.Copiar.Text = "&Copiar";
            this.Copiar.Click += new System.EventHandler(this.Copiar_Click);
            // 
            // Pesquisar
            // 
            this.Pesquisar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Pesquisar.Image = ((System.Drawing.Image)(resources.GetObject("Pesquisar.Image")));
            this.Pesquisar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Pesquisar.Name = "Pesquisar";
            this.Pesquisar.Size = new System.Drawing.Size(23, 22);
            this.Pesquisar.Text = "Pesquisar - F4";
            this.Pesquisar.Click += new System.EventHandler(this.Pesquisar_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnIndicações
            // 
            this.BtnIndicações.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnIndicações.Image = ((System.Drawing.Image)(resources.GetObject("BtnIndicações.Image")));
            this.BtnIndicações.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnIndicações.Name = "BtnIndicações";
            this.BtnIndicações.Size = new System.Drawing.Size(23, 22);
            this.BtnIndicações.Text = "Indicações";
            this.BtnIndicações.Click += new System.EventHandler(this.BtnIndicações_Click);
            // 
            // BtnImprimir
            // 
            this.BtnImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnImprimir.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir.Image")));
            this.BtnImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnImprimir.Name = "BtnImprimir";
            this.BtnImprimir.Size = new System.Drawing.Size(23, 22);
            this.BtnImprimir.Text = "Imprimir";
            this.BtnImprimir.Click += new System.EventHandler(this.BtnImprimir_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // Sair
            // 
            this.Sair.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Sair.Image = ((System.Drawing.Image)(resources.GetObject("Sair.Image")));
            this.Sair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Sair.Name = "Sair";
            this.Sair.Size = new System.Drawing.Size(23, 22);
            this.Sair.Text = "Sair - Esc";
            this.Sair.Click += new System.EventHandler(this.Sair_Click);
            // 
            // GpPaciente
            // 
            this.GpPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GpPaciente.Controls.Add(this.TextEmpresa);
            this.GpPaciente.Controls.Add(this.label23);
            this.GpPaciente.Controls.Add(this.GpIndicacao);
            this.GpPaciente.Controls.Add(this.TxtFicha);
            this.GpPaciente.Controls.Add(this.label22);
            this.GpPaciente.Controls.Add(this.TxtCPF);
            this.GpPaciente.Controls.Add(this.TxtRG);
            this.GpPaciente.Controls.Add(this.label5);
            this.GpPaciente.Controls.Add(this.TxtPlanoId);
            this.GpPaciente.Controls.Add(this.LblPlano);
            this.GpPaciente.Controls.Add(this.BtnPlano);
            this.GpPaciente.Controls.Add(this.label21);
            this.GpPaciente.Controls.Add(this.TxtProfissaoId);
            this.GpPaciente.Controls.Add(this.LblProfissao);
            this.GpPaciente.Controls.Add(this.BtnProfissao);
            this.GpPaciente.Controls.Add(this.label20);
            this.GpPaciente.Controls.Add(this.TxtEstadoCivilId);
            this.GpPaciente.Controls.Add(this.LblEstadoCivil);
            this.GpPaciente.Controls.Add(this.BtnEstadoCivil);
            this.GpPaciente.Controls.Add(this.label13);
            this.GpPaciente.Controls.Add(this.TxtSexoId);
            this.GpPaciente.Controls.Add(this.LblSexo);
            this.GpPaciente.Controls.Add(this.BtnSexo);
            this.GpPaciente.Controls.Add(this.label16);
            this.GpPaciente.Controls.Add(this.label3);
            this.GpPaciente.Controls.Add(this.TxtDataNascimento);
            this.GpPaciente.Controls.Add(this.label15);
            this.GpPaciente.Controls.Add(this.TxtMatricula);
            this.GpPaciente.Controls.Add(this.TxtObservacao);
            this.GpPaciente.Controls.Add(this.label12);
            this.GpPaciente.Controls.Add(this.groupBox1);
            this.GpPaciente.Controls.Add(this.groupBox2);
            this.GpPaciente.Controls.Add(this.label4);
            this.GpPaciente.Controls.Add(this.TxtNome);
            this.GpPaciente.Controls.Add(this.label2);
            this.GpPaciente.Controls.Add(this.TxtId);
            this.GpPaciente.Controls.Add(this.label1);
            this.GpPaciente.Location = new System.Drawing.Point(12, 57);
            this.GpPaciente.Name = "GpPaciente";
            this.GpPaciente.Size = new System.Drawing.Size(854, 961);
            this.GpPaciente.TabIndex = 21;
            this.GpPaciente.TabStop = false;
            this.GpPaciente.Text = "Dados do Cliente/Paciente";
            // 
            // TextEmpresa
            // 
            this.TextEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextEmpresa.Location = new System.Drawing.Point(129, 204);
            this.TextEmpresa.MaxLength = 300;
            this.TextEmpresa.Name = "TextEmpresa";
            this.TextEmpresa.Size = new System.Drawing.Size(558, 21);
            this.TextEmpresa.TabIndex = 76;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(61, 208);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 15);
            this.label23.TabIndex = 77;
            this.label23.Text = "Empresa";
            // 
            // GpIndicacao
            // 
            this.GpIndicacao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GpIndicacao.Controls.Add(this.LblIndicacao);
            this.GpIndicacao.Controls.Add(this.labelNomeIndicou);
            this.GpIndicacao.Controls.Add(this.TxtNomePessoaIndicou);
            this.GpIndicacao.Controls.Add(this.label25);
            this.GpIndicacao.Controls.Add(this.TxtPessoaIndicouId);
            this.GpIndicacao.Controls.Add(this.LblPessoaIndicou);
            this.GpIndicacao.Controls.Add(this.ComboTipo);
            this.GpIndicacao.Controls.Add(this.BtnPessoaIndicou);
            this.GpIndicacao.Controls.Add(this.labelIndicou);
            this.GpIndicacao.Location = new System.Drawing.Point(7, 279);
            this.GpIndicacao.Name = "GpIndicacao";
            this.GpIndicacao.Size = new System.Drawing.Size(841, 96);
            this.GpIndicacao.TabIndex = 10;
            this.GpIndicacao.TabStop = false;
            this.GpIndicacao.Text = "Indicação";
            // 
            // LblIndicacao
            // 
            this.LblIndicacao.AutoSize = true;
            this.LblIndicacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblIndicacao.Location = new System.Drawing.Point(259, 13);
            this.LblIndicacao.Name = "LblIndicacao";
            this.LblIndicacao.Size = new System.Drawing.Size(69, 15);
            this.LblIndicacao.TabIndex = 84;
            this.LblIndicacao.Text = "Indicacao";
            this.LblIndicacao.Visible = false;
            // 
            // labelNomeIndicou
            // 
            this.labelNomeIndicou.AutoSize = true;
            this.labelNomeIndicou.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomeIndicou.Location = new System.Drawing.Point(78, 68);
            this.labelNomeIndicou.Name = "labelNomeIndicou";
            this.labelNomeIndicou.Size = new System.Drawing.Size(41, 15);
            this.labelNomeIndicou.TabIndex = 83;
            this.labelNomeIndicou.Text = "Nome";
            // 
            // TxtNomePessoaIndicou
            // 
            this.TxtNomePessoaIndicou.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNomePessoaIndicou.Location = new System.Drawing.Point(123, 65);
            this.TxtNomePessoaIndicou.MaxLength = 100;
            this.TxtNomePessoaIndicou.Name = "TxtNomePessoaIndicou";
            this.TxtNomePessoaIndicou.Size = new System.Drawing.Size(558, 21);
            this.TxtNomePessoaIndicou.TabIndex = 82;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(84, 13);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(35, 15);
            this.label25.TabIndex = 81;
            this.label25.Text = "Tipo";
            // 
            // TxtPessoaIndicouId
            // 
            this.TxtPessoaIndicouId.Location = new System.Drawing.Point(123, 39);
            this.TxtPessoaIndicouId.Name = "TxtPessoaIndicouId";
            this.TxtPessoaIndicouId.Size = new System.Drawing.Size(100, 20);
            this.TxtPessoaIndicouId.TabIndex = 77;
            this.TxtPessoaIndicouId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtPessoaIndicouId.TextChanged += new System.EventHandler(this.TxtPessoaIndicouId_TextChanged);
            this.TxtPessoaIndicouId.Leave += new System.EventHandler(this.TxtPessoaIndicouId_Leave);
            // 
            // LblPessoaIndicou
            // 
            this.LblPessoaIndicou.AutoSize = true;
            this.LblPessoaIndicou.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPessoaIndicou.Location = new System.Drawing.Point(259, 42);
            this.LblPessoaIndicou.Name = "LblPessoaIndicou";
            this.LblPessoaIndicou.Size = new System.Drawing.Size(54, 15);
            this.LblPessoaIndicou.TabIndex = 80;
            this.LblPessoaIndicou.Text = "Pessoa";
            // 
            // ComboTipo
            // 
            this.ComboTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboTipo.FormattingEnabled = true;
            this.ComboTipo.Location = new System.Drawing.Point(123, 10);
            this.ComboTipo.Name = "ComboTipo";
            this.ComboTipo.Size = new System.Drawing.Size(131, 21);
            this.ComboTipo.TabIndex = 0;
            this.ComboTipo.TextChanged += new System.EventHandler(this.ComboTipo_SelectedIndexChanged);
            // 
            // BtnPessoaIndicou
            // 
            this.BtnPessoaIndicou.Location = new System.Drawing.Point(224, 39);
            this.BtnPessoaIndicou.Name = "BtnPessoaIndicou";
            this.BtnPessoaIndicou.Size = new System.Drawing.Size(29, 21);
            this.BtnPessoaIndicou.TabIndex = 79;
            this.BtnPessoaIndicou.TabStop = false;
            this.BtnPessoaIndicou.Text = "...";
            this.BtnPessoaIndicou.UseVisualStyleBackColor = true;
            this.BtnPessoaIndicou.Click += new System.EventHandler(this.BtnPessoaIndicou_Click);
            // 
            // labelIndicou
            // 
            this.labelIndicou.AutoSize = true;
            this.labelIndicou.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIndicou.Location = new System.Drawing.Point(35, 42);
            this.labelIndicou.Name = "labelIndicou";
            this.labelIndicou.Size = new System.Drawing.Size(84, 15);
            this.labelIndicou.TabIndex = 78;
            this.labelIndicou.Text = "Quem Indicou";
            // 
            // TxtFicha
            // 
            this.TxtFicha.Location = new System.Drawing.Point(339, 18);
            this.TxtFicha.Name = "TxtFicha";
            this.TxtFicha.Size = new System.Drawing.Size(100, 20);
            this.TxtFicha.TabIndex = 0;
            this.TxtFicha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(280, 21);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 15);
            this.label22.TabIndex = 75;
            this.label22.Text = "Nº Ficha";
            // 
            // TxtCPF
            // 
            this.TxtCPF.Location = new System.Drawing.Point(130, 74);
            this.TxtCPF.Mask = "000,000,000-00";
            this.TxtCPF.Name = "TxtCPF";
            this.TxtCPF.Size = new System.Drawing.Size(100, 20);
            this.TxtCPF.TabIndex = 2;
            this.TxtCPF.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TxtRG
            // 
            this.TxtRG.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRG.Location = new System.Drawing.Point(283, 73);
            this.TxtRG.MaxLength = 100;
            this.TxtRG.Name = "TxtRG";
            this.TxtRG.Size = new System.Drawing.Size(156, 21);
            this.TxtRG.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(244, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 15);
            this.label5.TabIndex = 73;
            this.label5.Text = "RG";
            // 
            // TxtPlanoId
            // 
            this.TxtPlanoId.Location = new System.Drawing.Point(129, 231);
            this.TxtPlanoId.Name = "TxtPlanoId";
            this.TxtPlanoId.Size = new System.Drawing.Size(100, 20);
            this.TxtPlanoId.TabIndex = 8;
            this.TxtPlanoId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtPlanoId.TextChanged += new System.EventHandler(this.TxtPlanoId_TextChanged);
            this.TxtPlanoId.Leave += new System.EventHandler(this.TxtPlanoId_Leave);
            // 
            // LblPlano
            // 
            this.LblPlano.AutoSize = true;
            this.LblPlano.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPlano.Location = new System.Drawing.Point(265, 234);
            this.LblPlano.Name = "LblPlano";
            this.LblPlano.Size = new System.Drawing.Size(44, 15);
            this.LblPlano.TabIndex = 71;
            this.LblPlano.Text = "Plano";
            // 
            // BtnPlano
            // 
            this.BtnPlano.Location = new System.Drawing.Point(230, 231);
            this.BtnPlano.Name = "BtnPlano";
            this.BtnPlano.Size = new System.Drawing.Size(29, 21);
            this.BtnPlano.TabIndex = 70;
            this.BtnPlano.TabStop = false;
            this.BtnPlano.Text = "...";
            this.BtnPlano.UseVisualStyleBackColor = true;
            this.BtnPlano.Click += new System.EventHandler(this.BtnPlano_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(79, 234);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 15);
            this.label21.TabIndex = 68;
            this.label21.Text = "Plano";
            // 
            // TxtProfissaoId
            // 
            this.TxtProfissaoId.Location = new System.Drawing.Point(129, 178);
            this.TxtProfissaoId.Name = "TxtProfissaoId";
            this.TxtProfissaoId.Size = new System.Drawing.Size(100, 20);
            this.TxtProfissaoId.TabIndex = 7;
            this.TxtProfissaoId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtProfissaoId.TextChanged += new System.EventHandler(this.TxtProfissaoId_TextChanged);
            this.TxtProfissaoId.Leave += new System.EventHandler(this.TxtProfissaoId_Leave);
            // 
            // LblProfissao
            // 
            this.LblProfissao.AutoSize = true;
            this.LblProfissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProfissao.Location = new System.Drawing.Point(265, 181);
            this.LblProfissao.Name = "LblProfissao";
            this.LblProfissao.Size = new System.Drawing.Size(67, 15);
            this.LblProfissao.TabIndex = 67;
            this.LblProfissao.Text = "Profissão";
            // 
            // BtnProfissao
            // 
            this.BtnProfissao.Location = new System.Drawing.Point(230, 178);
            this.BtnProfissao.Name = "BtnProfissao";
            this.BtnProfissao.Size = new System.Drawing.Size(29, 21);
            this.BtnProfissao.TabIndex = 10;
            this.BtnProfissao.TabStop = false;
            this.BtnProfissao.Text = "...";
            this.BtnProfissao.UseVisualStyleBackColor = true;
            this.BtnProfissao.Click += new System.EventHandler(this.BtnProfissao_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(60, 182);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 15);
            this.label20.TabIndex = 64;
            this.label20.Text = "Profissão";
            // 
            // TxtEstadoCivilId
            // 
            this.TxtEstadoCivilId.Location = new System.Drawing.Point(129, 152);
            this.TxtEstadoCivilId.Name = "TxtEstadoCivilId";
            this.TxtEstadoCivilId.Size = new System.Drawing.Size(100, 20);
            this.TxtEstadoCivilId.TabIndex = 6;
            this.TxtEstadoCivilId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtEstadoCivilId.TextChanged += new System.EventHandler(this.TxtEstadoCivilId_TextChanged);
            this.TxtEstadoCivilId.Leave += new System.EventHandler(this.TxtEstadoCivilId_Leave);
            // 
            // LblEstadoCivil
            // 
            this.LblEstadoCivil.AutoSize = true;
            this.LblEstadoCivil.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEstadoCivil.Location = new System.Drawing.Point(265, 155);
            this.LblEstadoCivil.Name = "LblEstadoCivil";
            this.LblEstadoCivil.Size = new System.Drawing.Size(82, 15);
            this.LblEstadoCivil.TabIndex = 62;
            this.LblEstadoCivil.Text = "Estado Civil";
            // 
            // BtnEstadoCivil
            // 
            this.BtnEstadoCivil.Location = new System.Drawing.Point(230, 152);
            this.BtnEstadoCivil.Name = "BtnEstadoCivil";
            this.BtnEstadoCivil.Size = new System.Drawing.Size(29, 21);
            this.BtnEstadoCivil.TabIndex = 8;
            this.BtnEstadoCivil.TabStop = false;
            this.BtnEstadoCivil.Text = "...";
            this.BtnEstadoCivil.UseVisualStyleBackColor = true;
            this.BtnEstadoCivil.Click += new System.EventHandler(this.BtnEstadoCivil_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(48, 156);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 15);
            this.label13.TabIndex = 61;
            this.label13.Text = "Estado Civil";
            // 
            // TxtSexoId
            // 
            this.TxtSexoId.Location = new System.Drawing.Point(129, 126);
            this.TxtSexoId.Name = "TxtSexoId";
            this.TxtSexoId.Size = new System.Drawing.Size(100, 20);
            this.TxtSexoId.TabIndex = 5;
            this.TxtSexoId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtSexoId.TextChanged += new System.EventHandler(this.TxtSexoId_TextChanged);
            this.TxtSexoId.Leave += new System.EventHandler(this.TxtSexoId_Leave);
            // 
            // LblSexo
            // 
            this.LblSexo.AutoSize = true;
            this.LblSexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSexo.Location = new System.Drawing.Point(265, 129);
            this.LblSexo.Name = "LblSexo";
            this.LblSexo.Size = new System.Drawing.Size(90, 15);
            this.LblSexo.TabIndex = 58;
            this.LblSexo.Text = "Gênero/Sexo";
            // 
            // BtnSexo
            // 
            this.BtnSexo.Location = new System.Drawing.Point(230, 126);
            this.BtnSexo.Name = "BtnSexo";
            this.BtnSexo.Size = new System.Drawing.Size(29, 21);
            this.BtnSexo.TabIndex = 6;
            this.BtnSexo.TabStop = false;
            this.BtnSexo.Text = "...";
            this.BtnSexo.UseVisualStyleBackColor = true;
            this.BtnSexo.Click += new System.EventHandler(this.BtnSexo_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(39, 130);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 15);
            this.label16.TabIndex = 57;
            this.label16.Text = "Gênero/Sexo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 15);
            this.label3.TabIndex = 54;
            this.label3.Text = "Dt. Nascimento";
            // 
            // TxtDataNascimento
            // 
            this.TxtDataNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.TxtDataNascimento.Location = new System.Drawing.Point(130, 100);
            this.TxtDataNascimento.Name = "TxtDataNascimento";
            this.TxtDataNascimento.Size = new System.Drawing.Size(100, 20);
            this.TxtDataNascimento.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(60, 261);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 15);
            this.label15.TabIndex = 52;
            this.label15.Text = "Matricula";
            // 
            // TxtMatricula
            // 
            this.TxtMatricula.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMatricula.Location = new System.Drawing.Point(129, 257);
            this.TxtMatricula.MaxLength = 100;
            this.TxtMatricula.Name = "TxtMatricula";
            this.TxtMatricula.Size = new System.Drawing.Size(130, 21);
            this.TxtMatricula.TabIndex = 9;
            // 
            // TxtObservacao
            // 
            this.TxtObservacao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtObservacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtObservacao.Location = new System.Drawing.Point(129, 600);
            this.TxtObservacao.MaxLength = 500;
            this.TxtObservacao.Multiline = true;
            this.TxtObservacao.Name = "TxtObservacao";
            this.TxtObservacao.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TxtObservacao.Size = new System.Drawing.Size(684, 91);
            this.TxtObservacao.TabIndex = 13;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(34, 600);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 15);
            this.label12.TabIndex = 48;
            this.label12.Text = "Observações";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.TxtEmail);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.TxtDDDComercial);
            this.groupBox1.Controls.Add(this.TxtFoneComercial);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.TxtDDDCelular);
            this.groupBox1.Controls.Add(this.TxtFoneCelular);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.TxtDDDResidencial);
            this.groupBox1.Controls.Add(this.TxtFoneResidencial);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Location = new System.Drawing.Point(7, 517);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(841, 77);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Contatos";
            // 
            // TxtEmail
            // 
            this.TxtEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Location = new System.Drawing.Point(123, 46);
            this.TxtEmail.MaxLength = 500;
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(384, 21);
            this.TxtEmail.TabIndex = 6;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(69, 49);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 15);
            this.label19.TabIndex = 47;
            this.label19.Text = "E-mail";
            // 
            // TxtDDDComercial
            // 
            this.TxtDDDComercial.Location = new System.Drawing.Point(729, 20);
            this.TxtDDDComercial.MaxLength = 2;
            this.TxtDDDComercial.Name = "TxtDDDComercial";
            this.TxtDDDComercial.Size = new System.Drawing.Size(41, 20);
            this.TxtDDDComercial.TabIndex = 4;
            this.TxtDDDComercial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxtFoneComercial
            // 
            this.TxtFoneComercial.Location = new System.Drawing.Point(771, 20);
            this.TxtFoneComercial.Name = "TxtFoneComercial";
            this.TxtFoneComercial.Size = new System.Drawing.Size(157, 20);
            this.TxtFoneComercial.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(629, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 15);
            this.label18.TabIndex = 45;
            this.label18.Text = "Tel.  Comercial";
            // 
            // TxtDDDCelular
            // 
            this.TxtDDDCelular.Location = new System.Drawing.Point(415, 20);
            this.TxtDDDCelular.MaxLength = 2;
            this.TxtDDDCelular.Name = "TxtDDDCelular";
            this.TxtDDDCelular.Size = new System.Drawing.Size(41, 20);
            this.TxtDDDCelular.TabIndex = 2;
            this.TxtDDDCelular.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxtFoneCelular
            // 
            this.TxtFoneCelular.Location = new System.Drawing.Point(457, 20);
            this.TxtFoneCelular.Name = "TxtFoneCelular";
            this.TxtFoneCelular.Size = new System.Drawing.Size(157, 20);
            this.TxtFoneCelular.TabIndex = 3;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(335, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 15);
            this.label17.TabIndex = 42;
            this.label17.Text = "Tel. Celular";
            // 
            // TxtDDDResidencial
            // 
            this.TxtDDDResidencial.Location = new System.Drawing.Point(123, 20);
            this.TxtDDDResidencial.MaxLength = 2;
            this.TxtDDDResidencial.Name = "TxtDDDResidencial";
            this.TxtDDDResidencial.Size = new System.Drawing.Size(41, 20);
            this.TxtDDDResidencial.TabIndex = 0;
            this.TxtDDDResidencial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxtFoneResidencial
            // 
            this.TxtFoneResidencial.Location = new System.Drawing.Point(165, 20);
            this.TxtFoneResidencial.Name = "TxtFoneResidencial";
            this.TxtFoneResidencial.Size = new System.Drawing.Size(157, 20);
            this.TxtFoneResidencial.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(17, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 15);
            this.label14.TabIndex = 39;
            this.label14.Text = "Tel. Residencial";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.TxtCEP);
            this.groupBox2.Controls.Add(this.TxtCidadeId);
            this.groupBox2.Controls.Add(this.LblCidadeDescricao);
            this.groupBox2.Controls.Add(this.BtnCidade);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.TxtBairro);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.TxtComplemento);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.TxtNumero);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.TxtLogradouro);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(7, 381);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(841, 130);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Endereço";
            // 
            // TxtCEP
            // 
            this.TxtCEP.Location = new System.Drawing.Point(123, 73);
            this.TxtCEP.Mask = "00000-000";
            this.TxtCEP.Name = "TxtCEP";
            this.TxtCEP.Size = new System.Drawing.Size(100, 20);
            this.TxtCEP.TabIndex = 10;
            this.TxtCEP.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.TxtCEP.Leave += new System.EventHandler(this.TxtCEP_Leave);
            // 
            // TxtCidadeId
            // 
            this.TxtCidadeId.Location = new System.Drawing.Point(123, 100);
            this.TxtCidadeId.Name = "TxtCidadeId";
            this.TxtCidadeId.Size = new System.Drawing.Size(100, 20);
            this.TxtCidadeId.TabIndex = 5;
            this.TxtCidadeId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtCidadeId.TextChanged += new System.EventHandler(this.TxtCidadeId_TextChanged);
            this.TxtCidadeId.Leave += new System.EventHandler(this.TxtCidadeId_Leave);
            // 
            // LblCidadeDescricao
            // 
            this.LblCidadeDescricao.AutoSize = true;
            this.LblCidadeDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCidadeDescricao.Location = new System.Drawing.Point(259, 103);
            this.LblCidadeDescricao.Name = "LblCidadeDescricao";
            this.LblCidadeDescricao.Size = new System.Drawing.Size(52, 15);
            this.LblCidadeDescricao.TabIndex = 46;
            this.LblCidadeDescricao.Text = "Cidade";
            // 
            // BtnCidade
            // 
            this.BtnCidade.Location = new System.Drawing.Point(224, 100);
            this.BtnCidade.Name = "BtnCidade";
            this.BtnCidade.Size = new System.Drawing.Size(29, 21);
            this.BtnCidade.TabIndex = 11;
            this.BtnCidade.TabStop = false;
            this.BtnCidade.Text = "...";
            this.BtnCidade.UseVisualStyleBackColor = true;
            this.BtnCidade.Click += new System.EventHandler(this.BtnCidade_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(66, 103);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 15);
            this.label11.TabIndex = 45;
            this.label11.Text = "Cidade";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(81, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 15);
            this.label10.TabIndex = 42;
            this.label10.Text = "CEP";
            // 
            // TxtBairro
            // 
            this.TxtBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBairro.Location = new System.Drawing.Point(499, 46);
            this.TxtBairro.MaxLength = 100;
            this.TxtBairro.Name = "TxtBairro";
            this.TxtBairro.Size = new System.Drawing.Size(367, 21);
            this.TxtBairro.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(442, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 15);
            this.label9.TabIndex = 40;
            this.label9.Text = "Bairro";
            // 
            // TxtComplemento
            // 
            this.TxtComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtComplemento.Location = new System.Drawing.Point(123, 46);
            this.TxtComplemento.MaxLength = 100;
            this.TxtComplemento.Name = "TxtComplemento";
            this.TxtComplemento.Size = new System.Drawing.Size(309, 21);
            this.TxtComplemento.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(27, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 15);
            this.label8.TabIndex = 38;
            this.label8.Text = "Complemento";
            // 
            // TxtNumero
            // 
            this.TxtNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNumero.Location = new System.Drawing.Point(780, 19);
            this.TxtNumero.MaxLength = 100;
            this.TxtNumero.Name = "TxtNumero";
            this.TxtNumero.Size = new System.Drawing.Size(86, 21);
            this.TxtNumero.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(707, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 15);
            this.label7.TabIndex = 36;
            this.label7.Text = "Número";
            // 
            // TxtLogradouro
            // 
            this.TxtLogradouro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLogradouro.Location = new System.Drawing.Point(123, 19);
            this.TxtLogradouro.MaxLength = 100;
            this.TxtLogradouro.Name = "TxtLogradouro";
            this.TxtLogradouro.Size = new System.Drawing.Size(557, 21);
            this.TxtLogradouro.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(31, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 15);
            this.label6.TabIndex = 34;
            this.label6.Text = "Logradouro";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(85, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "CPF";
            // 
            // TxtNome
            // 
            this.TxtNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNome.Location = new System.Drawing.Point(130, 47);
            this.TxtNome.MaxLength = 100;
            this.TxtNome.Name = "TxtNome";
            this.TxtNome.Size = new System.Drawing.Size(557, 21);
            this.TxtNome.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(73, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nome";
            // 
            // TxtId
            // 
            this.TxtId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtId.Location = new System.Drawing.Point(130, 18);
            this.TxtId.Name = "TxtId";
            this.TxtId.ReadOnly = true;
            this.TxtId.Size = new System.Drawing.Size(100, 21);
            this.TxtId.TabIndex = 0;
            this.TxtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(72, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Código";
            // 
            // Errors
            // 
            this.Errors.ContainerControl = this;
            this.Errors.RightToLeft = true;
            // 
            // FormPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(0, 780);
            this.ClientSize = new System.Drawing.Size(1019, 749);
            this.Controls.Add(this.GpPaciente);
            this.Controls.Add(this.TxtLog);
            this.Controls.Add(this.MenuForm);
            this.KeyPreview = true;
            this.Name = "FormPaciente";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Cadastro de Cliente / Paciente";
            this.Load += new System.EventHandler(this.FormPaciente_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormPaciente_KeyDown);
            this.MenuForm.ResumeLayout(false);
            this.MenuForm.PerformLayout();
            this.GpPaciente.ResumeLayout(false);
            this.GpPaciente.PerformLayout();
            this.GpIndicacao.ResumeLayout(false);
            this.GpIndicacao.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Errors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtLog;
        public System.Windows.Forms.ToolStrip MenuForm;
        private System.Windows.Forms.ToolStripButton Novo;
        private System.Windows.Forms.ToolStripButton Salvar;
        private System.Windows.Forms.ToolStripButton Editar;
        private System.Windows.Forms.ToolStripButton Excluir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton Copiar;
        private System.Windows.Forms.ToolStripButton Pesquisar;
        private System.Windows.Forms.ToolStripButton Sair;
        private System.Windows.Forms.GroupBox GpPaciente;
        private System.Windows.Forms.MaskedTextBox TxtCPF;
        private System.Windows.Forms.TextBox TxtRG;
        private System.Windows.Forms.Label label5;
        private Gp.TextBoxForceNumber TxtPlanoId;
        private System.Windows.Forms.Label LblPlano;
        private System.Windows.Forms.Button BtnPlano;
        private System.Windows.Forms.Label label21;
        private Gp.TextBoxForceNumber TxtProfissaoId;
        private System.Windows.Forms.Label LblProfissao;
        private System.Windows.Forms.Button BtnProfissao;
        private System.Windows.Forms.Label label20;
        private Gp.TextBoxForceNumber TxtEstadoCivilId;
        private System.Windows.Forms.Label LblEstadoCivil;
        private System.Windows.Forms.Button BtnEstadoCivil;
        private System.Windows.Forms.Label label13;
        private Gp.TextBoxForceNumber TxtSexoId;
        private System.Windows.Forms.Label LblSexo;
        private System.Windows.Forms.Button BtnSexo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker TxtDataNascimento;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox TxtMatricula;
        private System.Windows.Forms.TextBox TxtObservacao;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.Label label19;
        private Gp.TextBoxForceNumber TxtDDDComercial;
        private Gp.TextBoxForceNumber TxtFoneComercial;
        private System.Windows.Forms.Label label18;
        private Gp.TextBoxForceNumber TxtDDDCelular;
        private Gp.TextBoxForceNumber TxtFoneCelular;
        private System.Windows.Forms.Label label17;
        private Gp.TextBoxForceNumber TxtDDDResidencial;
        private Gp.TextBoxForceNumber TxtFoneResidencial;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.MaskedTextBox TxtCEP;
        private Gp.TextBoxForceNumber TxtCidadeId;
        private System.Windows.Forms.Label LblCidadeDescricao;
        private System.Windows.Forms.Button BtnCidade;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TxtBairro;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtComplemento;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtNumero;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtLogradouro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ErrorProvider Errors;
        private TextBoxForceNumber TxtFicha;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox GpIndicacao;
        private System.Windows.Forms.Label labelNomeIndicou;
        private System.Windows.Forms.TextBox TxtNomePessoaIndicou;
        private System.Windows.Forms.Label label25;
        private TextBoxForceNumber TxtPessoaIndicouId;
        private System.Windows.Forms.Label LblPessoaIndicou;
        private System.Windows.Forms.ComboBox ComboTipo;
        private System.Windows.Forms.Button BtnPessoaIndicou;
        private System.Windows.Forms.Label labelIndicou;
        private System.Windows.Forms.Label LblIndicacao;
        private System.Windows.Forms.ToolStripButton BtnIndicações;
        private System.Windows.Forms.ToolStripButton BtnImprimir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.TextBox TextEmpresa;
        private System.Windows.Forms.Label label23;
    }
}