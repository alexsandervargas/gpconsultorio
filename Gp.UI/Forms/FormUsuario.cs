﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gp.App.DTO;
using Gp.App.Services;

namespace Gp
{
    public partial class FormUsuario : Form
    {
        FormsService Service;
        public UsuarioDTO UsuarioLogado { get; set; }

        
        //Popula os Combobox do form limpa os campos e desabilita os controles
        public FormUsuario()
        {
            InitializeComponent();

            //Instancia FormService
            Service = new FormsService();

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.DisableControls(this);

            //Popula ComboBox Ativo
            this.ComboAtivo.Items.Add("NÃO");
            this.ComboAtivo.Items.Add("SIM");
            this.ComboAtivo.SelectedIndex = 1;
            
            this.WindowState = FormWindowState.Maximized;
            this.Refresh();
        }

        //Inicialização do form com ajustes de botões
        private void FormUsuario_Load(object sender, EventArgs e)
        {
            //Ajusta os botoes
            Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);
        }

        //Gerencia as teclas de atalho
        private void FormUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            Service.Keys(keyEvent: e, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar, BtnPesquisar: this.Pesquisar,
                BtnSair: this.Sair, BtnExcluir: this.Excluir);
        }

        //Preenche o formulário com os dados do usuário
        public void LoadForm()
        {
            try
            {
                var usuario = new UsuarioService().GetById(Convert.ToInt32(this.TxtId.Text));

                this.TxtNome.Text = usuario.Nome;
                this.TxtLogin.Text = usuario.Login;
                this.TxtEmail.Text = usuario.Email;
                this.TxtSenha.Text = usuario.Senha;
                this.TxtConfirmaSenha.Text = usuario.Senha;

                this.ComboAtivo.SelectedIndex = Convert.ToInt32(usuario.Ativo);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        #region ValidationsForm
        //Validações do Formulário Usuário
        public bool ValidateForm()
        {
            bool Validate = true;

            try
            {
                //Veryfy Text Nome
                if (this.TxtNome.Text == string.Empty)
                {
                    this.Errors.SetError(TxtNome, "Informe o Nome do Usuário");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtNome, "");
                }

                //Veryfy Text Email
                if (this.TxtEmail.Text == string.Empty)
                {
                    this.Errors.SetError(TxtEmail, "Informe o Email");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtEmail, "");
                }

                //Veryfy Text Login
                if (this.TxtLogin.Text == string.Empty)
                {
                    this.Errors.SetError(TxtLogin, "Informe o Login");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtLogin, "");
                }

                //Veryfy Text Senha e ConfirmaSenha
                if (this.TxtSenha.Text == string.Empty)
                {
                    this.Errors.SetError(TxtSenha, "Informe a Senha");
                    Validate = false;
                }
                else if (this.TxtSenha.Text != this.TxtConfirmaSenha.Text)
                {
                    this.Errors.SetError(TxtSenha, "As senhas fornecidas não conbinam");
                    this.Errors.SetError(TxtConfirmaSenha, "As senhas fornecidas não conbinam");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtSenha, "");
                    this.Errors.SetError(TxtConfirmaSenha, "");
                }

                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return Validate;

        }

        #endregion

        #region ButtonsMenu   

        //Habilita nova inserção de usuário no formulário
        private void Novo_Click(object sender, EventArgs e)
        {
            //Verifica se formulário esta preenchido e em edição de algum usuário
            //Caso esteja questiona o usuário se deseja salvar as alterações realizadas ou não
            if (this.TxtId.Text != "" && GpUsuario.Enabled == true)
            {
                if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Salvar.PerformClick();
                }
            }

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.EnableControls(this);

            //Desabilita os Botoes do form
            Service.ButtonsNewInsert(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

        }

        //Salva usuário preenchido no formulário
        private void Salvar_Click(object sender, EventArgs e)
        {

            if (ValidateForm())
            {
                try
                {

                    UsuarioDTO usuario = new UsuarioDTO
                    {
                        Nome = this.TxtNome.Text,
                        Login = this.TxtLogin.Text,
                        Senha = this.TxtSenha.Text,
                        Email = this.TxtEmail.Text,
                        Ativo = Convert.ToBoolean(this.ComboAtivo.SelectedIndex),
                        ConfirmaSenha = this.TxtConfirmaSenha.Text,
                    };


                    if (this.TxtId.Text != "")
                    {
                        usuario.Id = Convert.ToInt32(this.TxtId.Text);
                    }



                    if (usuario.Id != 0)
                    {
                        if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                    }

                    new UsuarioService().Save(usuario);

                    if (usuario.Id != 0)
                    {
                        this.TxtId.Text = Convert.ToString(usuario.Id);

                        Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(usuario: usuario, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

                       
                        MessageBox.Show("Usuário [" + this.TxtNome.Text + "] salvo com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        //Fecha Formulário
        private void Sair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Habilita controle para edição do usuário
        private void Editar_Click(object sender, EventArgs e)
        {
            Service.EnableControls(this);

            Service.AdjutsButtonsToEdition(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

        }

        //Abre formulário para pesquisa
        private void Pesquisar_Click(object sender, EventArgs e)
        {
            var form = new FormUsuarioBusca();

            form.ShowDialog();

            var usuario = form.Usuario;

            if (usuario != null)
            {

                this.Service.ClearForm(this);
                this.Service.DisableControls(this);
                Service.AdjutsButtonsToSearchAndSave(usuario: usuario, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                this.TxtId.Text = usuario.Id.ToString();

                LoadForm();

            }
        }

        //Exclui usuário
        private void Excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Confirma Exclusão?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        new UsuarioService().Delete(Convert.ToInt32(this.TxtId.Text));
                        this.Service.ClearForm(this);
                        this.Service.DisableControls(this);

                        Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

                        MessageBox.Show("Exclusão Confirmada!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        //Realiza cópia do usuário em tela
        private void Copiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Deseja Copiar este usuário?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var usuario = new UsuarioService().GetById(Convert.ToInt32(this.TxtId.Text));
                        usuario.Nome += " Cópia";
                        usuario.Login += " Copia";
                        usuario.Id = 0;

                        new UsuarioService().Save(usuario);

                        this.TxtId.Text = usuario.Id.ToString();

                        this.Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(
                            usuario: usuario,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

                        LoadForm();

                        MessageBox.Show("Cópia efetuada com sucesso?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        
        #endregion

    }
}
