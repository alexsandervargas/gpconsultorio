﻿namespace Gp
{
    partial class FormGp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGp));
            this.MenuGP = new System.Windows.Forms.MenuStrip();
            this.cadastroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conselhoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empresaDeSaúdeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientePacienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.planoDeSaúdeOdontológicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dentistaMédicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Administracao = new System.Windows.Forms.ToolStripMenuItem();
            this.cidadeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.estadoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.estadoCivilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generoSexoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paísToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profissãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.usuárioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Janelas = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuAtalhos = new System.Windows.Forms.ToolStrip();
            this.BtnEmpresa = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.BtnPaciente = new System.Windows.Forms.ToolStripButton();
            this.BtnPlano = new System.Windows.Forms.ToolStripButton();
            this.BtnProfissional = new System.Windows.Forms.ToolStripButton();
            this.BarraStatus = new System.Windows.Forms.StatusStrip();
            this.IdUsuario = new System.Windows.Forms.ToolStripStatusLabel();
            this.Espace = new System.Windows.Forms.ToolStripStatusLabel();
            this.NomeUsuario = new System.Windows.Forms.ToolStripStatusLabel();
            this.anivesariantesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuGP.SuspendLayout();
            this.MenuAtalhos.SuspendLayout();
            this.BarraStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuGP
            // 
            this.MenuGP.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroToolStripMenuItem,
            this.Administracao,
            this.Janelas});
            this.MenuGP.Location = new System.Drawing.Point(0, 0);
            this.MenuGP.MdiWindowListItem = this.Janelas;
            this.MenuGP.Name = "MenuGP";
            this.MenuGP.Size = new System.Drawing.Size(724, 24);
            this.MenuGP.TabIndex = 1;
            // 
            // cadastroToolStripMenuItem
            // 
            this.cadastroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.conselhoToolStripMenuItem,
            this.empresaDeSaúdeToolStripMenuItem,
            this.clientePacienteToolStripMenuItem,
            this.planoDeSaúdeOdontológicoToolStripMenuItem,
            this.dentistaMédicoToolStripMenuItem});
            this.cadastroToolStripMenuItem.Name = "cadastroToolStripMenuItem";
            this.cadastroToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.cadastroToolStripMenuItem.Text = "Cadastro";
            // 
            // conselhoToolStripMenuItem
            // 
            this.conselhoToolStripMenuItem.Name = "conselhoToolStripMenuItem";
            this.conselhoToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.conselhoToolStripMenuItem.Text = "Conselho";
            this.conselhoToolStripMenuItem.Click += new System.EventHandler(this.conselhoToolStripMenuItem_Click);
            // 
            // empresaDeSaúdeToolStripMenuItem
            // 
            this.empresaDeSaúdeToolStripMenuItem.Name = "empresaDeSaúdeToolStripMenuItem";
            this.empresaDeSaúdeToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.empresaDeSaúdeToolStripMenuItem.Text = "Empresa de Saúde";
            this.empresaDeSaúdeToolStripMenuItem.Click += new System.EventHandler(this.empresaDeSaúdeToolStripMenuItem_Click);
            // 
            // clientePacienteToolStripMenuItem
            // 
            this.clientePacienteToolStripMenuItem.Name = "clientePacienteToolStripMenuItem";
            this.clientePacienteToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.clientePacienteToolStripMenuItem.Text = "Paciente";
            this.clientePacienteToolStripMenuItem.Click += new System.EventHandler(this.clientePacienteToolStripMenuItem_Click);
            // 
            // planoDeSaúdeOdontológicoToolStripMenuItem
            // 
            this.planoDeSaúdeOdontológicoToolStripMenuItem.Name = "planoDeSaúdeOdontológicoToolStripMenuItem";
            this.planoDeSaúdeOdontológicoToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.planoDeSaúdeOdontológicoToolStripMenuItem.Text = "Plano de Saúde/Odontológico";
            this.planoDeSaúdeOdontológicoToolStripMenuItem.Click += new System.EventHandler(this.planoDeSaúdeOdontológicoToolStripMenuItem_Click);
            // 
            // dentistaMédicoToolStripMenuItem
            // 
            this.dentistaMédicoToolStripMenuItem.Name = "dentistaMédicoToolStripMenuItem";
            this.dentistaMédicoToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.dentistaMédicoToolStripMenuItem.Text = "Profissional de Saúde";
            this.dentistaMédicoToolStripMenuItem.Click += new System.EventHandler(this.dentistaMédicoToolStripMenuItem_Click);
            // 
            // Administracao
            // 
            this.Administracao.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cidadeToolStripMenuItem1,
            this.estadoToolStripMenuItem1,
            this.estadoCivilToolStripMenuItem,
            this.generoSexoToolStripMenuItem,
            this.paísToolStripMenuItem,
            this.profissãoToolStripMenuItem,
            this.toolStripSeparator2,
            this.usuárioToolStripMenuItem,
            this.anivesariantesToolStripMenuItem});
            this.Administracao.Name = "Administracao";
            this.Administracao.Size = new System.Drawing.Size(96, 20);
            this.Administracao.Text = "Administração";
            // 
            // cidadeToolStripMenuItem1
            // 
            this.cidadeToolStripMenuItem1.Name = "cidadeToolStripMenuItem1";
            this.cidadeToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.cidadeToolStripMenuItem1.Text = "Cidade";
            this.cidadeToolStripMenuItem1.Click += new System.EventHandler(this.cidadeToolStripMenuItem1_Click);
            // 
            // estadoToolStripMenuItem1
            // 
            this.estadoToolStripMenuItem1.Name = "estadoToolStripMenuItem1";
            this.estadoToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.estadoToolStripMenuItem1.Text = "Estado";
            this.estadoToolStripMenuItem1.Click += new System.EventHandler(this.estadoToolStripMenuItem1_Click);
            // 
            // estadoCivilToolStripMenuItem
            // 
            this.estadoCivilToolStripMenuItem.Name = "estadoCivilToolStripMenuItem";
            this.estadoCivilToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.estadoCivilToolStripMenuItem.Text = "Estado Civil";
            this.estadoCivilToolStripMenuItem.Click += new System.EventHandler(this.estadoCivilToolStripMenuItem_Click);
            // 
            // generoSexoToolStripMenuItem
            // 
            this.generoSexoToolStripMenuItem.Name = "generoSexoToolStripMenuItem";
            this.generoSexoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.generoSexoToolStripMenuItem.Text = "Gênero/Sexo";
            this.generoSexoToolStripMenuItem.Click += new System.EventHandler(this.generoSexoToolStripMenuItem_Click);
            // 
            // paísToolStripMenuItem
            // 
            this.paísToolStripMenuItem.Name = "paísToolStripMenuItem";
            this.paísToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.paísToolStripMenuItem.Text = "País";
            this.paísToolStripMenuItem.Click += new System.EventHandler(this.paísToolStripMenuItem_Click);
            // 
            // profissãoToolStripMenuItem
            // 
            this.profissãoToolStripMenuItem.Name = "profissãoToolStripMenuItem";
            this.profissãoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.profissãoToolStripMenuItem.Text = "Profissão";
            this.profissãoToolStripMenuItem.Click += new System.EventHandler(this.profissãoToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(177, 6);
            // 
            // usuárioToolStripMenuItem
            // 
            this.usuárioToolStripMenuItem.Name = "usuárioToolStripMenuItem";
            this.usuárioToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.usuárioToolStripMenuItem.Text = "Usuário";
            this.usuárioToolStripMenuItem.Click += new System.EventHandler(this.usuárioToolStripMenuItem_Click);
            // 
            // Janelas
            // 
            this.Janelas.Name = "Janelas";
            this.Janelas.Size = new System.Drawing.Size(51, 20);
            this.Janelas.Text = "Janela";
            // 
            // MenuAtalhos
            // 
            this.MenuAtalhos.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuAtalhos.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.MenuAtalhos.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.MenuAtalhos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnEmpresa,
            this.toolStripButton1,
            this.BtnPaciente,
            this.BtnPlano,
            this.BtnProfissional});
            this.MenuAtalhos.Location = new System.Drawing.Point(0, 24);
            this.MenuAtalhos.Name = "MenuAtalhos";
            this.MenuAtalhos.Size = new System.Drawing.Size(73, 393);
            this.MenuAtalhos.TabIndex = 2;
            // 
            // BtnEmpresa
            // 
            this.BtnEmpresa.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpresa.Image")));
            this.BtnEmpresa.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmpresa.Name = "BtnEmpresa";
            this.BtnEmpresa.Size = new System.Drawing.Size(70, 59);
            this.BtnEmpresa.Text = "Empresa";
            this.BtnEmpresa.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnEmpresa.Click += new System.EventHandler(this.BtnEmpresa_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(70, 59);
            this.toolStripButton1.Text = "Conselho";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // BtnPaciente
            // 
            this.BtnPaciente.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPaciente.Image = ((System.Drawing.Image)(resources.GetObject("BtnPaciente.Image")));
            this.BtnPaciente.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnPaciente.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPaciente.Name = "BtnPaciente";
            this.BtnPaciente.Size = new System.Drawing.Size(70, 59);
            this.BtnPaciente.Text = "Paciente";
            this.BtnPaciente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnPaciente.Click += new System.EventHandler(this.BtnPaciente_Click);
            // 
            // BtnPlano
            // 
            this.BtnPlano.Image = ((System.Drawing.Image)(resources.GetObject("BtnPlano.Image")));
            this.BtnPlano.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPlano.Name = "BtnPlano";
            this.BtnPlano.Size = new System.Drawing.Size(70, 59);
            this.BtnPlano.Text = "Plano";
            this.BtnPlano.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnPlano.Click += new System.EventHandler(this.BtnPlano_Click);
            // 
            // BtnProfissional
            // 
            this.BtnProfissional.Image = ((System.Drawing.Image)(resources.GetObject("BtnProfissional.Image")));
            this.BtnProfissional.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnProfissional.Name = "BtnProfissional";
            this.BtnProfissional.Size = new System.Drawing.Size(70, 59);
            this.BtnProfissional.Text = "Profissional";
            this.BtnProfissional.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnProfissional.Click += new System.EventHandler(this.BtnProfissional_Click);
            // 
            // BarraStatus
            // 
            this.BarraStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.IdUsuario,
            this.Espace,
            this.NomeUsuario});
            this.BarraStatus.Location = new System.Drawing.Point(73, 395);
            this.BarraStatus.Name = "BarraStatus";
            this.BarraStatus.Size = new System.Drawing.Size(651, 22);
            this.BarraStatus.TabIndex = 3;
            this.BarraStatus.Text = "statusStrip1";
            // 
            // IdUsuario
            // 
            this.IdUsuario.Name = "IdUsuario";
            this.IdUsuario.Size = new System.Drawing.Size(57, 17);
            this.IdUsuario.Text = "IdUsuário";
            // 
            // Espace
            // 
            this.Espace.Name = "Espace";
            this.Espace.Size = new System.Drawing.Size(12, 17);
            this.Espace.Text = "-";
            // 
            // NomeUsuario
            // 
            this.NomeUsuario.Name = "NomeUsuario";
            this.NomeUsuario.Size = new System.Drawing.Size(80, 17);
            this.NomeUsuario.Text = "NomeUsuario";
            // 
            // anivesariantesToolStripMenuItem
            // 
            this.anivesariantesToolStripMenuItem.Name = "anivesariantesToolStripMenuItem";
            this.anivesariantesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.anivesariantesToolStripMenuItem.Text = "Anivesariantes";
            this.anivesariantesToolStripMenuItem.Click += new System.EventHandler(this.anivesariantesToolStripMenuItem_Click);
            // 
            // FormGp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 417);
            this.Controls.Add(this.BarraStatus);
            this.Controls.Add(this.MenuAtalhos);
            this.Controls.Add(this.MenuGP);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.MenuGP;
            this.Name = "FormGp";
            this.Text = "GP -Gestão de Pacientes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormGp_Load);
            this.MenuGP.ResumeLayout(false);
            this.MenuGP.PerformLayout();
            this.MenuAtalhos.ResumeLayout(false);
            this.MenuAtalhos.PerformLayout();
            this.BarraStatus.ResumeLayout(false);
            this.BarraStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuGP;
        private System.Windows.Forms.ToolStrip MenuAtalhos;
        private System.Windows.Forms.StatusStrip BarraStatus;
        private System.Windows.Forms.ToolStripMenuItem Administracao;
        private System.Windows.Forms.ToolStripButton BtnPaciente;
        private System.Windows.Forms.ToolStripMenuItem Janelas;
        private System.Windows.Forms.ToolStripStatusLabel IdUsuario;
        private System.Windows.Forms.ToolStripStatusLabel Espace;
        private System.Windows.Forms.ToolStripStatusLabel NomeUsuario;
        private System.Windows.Forms.ToolStripMenuItem cadastroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientePacienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem conselhoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empresaDeSaúdeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem planoDeSaúdeOdontológicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dentistaMédicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cidadeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem estadoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem estadoCivilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generoSexoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem usuárioToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton BtnProfissional;
        private System.Windows.Forms.ToolStripButton BtnEmpresa;
        private System.Windows.Forms.ToolStripButton BtnPlano;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripMenuItem paísToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profissãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem anivesariantesToolStripMenuItem;
    }
}