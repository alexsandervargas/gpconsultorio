﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gp.App.DTO;
using Gp.App.Services;

namespace Gp
{
    public partial class FormEstadoCivil : Form
    {
        FormsService Service;
        public UsuarioDTO UsuarioLogado { get; set; }

        public FormEstadoCivil()
        {
            InitializeComponent();

            //Instancia FormService
            Service = new FormsService();

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.DisableControls(this);            

            this.WindowState = FormWindowState.Maximized;
            this.Refresh();
        }

        //Gerencia as teclas de atalho
        private void FormEstadoCivil_KeyDown(object sender, KeyEventArgs e)
        {
            Service.Keys(keyEvent: e, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar, BtnPesquisar: this.Pesquisar,
                BtnSair: this.Sair, BtnExcluir: this.Excluir);

        }

        //Inicialização do form com ajustes de botões
        private void FormEstadoCivil_Load(object sender, EventArgs e)
        {
            //Ajusta os botoes
            Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

        }
                     
        //Preenche o formulário com os dados
        public void LoadForm()
        {
            try
            {
                var estadocivil = new EstadoCivilService().GetById(Convert.ToInt32(this.TxtId.Text));

                this.TxtDescricao.Text = estadocivil.Descricao;

                ValidateForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region ValidationsForm
        //Validações do Formulário Usuário
        public bool ValidateForm()
        {
            bool Validate = true;

            try
            {
                //Veryfy Text Nome
                if (this.TxtDescricao.Text == string.Empty)
                {
                    this.Errors.SetError(TxtDescricao, "Informe a Descrição do Estado Civil");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtDescricao, "");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return Validate;

        }

        #endregion

        #region ButtonsMenu

        //Habilita nova inserção de usuário no formulário
        private void Novo_Click(object sender, EventArgs e)
        {
            //Verifica se formulário esta preenchido e em edição de algum estado civil
            //Caso esteja questiona o usuário se deseja salvar as alterações realizadas ou não
            if (this.TxtId.Text != "" && GpEstadoCivil.Enabled == true)
            {
                if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Salvar.PerformClick();
                }
            }

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.EnableControls(this);

            //Desabilita os Botoes do form
            Service.ButtonsNewInsert(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

        }

        //Salva dados preenchido no formulário
        private void Salvar_Click(object sender, EventArgs e)
        {

            if (ValidateForm())
            {
                try
                {

                    EstadoCivilDTO estadocivil = new EstadoCivilDTO
                    {
                        Descricao = this.TxtDescricao.Text,
                    };


                    if (this.TxtId.Text != "")
                    {
                        estadocivil.Id = Convert.ToInt32(this.TxtId.Text);
                    }



                    if (estadocivil.Id != 0)
                    {
                        if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                    }

                    new EstadoCivilService().Save(estadocivil);

                    if (estadocivil.Id != 0)
                    {
                        this.TxtId.Text = Convert.ToString(estadocivil.Id);

                        Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                        MessageBox.Show("Estado Civil [" + this.TxtDescricao.Text + "] salvo com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        //Habilita controle para edição
        private void Editar_Click(object sender, EventArgs e)
        {
            Service.EnableControls(this);

            Service.AdjutsButtonsToEdition(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

        }

        //Exclui 
        private void Excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Confirma Exclusão?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        new EstadoCivilService().Delete(Convert.ToInt32(this.TxtId.Text));
                        this.Service.ClearForm(this);
                        this.Service.DisableControls(this);

                        Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

                        MessageBox.Show("Exclusão Confirmada!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        //Realiza cópia dos dados em tela
        private void Copiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Deseja Copiar este Estado Civil?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var estadocivil = new EstadoCivilService().GetById(Convert.ToInt32(this.TxtId.Text));
                        estadocivil.Descricao += " Cópia";
                        estadocivil.Id = 0;

                        new EstadoCivilService().Save(estadocivil);

                        this.TxtId.Text = estadocivil.Id.ToString();

                        this.Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

                        LoadForm();

                        MessageBox.Show("Cópia efetuada com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Abre formulário para pesquisa
        private void Pesquisar_Click(object sender, EventArgs e)
        {
            var form = new FormEstadoCivilBusca();

            form.ShowDialog();

            var estadocivil = form.EstadoCivil;

            if (estadocivil != null)
            {

                this.Service.ClearForm(this);
                this.Service.DisableControls(this);
                Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                this.TxtId.Text = estadocivil.Id.ToString();

                LoadForm();

            }
        }

        //Fecha Formulário
        private void Sair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
       
    }
}
