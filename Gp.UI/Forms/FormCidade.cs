﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gp.App.DTO;
using Gp.App.Services;

namespace Gp
{
    public partial class FormCidade : Form
    {
        FormsService Service;
        public UsuarioDTO UsuarioLogado { get; set; }

        public FormCidade()
        {
            InitializeComponent();

            //Instancia FormService
            Service = new FormsService();

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.DisableControls(this);

            this.WindowState = FormWindowState.Maximized;
            this.Refresh();
        }

        //Gerencia as teclas de atalho
        private void FormCidade_KeyDown(object sender, KeyEventArgs e)
        {
            Service.Keys(keyEvent: e, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar, BtnPesquisar: this.Pesquisar,
                BtnSair: this.Sair, BtnExcluir: this.Excluir);

        }

        //Inicialização do form com ajustes de botões
        private void FormCidade_Load(object sender, EventArgs e)
        {
            //Ajusta os botoes
            Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

        }

        //Preenche o formulário com os dados
        public void LoadForm()
        {
            try
            {
                var cidade = new CidadeService().GetById(Convert.ToInt32(this.TxtId.Text));


                this.TxtDescricao.Text = cidade.Descricao;                
                this.TxtEstadoId.Text = Convert.ToString(cidade.EstadoId);

                LoadEstado();

                ValidateForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Carrega o pais nos seus devidos campos do form
        public void LoadEstado()
        {
            var estado = new EstadoService().GetById(Convert.ToInt32(this.TxtEstadoId.Text));
            if (estado != null)
            {
                this.TxtEstadoId.Text = Convert.ToString(estado.Id);
                this.LblEstadoDescricao.Text = estado.Descricao;
                this.Errors.SetError(TxtEstadoId, "");
            }
            else
            {
                this.LblEstadoDescricao.Text = "";
                this.Errors.SetError(TxtEstadoId, "Código de Estado Inválido.");
            }


        }


        #region CamposForm
        private void TxtEstadoId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtEstadoId, "");
        }

        private void TxtEstadoId_Leave(object sender, EventArgs e)
        {
            if (this.TxtEstadoId.Text != string.Empty)
            {
                LoadEstado();
            }
        }
        #endregion

        #region ValidationsForm
        //Validações do Formulário Usuário
        public bool ValidateForm()
        {
            bool Validate = true;

            try
            {
                //Veryfy Text Nome
                if (this.TxtDescricao.Text == string.Empty)
                {
                    this.Errors.SetError(TxtDescricao, "Informe a Descrição do Estado");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtDescricao, "");
                }

               
                //Veryfy Estado
                if (this.TxtEstadoId.Text == string.Empty || this.LblEstadoDescricao.Text == string.Empty)
                {
                    this.Errors.SetError(TxtEstadoId, "Informe o Estado");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtEstadoId, "");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return Validate;

        }

        #endregion

        #region ButtonsMenu

        //Habilita nova inserção de usuário no formulário
        private void Novo_Click(object sender, EventArgs e)
        {
            //Verifica se formulário esta preenchido e em edição de algum estado 
            //Caso esteja questiona o usuário se deseja salvar as alterações realizadas ou não
            if (this.TxtId.Text != "" && GpCidade.Enabled == true)
            {
                if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Salvar.PerformClick();
                }
            }

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.EnableControls(this);

            //Desabilita os Botoes do form
            Service.ButtonsNewInsert(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

        }

        //Salva dados preenchido no formulário
        private void Salvar_Click(object sender, EventArgs e)
        {

            if (ValidateForm())
            {
                try
                {

                    CidadeDTO cidade = new CidadeDTO
                    {
                        Descricao = this.TxtDescricao.Text,                        
                        EstadoId = Convert.ToInt32(this.TxtEstadoId.Text)
                    };


                    if (this.TxtId.Text != "")
                    {
                        cidade.Id = Convert.ToInt32(this.TxtId.Text);
                    }



                    if (cidade.Id != 0)
                    {
                        if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                    }

                    new CidadeService().Save(cidade);

                    if (cidade.Id != 0)
                    {
                        this.TxtId.Text = Convert.ToString(cidade.Id);

                        Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                        MessageBox.Show("Cidade [" + this.TxtDescricao.Text + "] salva com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        //Habilita controle para edição
        private void Editar_Click(object sender, EventArgs e)
        {
            Service.EnableControls(this);

            Service.AdjutsButtonsToEdition(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

        }

        //Exclui 
        private void Excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Confirma Exclusão?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        new CidadeService().Delete(Convert.ToInt32(this.TxtId.Text));
                        this.Service.ClearForm(this);
                        this.Service.DisableControls(this);

                        Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

                        MessageBox.Show("Exclusão Confirmada!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        //Realiza cópia dos dados em tela
        private void Copiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Deseja Copiar esta Cidade ?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var cidade = new CidadeService().GetById(Convert.ToInt32(this.TxtId.Text));
                        cidade.Descricao += " Cópia";
                        cidade.Id = 0;
                        cidade.EstadoId = Convert.ToInt32(this.TxtEstadoId.Text);

                        new CidadeService().Save(cidade);

                        this.TxtId.Text = cidade.Id.ToString();

                        this.Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

                        LoadForm();

                        MessageBox.Show("Cópia efetuada com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Abre formulário para pesquisa
        private void Pesquisar_Click(object sender, EventArgs e)
        {
            var form = new FormCidadeBusca();

            form.ShowDialog();

            var cidade = form.Cidade;

            if (cidade != null)
            {

                this.Service.ClearForm(this);
                this.Service.DisableControls(this);
                Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                this.TxtId.Text = cidade.Id.ToString();

                LoadForm();

            }
        }

        //Fecha Formulário
        private void Sair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Botão de pesquisa de país
        private void BtnEstado_Click(object sender, EventArgs e)
        {
            var form = new FormEstadoBusca();

            form.ShowDialog();

            var estado = form.Estado;

            if (estado != null)
            {
                this.TxtEstadoId.Text = estado.Id.ToString();

                LoadEstado();

            }
        }

        #endregion
    }
}
