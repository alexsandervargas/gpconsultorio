﻿namespace Gp
{
    partial class FormCidade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCidade));
            this.TxtLog = new System.Windows.Forms.TextBox();
            this.GpCidade = new System.Windows.Forms.GroupBox();
            this.TxtEstadoId = new Gp.TextBoxForceNumber();
            this.LblEstadoDescricao = new System.Windows.Forms.Label();
            this.BtnEstado = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtDescricao = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.MenuForm = new System.Windows.Forms.ToolStrip();
            this.Novo = new System.Windows.Forms.ToolStripButton();
            this.Salvar = new System.Windows.Forms.ToolStripButton();
            this.Editar = new System.Windows.Forms.ToolStripButton();
            this.Excluir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.Copiar = new System.Windows.Forms.ToolStripButton();
            this.Pesquisar = new System.Windows.Forms.ToolStripButton();
            this.Sair = new System.Windows.Forms.ToolStripButton();
            this.Errors = new System.Windows.Forms.ErrorProvider(this.components);
            this.GpCidade.SuspendLayout();
            this.MenuForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Errors)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtLog
            // 
            this.TxtLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtLog.Enabled = false;
            this.TxtLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLog.Location = new System.Drawing.Point(0, 26);
            this.TxtLog.Name = "TxtLog";
            this.TxtLog.ReadOnly = true;
            this.TxtLog.Size = new System.Drawing.Size(800, 21);
            this.TxtLog.TabIndex = 13;
            // 
            // GpCidade
            // 
            this.GpCidade.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GpCidade.Controls.Add(this.TxtEstadoId);
            this.GpCidade.Controls.Add(this.LblEstadoDescricao);
            this.GpCidade.Controls.Add(this.BtnEstado);
            this.GpCidade.Controls.Add(this.label3);
            this.GpCidade.Controls.Add(this.TxtDescricao);
            this.GpCidade.Controls.Add(this.label2);
            this.GpCidade.Controls.Add(this.TxtId);
            this.GpCidade.Controls.Add(this.label1);
            this.GpCidade.Location = new System.Drawing.Point(12, 53);
            this.GpCidade.Name = "GpCidade";
            this.GpCidade.Size = new System.Drawing.Size(776, 138);
            this.GpCidade.TabIndex = 12;
            this.GpCidade.TabStop = false;
            this.GpCidade.Text = "Dados da Cidade";
            // 
            // TxtEstadoId
            // 
            this.TxtEstadoId.Location = new System.Drawing.Point(129, 73);
            this.TxtEstadoId.Name = "TxtEstadoId";
            this.TxtEstadoId.Size = new System.Drawing.Size(100, 20);
            this.TxtEstadoId.TabIndex = 3;
            this.TxtEstadoId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtEstadoId.TextChanged += new System.EventHandler(this.TxtEstadoId_TextChanged);
            this.TxtEstadoId.Leave += new System.EventHandler(this.TxtEstadoId_Leave);
            // 
            // LblEstadoDescricao
            // 
            this.LblEstadoDescricao.AutoSize = true;
            this.LblEstadoDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEstadoDescricao.Location = new System.Drawing.Point(265, 76);
            this.LblEstadoDescricao.Name = "LblEstadoDescricao";
            this.LblEstadoDescricao.Size = new System.Drawing.Size(51, 15);
            this.LblEstadoDescricao.TabIndex = 8;
            this.LblEstadoDescricao.Text = "Estado";
            // 
            // BtnEstado
            // 
            this.BtnEstado.Location = new System.Drawing.Point(230, 73);
            this.BtnEstado.Name = "BtnEstado";
            this.BtnEstado.Size = new System.Drawing.Size(29, 21);
            this.BtnEstado.TabIndex = 4;
            this.BtnEstado.Text = "...";
            this.BtnEstado.UseVisualStyleBackColor = true;
            this.BtnEstado.Click += new System.EventHandler(this.BtnEstado_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(67, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Estado";
            // 
            // TxtDescricao
            // 
            this.TxtDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDescricao.Location = new System.Drawing.Point(129, 46);
            this.TxtDescricao.MaxLength = 100;
            this.TxtDescricao.Name = "TxtDescricao";
            this.TxtDescricao.Size = new System.Drawing.Size(361, 21);
            this.TxtDescricao.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(47, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Descrição";
            // 
            // TxtId
            // 
            this.TxtId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtId.Location = new System.Drawing.Point(129, 20);
            this.TxtId.Name = "TxtId";
            this.TxtId.ReadOnly = true;
            this.TxtId.Size = new System.Drawing.Size(100, 21);
            this.TxtId.TabIndex = 0;
            this.TxtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(72, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Código";
            // 
            // MenuForm
            // 
            this.MenuForm.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.MenuForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Novo,
            this.Salvar,
            this.Editar,
            this.Excluir,
            this.toolStripSeparator,
            this.Copiar,
            this.Pesquisar,
            this.Sair});
            this.MenuForm.Location = new System.Drawing.Point(0, 0);
            this.MenuForm.Name = "MenuForm";
            this.MenuForm.Size = new System.Drawing.Size(800, 25);
            this.MenuForm.TabIndex = 11;
            this.MenuForm.Text = "toolStrip1";
            // 
            // Novo
            // 
            this.Novo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Novo.Image = ((System.Drawing.Image)(resources.GetObject("Novo.Image")));
            this.Novo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Novo.Name = "Novo";
            this.Novo.Size = new System.Drawing.Size(23, 22);
            this.Novo.Text = "Novo - F1";
            this.Novo.Click += new System.EventHandler(this.Novo_Click);
            // 
            // Salvar
            // 
            this.Salvar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Salvar.Enabled = false;
            this.Salvar.Image = ((System.Drawing.Image)(resources.GetObject("Salvar.Image")));
            this.Salvar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Salvar.Name = "Salvar";
            this.Salvar.Size = new System.Drawing.Size(23, 22);
            this.Salvar.Text = "Salvar - F6";
            this.Salvar.Click += new System.EventHandler(this.Salvar_Click);
            // 
            // Editar
            // 
            this.Editar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Editar.Enabled = false;
            this.Editar.Image = ((System.Drawing.Image)(resources.GetObject("Editar.Image")));
            this.Editar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Editar.Name = "Editar";
            this.Editar.Size = new System.Drawing.Size(23, 22);
            this.Editar.Text = "Editar - F2";
            this.Editar.Click += new System.EventHandler(this.Editar_Click);
            // 
            // Excluir
            // 
            this.Excluir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Excluir.Enabled = false;
            this.Excluir.Image = ((System.Drawing.Image)(resources.GetObject("Excluir.Image")));
            this.Excluir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Excluir.Name = "Excluir";
            this.Excluir.Size = new System.Drawing.Size(23, 22);
            this.Excluir.Text = "Excluir - F3";
            this.Excluir.Click += new System.EventHandler(this.Excluir_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // Copiar
            // 
            this.Copiar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Copiar.Enabled = false;
            this.Copiar.Image = ((System.Drawing.Image)(resources.GetObject("Copiar.Image")));
            this.Copiar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Copiar.Name = "Copiar";
            this.Copiar.Size = new System.Drawing.Size(23, 22);
            this.Copiar.Text = "&Copiar";
            this.Copiar.Click += new System.EventHandler(this.Copiar_Click);
            // 
            // Pesquisar
            // 
            this.Pesquisar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Pesquisar.Image = ((System.Drawing.Image)(resources.GetObject("Pesquisar.Image")));
            this.Pesquisar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Pesquisar.Name = "Pesquisar";
            this.Pesquisar.Size = new System.Drawing.Size(23, 22);
            this.Pesquisar.Text = "Pesquisar - F4";
            this.Pesquisar.Click += new System.EventHandler(this.Pesquisar_Click);
            // 
            // Sair
            // 
            this.Sair.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Sair.Image = ((System.Drawing.Image)(resources.GetObject("Sair.Image")));
            this.Sair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Sair.Name = "Sair";
            this.Sair.Size = new System.Drawing.Size(23, 22);
            this.Sair.Text = "Sair - Esc";
            this.Sair.Click += new System.EventHandler(this.Sair_Click);
            // 
            // Errors
            // 
            this.Errors.ContainerControl = this;
            this.Errors.RightToLeft = true;
            // 
            // FormCidade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 203);
            this.Controls.Add(this.TxtLog);
            this.Controls.Add(this.GpCidade);
            this.Controls.Add(this.MenuForm);
            this.KeyPreview = true;
            this.Name = "FormCidade";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Cadastro de Cidade";
            this.Load += new System.EventHandler(this.FormCidade_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormCidade_KeyDown);
            this.GpCidade.ResumeLayout(false);
            this.GpCidade.PerformLayout();
            this.MenuForm.ResumeLayout(false);
            this.MenuForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Errors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtLog;
        private System.Windows.Forms.GroupBox GpCidade;
        private Gp.TextBoxForceNumber TxtEstadoId;
        private System.Windows.Forms.Label LblEstadoDescricao;
        private System.Windows.Forms.Button BtnEstado;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtDescricao;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtId;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ToolStrip MenuForm;
        private System.Windows.Forms.ToolStripButton Novo;
        private System.Windows.Forms.ToolStripButton Salvar;
        private System.Windows.Forms.ToolStripButton Editar;
        private System.Windows.Forms.ToolStripButton Excluir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton Copiar;
        private System.Windows.Forms.ToolStripButton Pesquisar;
        private System.Windows.Forms.ToolStripButton Sair;
        private System.Windows.Forms.ErrorProvider Errors;
    }
}