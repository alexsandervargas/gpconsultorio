﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gp.App.DTO;
using Gp.App.Services;

namespace Gp
{
    public partial class FormEstado : Form
    {
        FormsService Service;
        public UsuarioDTO UsuarioLogado { get; set; }

        public FormEstado()
        {
            InitializeComponent();

            //Instancia FormService
            Service = new FormsService();

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.DisableControls(this);

            this.WindowState = FormWindowState.Maximized;
            this.Refresh();
        }

        //Gerencia as teclas de atalho
        private void FormEstado_KeyDown(object sender, KeyEventArgs e)
        {
            Service.Keys(keyEvent: e, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar, BtnPesquisar: this.Pesquisar,
                BtnSair: this.Sair, BtnExcluir: this.Excluir);

        }

        //Inicialização do form com ajustes de botões
        private void FormEstado_Load(object sender, EventArgs e)
        {
            //Ajusta os botoes
            Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar, BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

        }

        //Preenche o formulário com os dados
        public void LoadForm()
        {
            try
            {
                var estado = new EstadoService().GetById(Convert.ToInt32(this.TxtId.Text));


                this.TxtDescricao.Text = estado.Descricao;
                this.TxtSigla.Text = estado.Sigla;
                this.TxtPaisId.Text = Convert.ToString(estado.PaisId);

                LoadPais();

                ValidateForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Carrega o pais nos seus devidos campos do form
        public void LoadPais()
        {
            var pais = new PaisService().GetById(Convert.ToInt32(this.TxtPaisId.Text));
            if (pais != null)
            {
                this.TxtPaisId.Text = Convert.ToString(pais.Id);
                this.LblPaisDescricao.Text = pais.Descricao;
                this.Errors.SetError(TxtPaisId, "");
            }
            else
            {
                this.LblPaisDescricao.Text = "";
                this.Errors.SetError(TxtPaisId, "Código de País Inválido.");               
            }


        }


        #region CamposForm
        private void TxtPaisId_TextChanged(object sender, EventArgs e)
        {
            this.Errors.SetError(TxtPaisId, "");
        }

        private void TxtPaisId_Leave(object sender, EventArgs e)
        {
            if (this.TxtPaisId.Text != string.Empty)
            {
                LoadPais();
            }
        }
        #endregion

        #region ValidationsForm
        //Validações do Formulário Usuário
        public bool ValidateForm()
        {
            bool Validate = true;

            try
            {
                //Veryfy Text Nome
                if (this.TxtDescricao.Text == string.Empty)
                {
                    this.Errors.SetError(TxtDescricao, "Informe a Descrição do Estado");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtDescricao, "");
                }

                //Veryfy Text Sigla
                if (this.TxtSigla.Text == string.Empty)
                {
                    this.Errors.SetError(TxtSigla, "Informe a Sigla do Estado");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtSigla, "");
                }


                //Veryfy Pais
                if (this.TxtPaisId.Text == string.Empty || this.LblPaisDescricao.Text == string.Empty)
                {
                    this.Errors.SetError(TxtPaisId, "Informe o País");
                    Validate = false;
                }
                else
                {
                    this.Errors.SetError(TxtPaisId, "");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return Validate;

        }

        #endregion

        #region ButtonsMenu

        //Habilita nova inserção de usuário no formulário
        private void Novo_Click(object sender, EventArgs e)
        {
            //Verifica se formulário esta preenchido e em edição de algum estado 
            //Caso esteja questiona o usuário se deseja salvar as alterações realizadas ou não
            if (this.TxtId.Text != "" && GpEstado.Enabled == true)
            {
                if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Salvar.PerformClick();
                }
            }

            //Executa Limpeza dos Campos
            Service.ClearForm(this);

            //Desabilita os Campos do Formulário
            Service.EnableControls(this);

            //Desabilita os Botoes do form
            Service.ButtonsNewInsert(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

        }

        //Salva dados preenchido no formulário
        private void Salvar_Click(object sender, EventArgs e)
        {

            if (ValidateForm())
            {
                try
                {

                    EstadoDTO estado = new EstadoDTO
                    {
                        Descricao = this.TxtDescricao.Text,
                        Sigla= this.TxtSigla.Text,
                        PaisId = Convert.ToInt32(this.TxtPaisId.Text)
                    };


                    if (this.TxtId.Text != "")
                    {
                        estado.Id = Convert.ToInt32(this.TxtId.Text);
                    }



                    if (estado.Id != 0)
                    {
                        if (MessageBox.Show("Deseja Salvar as Alterações?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                    }

                    new EstadoService().Save(estado);

                    if (estado.Id != 0)
                    {
                        this.TxtId.Text = Convert.ToString(estado.Id);

                        Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                        MessageBox.Show("Estado [" + this.TxtDescricao.Text + "] salvo com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        //Habilita controle para edição
        private void Editar_Click(object sender, EventArgs e)
        {
            Service.EnableControls(this);

            Service.AdjutsButtonsToEdition(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

        }

        //Exclui 
        private void Excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Confirma Exclusão?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        new EstadoService().Delete(Convert.ToInt32(this.TxtId.Text));
                        this.Service.ClearForm(this);
                        this.Service.DisableControls(this);

                        Service.ButtonsLoadForm(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);

                        MessageBox.Show("Exclusão Confirmada!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        //Realiza cópia dos dados em tela
        private void Copiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TxtId.Text != "")
                {
                    if (MessageBox.Show("Deseja Copiar este Estado ?", "GP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var estado = new EstadoService().GetById(Convert.ToInt32(this.TxtId.Text));
                        estado.Descricao += " Cópia";
                        estado.Id = 0;
                        estado.PaisId = Convert.ToInt32(this.TxtPaisId.Text);

                        new EstadoService().Save(estado);

                        this.TxtId.Text = estado.Id.ToString();

                        this.Service.DisableControls(this);

                        Service.AdjutsButtonsToSearchAndSave(
                            usuario: UsuarioLogado,
                            Form: this,
                            BtnNovo: this.Novo,
                            BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar,
                            BtnCopiar: this.Copiar,
                            BtnExcluir: this.Excluir);

                        LoadForm();

                        MessageBox.Show("Cópia efetuada com sucesso!", "GP", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "GP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Abre formulário para pesquisa
        private void Pesquisar_Click(object sender, EventArgs e)
        {
            var form = new FormEstadoBusca();

            form.ShowDialog();

            var estado = form.Estado;

            if (estado != null)
            {

                this.Service.ClearForm(this);
                this.Service.DisableControls(this);
                Service.AdjutsButtonsToSearchAndSave(usuario: UsuarioLogado, Form: this, BtnNovo: this.Novo, BtnSalvar: this.Salvar,
                            BtnEditar: this.Editar, BtnCopiar: this.Copiar, BtnExcluir: this.Excluir);


                this.TxtId.Text = estado.Id.ToString();

                LoadForm();

            }
        }

        //Fecha Formulário
        private void Sair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Botão de pesquisa de país
        private void BtnPais_Click(object sender, EventArgs e)
        {
            var form = new FormPaisBusca();

            form.ShowDialog();

            var pais = form.Pais;

            if (pais != null)
            {               
                this.TxtPaisId.Text = pais.Id.ToString();

                LoadPais();

            }
        }


        #endregion

        private void TxtDescricao_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
