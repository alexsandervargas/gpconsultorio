USE [Gpconsultorio]
GO

/****** Object:  View [dbo].[VW_SEL_PACIENTE]    Script Date: 12/02/2019 10:54:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[VW_SEL_PACIENTE] AS
/*Autor: Paulo Alexsander 
  Em: 12/02/2018 
*/
SELECT 

PE.CodPessoa									AS CodPessoa
,PE.Nome										AS Nome
,CASE WHEN PE.CodigoAreaResidencial = 0 
	THEN NULL ELSE	PE.CodigoAreaResidencial 
	END											AS DDDResidencial
,PE.TelefoneResidencial							AS FoneResidencial
,CASE WHEN PE.CodigoAreaCelular = 0 
	THEN NULL ELSE	PE.CodigoAreaCelular 
	END											AS DDDCelular
,PE.TelefoneCelular								AS FoneCelular
,CASE WHEN PE.CodigoAreaComercial	 = 0 
	THEN NULL ELSE PE.CodigoAreaComercial	
	END											AS DDDComercial
,PE.TelefoneComercial							AS FoneComercial
,PE.Email										AS Email
,PE.Logradouro									AS Logradouro
,PE.Numero										AS Numero
,PE.Complemento									AS Complemento
,PE.Bairro										AS Bairro
,PE.CEP											AS CEP
,PE.Observacao									AS Observaçoes
,PE.CodCidade									AS CodCidade
,CID.Descricao									AS Cidade
,UF.CodEstado									AS CodEstado
,UF.Descricao									AS Estado
,UF.Sigla										AS UF
,PAIS.Descricao									AS Pais
,PE.DataCadastro								AS DtCadastro
,PE.DataAtualizacao								AS DtAtualizacao
,PF.CodEstadoCivil								AS CodEstadoCivil
,EC.Descricao									AS EstadoCivil
,PF.CodSexo										AS CodSexo
,S.Descricao									AS Sexo
,PF.Nascimento									AS DtNascimento
,PF.RG											AS RG
,PF.CPF											AS CPF
,PF.CodProfissao								AS CodProfissao
,PROF.Descricao									AS Profissao
,PA.CodIndicacao								AS CodIndicacao
,IND.Tipo										AS TipoIndicacao
,IND.CodPessoaFisica							AS CodPessoaIndicou
,IND.DataCadastro								AS DataIndicacao
,COALESCE(PEI.NOME,IND.NomeIndicacao)			AS NomeIndicador
,PA.CodPlano									AS CodPlano
,PL.Descricao									AS Plano
,PA.Matricula									AS Matricula
,PA.Ficha										AS NumFicha
,PJ.NomeFantasia								AS NomeFantasia
,PEJ.Nome										AS RazaoSocial
,PJ.CNPJ										AS CNPJ
,PJ.InscricaoEstadual							AS InscricaoEstadual

 FROM Pacientes PA
	INNER JOIN PessoasFisicas PF
		ON PF.CodPessoaFisica=PA.CodPaciente
	INNER JOIN Pessoas PE
		ON PF.CodPessoaFisica=PE.CodPessoa
	LEFT JOIN Sexos S
		ON PF.CodSexo=S.CodSexo
	LEFT JOIN EstadosCivis EC
		ON EC.CodEstadoCivil=PF.CodEstadoCivil
	LEFT JOIN Profissoes PROF
		ON PROF.CodProfissao=PF.CodProfissao
	LEFT JOIN Cidades CID
		ON PE.CodCidade=CID.CodCidade
	LEFT JOIN Estados UF
		ON UF.CodEstado=CID.CodEstado
	LEFT JOIN Paises PAIS
		ON PAIS.CodPais=UF.CodPais
	LEFT JOIN Indicacoes IND
		ON IND.CodIndicacao=PA.CodIndicacao
	LEFT JOIN PessoasFisicas PFI
		ON IND.CodPessoaFisica=PFI.CodPessoaFisica
	LEFT JOIN Pessoas PEI
		ON PFI.CodPessoaFisica=PEI.CodPessoa
	LEFT JOIN Planos PL
		ON PL.CodPlano=PA.CodPlano
	LEFT JOIN PessoasJuridicas PJ 
		ON PJ.CodPessoaJuridica=PL.CodEmpresa
	LEFT JOIN Pessoas PEJ
		ON PJ.CodPessoaJuridica=PEJ.CodPessoa
GO


