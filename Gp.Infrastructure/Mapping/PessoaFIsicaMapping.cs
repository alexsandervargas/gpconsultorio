﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
    public class PessoaFisicaMapping : EntityTypeConfiguration<PessoaFisicaModel>
    {
        public PessoaFisicaMapping()
        {
            ToTable("PessoasFisicas");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodPessoaFisica");
            Property(u => u.Nome).IsRequired().HasMaxLength(60);
            Property(u => u.CPF).IsOptional().HasMaxLength(14);
            Property(u => u.RG).IsOptional().HasMaxLength(20);
            Property(u => u.Observacao).IsOptional().HasMaxLength(1500);
            HasOptional(u => u.Sexo).WithMany().Map(m => m.MapKey("CodSexo"));
            HasOptional(u => u.EstadoCivil).WithMany().Map(m => m.MapKey("CodEstadoCivil"));
            HasOptional(u => u.Profissao).WithMany().Map(m => m.MapKey("CodProfissao"));
            HasMany<IndicacaoModel>(u => u.Indicacoes).WithMany();
        }
    }
}
