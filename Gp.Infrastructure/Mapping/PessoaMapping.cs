﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
    public class PessoaMapping:EntityTypeConfiguration<PessoaModel>
    {
        public PessoaMapping()
        {
            ToTable("Pessoas");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodPessoa");
            Property(u => u.Email).IsOptional();
            Property(u => u.CodigoAreaResidencial).IsOptional();
            Property(u => u.TelefoneResidencial).IsOptional().HasMaxLength(50);
            Property(u => u.CodigoAreaCelular).IsOptional();
            Property(u => u.TelefoneCelular).IsOptional().HasMaxLength(50);
            Property(u => u.CodigoAreaComercial).IsOptional();
            Property(u => u.TelefoneComercial).IsOptional().HasMaxLength(50);
            Property(u => u.Logradouro).IsOptional();
            Property(u => u.Numero).IsOptional();
            Property(u => u.Complemento).IsOptional();
            Property(u => u.CEP).IsOptional().HasMaxLength(9);
            HasOptional(u => u.Cidade).WithMany().Map(m => m.MapKey("CodCidade"));
            Property(u => u.Bairro).IsOptional();
        }
    }
}
