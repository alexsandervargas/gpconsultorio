﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
    public class PaisMapping : EntityTypeConfiguration<PaisModel>
    {
        public PaisMapping()
        {
            ToTable("Paises");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodPais");
            Property(u => u.Descricao).IsRequired();
            HasMany<EstadoModel>(u => u.Estados).WithRequired(t => t.Pais).Map(m => m.MapKey("CodPais"));

        }
    }
}
