﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;


namespace Gp.Infrastructure.Mapping
{
    public class CidadeMapping : EntityTypeConfiguration<CidadeModel>
    {
        public CidadeMapping()
        {
            ToTable("Cidades");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodCidade");
            Property(u => u.Descricao).IsRequired();
            HasRequired(u => u.Estado).WithMany().Map(m => m.MapKey("CodEstado"));
        }
    }
}
