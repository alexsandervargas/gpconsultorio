﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
    public class PacienteMapping : EntityTypeConfiguration<PacienteModel>
    {
        public PacienteMapping()
        {
            ToTable("Pacientes");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodPaciente");
            HasOptional(u => u.Plano).WithMany().Map(m => m.MapKey("CodPlano"));
            Property(u => u.Matricula).HasColumnName("Matricula").IsOptional() ;
            Property(u => u.Ficha).HasColumnName("Ficha").IsOptional();
            HasOptional(u => u.Indicacao).WithMany().Map(m => m.MapKey("CodIndicacao"));
            Property(u => u.Empresa).HasColumnName("Empresa").HasMaxLength(200);
                                                                
                                                
        }
    }
}
