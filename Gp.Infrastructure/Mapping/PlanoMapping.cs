﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
   public  class PlanoMapping :EntityTypeConfiguration<PlanoModel>
    {
        public PlanoMapping()
        {
            ToTable("Planos");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodPlano");
            HasRequired(u=>u.PessoaJuridica).WithMany().Map(m => m.MapKey("CodEmpresa"));
            Property(u => u.Descricao).IsRequired();
            Property(u => u.Ativo).IsRequired();
            Property(u => u.Cooparticipacao).IsRequired();
            Property(u => u.Observacao).IsOptional().HasMaxLength(1500);

        }
    }
}
