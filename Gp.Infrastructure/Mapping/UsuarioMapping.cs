﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
    public class UsuarioMapping : EntityTypeConfiguration<UsuarioModel>
    {
        public UsuarioMapping()
        {
            ToTable("Usuarios");

            HasKey(u => u.Id);

            Property(u => u.Id);
            Property(u => u.Nome).IsRequired();
            Property(u => u.Email).IsRequired();
            Property(u => u.Login).IsRequired();
            Property(u => u.Senha).IsRequired();
            Property(u => u.Ativo).IsRequired();

        }
    }
}
