﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
    public class EstadoCivilMapping : EntityTypeConfiguration<EstadoCivilModel>
    {
        public EstadoCivilMapping()
        {
            ToTable("EstadosCivis");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodEstadoCivil");
            Property(u => u.Descricao).IsRequired();

        }
    }
}
