﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
    public class ProfissionalMapping : EntityTypeConfiguration<ProfissionalModel>
    {
        public ProfissionalMapping()
        {
            ToTable("Profissionais");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodProfissional");
           
        }
    }
}
