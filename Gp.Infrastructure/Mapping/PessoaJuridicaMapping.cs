﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
    public class PessoaJuridicaMapping: EntityTypeConfiguration<PessoaJuridicaModel>
    {
        public PessoaJuridicaMapping()
        {
            ToTable("PessoasJuridicas");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodPessoaJuridica");
            Property(u => u.Nome).IsRequired().HasMaxLength(60);
            Property(u => u.NomeFantasia).IsRequired().HasMaxLength(60);
            Property(u => u.CNPJ).IsRequired().HasMaxLength(18);
            Property(u => u.InscricaoEstadual).IsOptional();               
            HasMany<PlanoModel>(u => u.Planos).WithRequired(p => p.PessoaJuridica);
           
        }
    }
}
