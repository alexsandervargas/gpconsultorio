﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
    class EstadoMapping : EntityTypeConfiguration<EstadoModel>
    {
        public EstadoMapping()
        {
            ToTable("Estados");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodEstado");
            Property(u => u.Descricao).IsRequired();
            Property(u => u.Sigla).IsRequired().HasMaxLength(2);
            HasRequired(u => u.Pais).WithMany().Map(m => m.MapKey("CodPais"));

        }

    }
}
