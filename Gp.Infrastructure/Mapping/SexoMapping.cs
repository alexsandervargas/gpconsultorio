﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
    public class SexoMapping :EntityTypeConfiguration<SexoModel>
    {
        public SexoMapping()
        {
            ToTable("Sexos");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodSexo");
            Property(u => u.Descricao).IsRequired();
            
        }
    }
}
