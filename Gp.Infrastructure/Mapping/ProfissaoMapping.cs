﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
    public class ProfissaoMapping : EntityTypeConfiguration<ProfissaoModel>
    {
        public ProfissaoMapping()
        {
            ToTable("Profissoes");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodProfissao");
            Property(u => u.Descricao).IsRequired();
            HasMany<PessoaFisicaModel>(c => c.Profissionais).WithOptional(c => c.Profissao).Map(m => m.MapKey("CodProfissao"));
        }
    }
}
