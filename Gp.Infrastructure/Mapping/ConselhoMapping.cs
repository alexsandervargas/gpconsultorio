﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
    public class ConselhoMapping :EntityTypeConfiguration<ConselhoModel>
    {
        public ConselhoMapping()
        {
            ToTable("Conselhos");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodConselho");
            Property(u => u.Descricao).IsRequired();
            HasMany<ProfissionalModel>(c => c.Profissionais).WithRequired(c=>c.Conselho).Map(m=>m.MapKey("CodConselho"));
        }
    }
}
