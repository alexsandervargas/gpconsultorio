﻿using Gp.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Gp.Infrastructure.Mapping
{
    class IndicacaoMapping : EntityTypeConfiguration<IndicacaoModel>
    {

        public IndicacaoMapping()
        {
            ToTable("Indicacoes");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("CodIndicacao");
            Property(u => u.NomeIndicacao).IsOptional().HasColumnName("NomeIndicacao").HasMaxLength(60);
            Property(u => u.Tipo).IsRequired().HasColumnName("Tipo");
            HasOptional(u => u.PessoaIndicou).WithMany(x=>x.Indicacoes).Map(m => m.MapKey("CodPessoaFisica"));
            
        }
    }
}
