﻿using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using MySql.Data.MySqlClient;

namespace Gp.Infrastructure.Repository
{
    public class PacienteRepository : Repository<PacienteModel>
    {
        public PacienteRepository(GpContext context) : base(context)
        {

        }

        public DataTable ReportFicha(int id)
        {
            string StrConn = System.Configuration.ConfigurationManager.ConnectionStrings["Dbase1"].ToString();
            string Provider = System.Configuration.ConfigurationManager.ConnectionStrings["Dbase1"].ProviderName;


            var Query = "SELECT * FROM VW_SEL_PACIENTE WHERE CodPessoa= " + id.ToString();

            DataTable table = new DataTable();

            if (Provider.Contains("MySql"))
            {
                MySqlConnection MConn = new MySqlConnection(StrConn);
                var cmd = MConn.CreateCommand();
                cmd.CommandText = Query;
                MConn.Open();
                cmd.ExecuteNonQuery();

                MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);

                adapter.Fill(table);

                MConn.Close();


            }
            else
            {

                SqlConnection Conn = new SqlConnection(StrConn);
                Conn.Open();

                SqlCommand Cmd = new SqlCommand
                {
                    Connection = Conn,
                    CommandText = Query,
                };

                SqlDataAdapter adapter = new SqlDataAdapter(Cmd);

                adapter.Fill(table);

                Conn.Close();


            }



            return table;

        }

        public DataTable ReportAniversariantes(DateTime Inicial, DateTime Final)
        {
            string StrConn = System.Configuration.ConfigurationManager.ConnectionStrings["Dbase1"].ToString();
            string Provider = System.Configuration.ConfigurationManager.ConnectionStrings["Dbase1"].ProviderName;


            var Query = "SELECT *, (DATEPART(YEAR, GETDATE()) - DATEPART(YEAR, V.DtNascimento)) AS IDADE ";
            Query += "FROM VW_SEL_PACIENTE V ";
            Query += "WHERE((DATEPART(DAY, V.DtNascimento) >= " + Inicial.Day.ToString();
            Query += "AND DATEPART(MONTH, V.DtNascimento) >= " + Inicial.Month.ToString();
            Query += ") AND(DATEPART(DAY, V.DtNascimento) <= " + Final.Day.ToString();
            Query += " AND DATEPART(MONTH, V.DtNascimento) <= " + Final.Month.ToString();
            Query += ")) ORDER BY V.DtNascimento";

            DataTable table = new DataTable();

            if (Provider.Contains("MySql"))
            {
                MySqlConnection MConn = new MySqlConnection(StrConn);
                var cmd = MConn.CreateCommand();
                cmd.CommandText = Query;
                MConn.Open();
                cmd.ExecuteNonQuery();

                MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);

                adapter.Fill(table);

                MConn.Close();


            }
            else
            {

                SqlConnection Conn = new SqlConnection(StrConn);
                Conn.Open();

                SqlCommand Cmd = new SqlCommand
                {
                    Connection = Conn,
                    CommandText = Query,
                };

                SqlDataAdapter adapter = new SqlDataAdapter(Cmd);

                adapter.Fill(table);

                Conn.Close();


            }



            return table;

        }

        public IList<PacienteModel> GetAllInformation()
        {
            var pessoas = this.DbSet
                .Include(x => x.EstadoCivil)
                .Include(x => x.Sexo)
                .Include(x => x.Cidade)
                .Include(x => x.Profissao)
                .Include(x => x.Indicacao)
                .Include(x => x.Plano).ToList();

            return pessoas;

        }

        public PacienteModel GetByIdAllInformation(int id)
        {
            var pessoa = this.DbSet
                .Include(x => x.EstadoCivil)
                .Include(x => x.Sexo)
                .Include(x => x.Cidade)
                .Include(x => x.Profissao)
                .Include(x => x.Indicacao)
                .Include(x => x.Plano)
                .FirstOrDefault(w => w.Id == id);

            return pessoa;
        }

        public PacienteModel GetByIdBusca(int id)
        {
            var pessoa = this.DbSet
                .Include(x => x.Cidade)
                .FirstOrDefault(w => w.Id == id);
            return pessoa;
        }

        public IList<PacienteModel> GetIndicacoes(int PessoaIndicouId)
        {
            var Lista = this.DbSet.Where(x => x.Indicacao.PessoaIndicou.Id == PessoaIndicouId).ToList();

            return Lista;
        }


    }
}
