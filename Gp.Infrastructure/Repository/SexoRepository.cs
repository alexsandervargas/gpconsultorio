﻿using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace Gp.Infrastructure.Repository
{
    public class SexoRepository :Repository<SexoModel>
    {
        public SexoRepository(GpContext context):base(context)
        {

        }
    }
}
