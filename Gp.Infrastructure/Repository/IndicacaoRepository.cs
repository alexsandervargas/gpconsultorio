﻿using System.Collections.Generic;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using System.Data.Entity;
using System.Linq;
using System;
using System.Linq.Expressions;

namespace Gp.Infrastructure.Repository
{
    public class IndicacaoRepository : Repository<IndicacaoModel>
    {
        public IndicacaoRepository(GpContext context) : base(context)
        {

        }

        public IList<IndicacaoModel> GetAllInformation()
        {
            var Indicacoes = this.DbSet.Include(x => x.PessoaIndicou).ToList();

            return Indicacoes;

        }

        public IndicacaoModel GetWithPaciente(int id)
        {
            var indicacao = this.DbSet.Include(x => x.PessoaIndicou).FirstOrDefault(w => w.Id == id);
       
            return indicacao;
        }

        public IList<IndicacaoModel> GetWithIndicacoes(int PessoaIndicouId)
        {
            var Indicacoes = this.DbSet.Include(x => x.PessoaIndicou).Where(i => i.PessoaIndicou.Id == PessoaIndicouId).ToList();

            return Indicacoes;
        }


    }
}
