﻿using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Gp.Infrastructure.Repository
{
    public class PessoaJuridicaRepository : Repository<PessoaJuridicaModel>
    {
        public PessoaJuridicaRepository( GpContext context) : base(context)
        {

        }

        public IList<PessoaJuridicaModel> GetAllInformation()
        {
            var pessoas = this.DbSet.Include(x=>x.Cidade).ToList();

            return pessoas;

        }

       

        public PessoaJuridicaModel GetByIdAllInformation(int id)
        {
            var pessoa = this.DbSet.Include(x => x.Cidade).FirstOrDefault(w => w.Id == id);

            return pessoa;
        }

    }
}
