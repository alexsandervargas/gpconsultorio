﻿using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gp.Infrastructure.Repository
{
    public class PessoaRepository: Repository<PessoaModel>
    {
        public PessoaRepository(GpContext context):base(context)
        {

        }
    }
}
