﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using Gp.Domain.Models;
using Gp.Domain.Interfaces;
using Gp.Infrastructure.Context;
using System.Reflection;

namespace Gp.Infrastructure.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : EntityModel
    {
        protected GpContext Db;
        protected DbSet<TEntity> DbSet;

        public Repository(GpContext context)
        {
            Db = context;
            DbSet = Db.Set<TEntity>();
        }

        public virtual void Add(TEntity obj)
        {
            DbSet.Add(obj);
        }

        public virtual TEntity GetById(long id)
        {
            return DbSet.Find(id);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return DbSet.ToList();
        }

        public virtual void Update(TEntity obj)
        {
            DbSet.AddOrUpdate(obj);
        }
        

        public TEntity Get(Expression<Func<TEntity, bool>> @where)
        {
            return Db.Set<TEntity>().Where(where).FirstOrDefault<TEntity>();
        }


        public virtual void Remove(long id)
        {
            DbSet.Remove(DbSet.Find(id));
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.AsNoTracking().Where(predicate);
        }

        public IQueryable<TEntity> SearchExact(string keyword,Func<TEntity, string> getSearchField)
        {
            return Db.Set<TEntity>().Where(i => getSearchField(i) == keyword);
        }


        public int SaveChanges()
        {
            return Db.SaveChanges();
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.SuppressFinalize(this);
        }




    }
}
