﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;

namespace Gp.Infrastructure.Repository
{
    public class ProfissaoRepository : Repository<ProfissaoModel>
    {
        public ProfissaoRepository(GpContext context) : base(context)
        {

        }
    }
}
