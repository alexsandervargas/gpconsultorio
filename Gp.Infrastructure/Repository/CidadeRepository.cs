﻿using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;



namespace Gp.Infrastructure.Repository
{
    public class CidadeRepository : Repository<CidadeModel>
    {
        public CidadeRepository(GpContext context):base(context)
        {

        }


        public IList<CidadeModel> GetAllInformation()
        {
            var cidades = this.DbSet.Include(x=>x.Estado).ToList();

            return cidades;
            
        }

        public CidadeModel GetWithEstado(int id)
        {
            var cidade = this.DbSet.Include(x => x.Estado).FirstOrDefault(w => w.Id == id);

            return cidade;
        }

        public CidadeModel GetWithEstadoByName(string nomecidade,string UF)
        {
            var listacidade = this.DbSet.Include(x => x.Estado).Where(c => c.Descricao.ToLower().Contains( nomecidade.ToLower().TrimStart())).ToList();//.Where(e=>e.Estado.Sigla.ToUpper()==UF.ToUpper()).First();

            var cidade = listacidade.FirstOrDefault(x => x.Estado.Sigla.ToUpper() == UF.ToUpper().TrimStart());
            
            return cidade;
        }





        //public Cliente GetWithCatagoria(int id)
        //{

        //    var cliente = this.DbSet.Include(x => x.Categoria).FirstOrDefault(w => w.Id == id);

        //    var cliente1 = this.DbSet.Include(x => x.Contatos).FirstOrDefault(w => w.Id == id);

        //    cliente.Contatos = cliente1.Contatos;

        //    return cliente;

        //}

        //public IList<Cliente> GetGeral(string texto)
        //{

        //    var cliente = this.DbSet.Include(x => x.Categoria).Where(w => w.Nome.Contains(texto)).ToList();

        //    return cliente;

        //}

        //public IList<Cliente> GetWithCatagoria_Contatos(int id)
        //{
        //    return this.DbSet.Include(x => x.Categoria).Include(x => x.Contatos).Where(w => w.Id == id).ToList();


        //}
    }
}
