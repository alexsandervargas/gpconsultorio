﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;

namespace Gp.Infrastructure.Repository
{
    public class PlanoRepository : Repository<PlanoModel>
    {
        public PlanoRepository(GpContext context) : base(context)
        {

        }

        public IList<PlanoModel> GetAllInformation()
        {
            var planos = this.DbSet.Include(x => x.PessoaJuridica).ToList();

            return planos;

        }

        public PlanoModel GetWithPessoaJuridicaById(int id)
        {
            var plano = this.DbSet.Include(x => x.PessoaJuridica).FirstOrDefault(w => w.Id == id);

            return plano;
        }

        public IList<PlanoModel> GetAllInformationByPessoaId(int pessoajuridicaId)
        {
            var planos = this.DbSet.Include(x => x.PessoaJuridica).Where(x => x.PessoaJuridica.Id == pessoajuridicaId).ToList();

            return planos;

        }


    }
}
