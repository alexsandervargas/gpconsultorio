﻿using System.Collections.Generic;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using System.Data.Entity;
using System.Linq;
using System;
using System.Linq.Expressions;

namespace Gp.Infrastructure.Repository
{
    public class EstadoRepository :Repository<EstadoModel>
    {
        public EstadoRepository(GpContext context):base(context)
        {

        }

        public IList<EstadoModel> GetAllInformation()
        {
            var estados = this.DbSet.Include(x => x.Pais).ToList();

            return estados;

        }

        public EstadoModel GetWithPais(int id)
        {
            var estado = this.DbSet.Include(x => x.Pais).FirstOrDefault(w => w.Id == id);

            return estado;
        }
    }
}
