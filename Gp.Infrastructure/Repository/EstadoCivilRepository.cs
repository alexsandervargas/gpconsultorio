﻿using System.Collections.Generic;
using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using System.Data.Entity;
using System.Linq;
using System;
using System.Linq.Expressions;

namespace Gp.Infrastructure.Repository
{
    public class EstadoCivilRepository : Repository<EstadoCivilModel>
    {
        public EstadoCivilRepository( GpContext context) :base(context)
        {

        }
    }
}
