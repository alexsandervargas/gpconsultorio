﻿using Gp.Domain.Models;
using Gp.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace Gp.Infrastructure.Repository
{
    public class ProfissionalRepository: Repository<ProfissionalModel>
    {
        public ProfissionalRepository(GpContext context ):base(context)
        {

        }

        public IList<ProfissionalModel> GetAllInformation()
        {
            var profissionais = this.DbSet.Include(x => x.Cidade)
                .Include(x=>x.EstadoCivil)
                .Include(x=>x.Profissao)
                .Include(x=>x.Sexo)
                .Include(x=>x.Conselho).ToList();

            return profissionais;

        }



        public ProfissionalModel GetByIdAllInformation(int id)
        {
            var profissional = this.DbSet.Include(x => x.Cidade)
                .Include(x => x.EstadoCivil)
                .Include(x => x.Profissao)
                .Include(x => x.Sexo)
                .Include(x => x.Conselho).FirstOrDefault(w => w.Id == id);

            return profissional;
        }


    }
}
