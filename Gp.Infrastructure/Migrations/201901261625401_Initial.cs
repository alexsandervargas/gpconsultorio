namespace Gp.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Conselhos",
                c => new
                    {
                        CodConselho = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.CodConselho);
            
            CreateTable(
                "dbo.Pessoas",
                c => new
                    {
                        CodPessoa = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 60, unicode: false),
                        CodigoAreaResidencial = c.Int(),
                        TelefoneResidencial = c.String(maxLength: 10, unicode: false),
                        CodigoAreaCelular = c.Int(),
                        TelefoneCelular = c.String(maxLength: 10, unicode: false),
                        CodigoAreaComercial = c.Int(),
                        TelefoneComercial = c.String(maxLength: 10, unicode: false),
                        Email = c.String(maxLength: 100, unicode: false),
                        Logradouro = c.String(nullable: false, maxLength: 100, unicode: false),
                        Numero = c.String(nullable: false, maxLength: 100, unicode: false),
                        Complemento = c.String(maxLength: 100, unicode: false),
                        Bairro = c.String(nullable: false, maxLength: 100, unicode: false),
                        CEP = c.String(maxLength: 9, unicode: false),
                        Observacao = c.String(maxLength: 1500, unicode: false),
                        CodCidade = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CodPessoa)
                .ForeignKey("dbo.Cidades", t => t.CodCidade)
                .Index(t => t.CodCidade);
            
            CreateTable(
                "dbo.Cidades",
                c => new
                    {
                        CodCidade = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 100, unicode: false),
                        CodEstado = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CodCidade)
                .ForeignKey("dbo.Estados", t => t.CodEstado)
                .Index(t => t.CodEstado);
            
            CreateTable(
                "dbo.Estados",
                c => new
                    {
                        CodEstado = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 100, unicode: false),
                        Sigla = c.String(nullable: false, maxLength: 2, unicode: false),
                        CodPais = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CodEstado)
                .ForeignKey("dbo.Paises", t => t.CodPais)
                .Index(t => t.CodPais);
            
            CreateTable(
                "dbo.Paises",
                c => new
                    {
                        CodPais = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.CodPais);
            
            CreateTable(
                "dbo.EstadosCivis",
                c => new
                    {
                        CodEstadoCivil = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.CodEstadoCivil);
            
            CreateTable(
                "dbo.Indicacoes",
                c => new
                    {
                        CodIndicacao = c.Int(nullable: false, identity: true),
                        NomeIndicacao = c.String(maxLength: 60, unicode: false),
                        CodPessoaFisica = c.Int(),
                    })
                .PrimaryKey(t => t.CodIndicacao)
                .ForeignKey("dbo.PessoasFisicas", t => t.CodPessoaFisica)
                .Index(t => t.CodPessoaFisica);
            
            CreateTable(
                "dbo.Sexos",
                c => new
                    {
                        CodSexo = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.CodSexo);
            
            CreateTable(
                "dbo.PacientesConvenios",
                c => new
                    {
                        CodConvenio = c.Int(nullable: false, identity: true),
                        Matricula = c.String(nullable: false, maxLength: 100, unicode: false),
                        CodPaciente = c.Int(nullable: false),
                        CodPlano = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CodConvenio)
                .ForeignKey("dbo.Pacientes", t => t.CodPaciente)
                .ForeignKey("dbo.Planos", t => t.CodPlano)
                .Index(t => t.CodPaciente)
                .Index(t => t.CodPlano);
            
            CreateTable(
                "dbo.Planos",
                c => new
                    {
                        CodPlano = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 100, unicode: false),
                        Cooparticipacao = c.Boolean(nullable: false),
                        Ativo = c.Boolean(nullable: false),
                        Observacao = c.String(maxLength: 1500, unicode: false),
                        CodEmpresa = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CodPlano)
                .ForeignKey("dbo.PessoasJuridicas", t => t.CodEmpresa)
                .Index(t => t.CodEmpresa);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 100, unicode: false),
                        Login = c.String(nullable: false, maxLength: 100, unicode: false),
                        Senha = c.String(nullable: false, maxLength: 100, unicode: false),
                        Email = c.String(nullable: false, maxLength: 100, unicode: false),
                        Ativo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PessoasFisicas",
                c => new
                    {
                        CodPessoaFisica = c.Int(nullable: false),
                        CodEstadoCivil = c.Int(nullable: false),
                        CodSexo = c.Int(nullable: false),
                        Nascimento = c.DateTime(nullable: false, precision: 0),
                        RG = c.String(nullable: false, maxLength: 20, unicode: false),
                        CPF = c.String(nullable: false, maxLength: 14, unicode: false),
                        Profissao = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.CodPessoaFisica)
                .ForeignKey("dbo.Pessoas", t => t.CodPessoaFisica)
                .ForeignKey("dbo.EstadosCivis", t => t.CodEstadoCivil)
                .ForeignKey("dbo.Sexos", t => t.CodSexo)
                .Index(t => t.CodPessoaFisica)
                .Index(t => t.CodEstadoCivil)
                .Index(t => t.CodSexo);
            
            CreateTable(
                "dbo.Profissionais",
                c => new
                    {
                        CodProfissional = c.Int(nullable: false),
                        CodConselho = c.Int(nullable: false),
                        MatriculaConselho = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.CodProfissional)
                .ForeignKey("dbo.PessoasFisicas", t => t.CodProfissional)
                .ForeignKey("dbo.Conselhos", t => t.CodConselho)
                .Index(t => t.CodProfissional)
                .Index(t => t.CodConselho);
            
            CreateTable(
                "dbo.Pacientes",
                c => new
                    {
                        CodPaciente = c.Int(nullable: false),
                        CodIndicacao = c.Int(),
                    })
                .PrimaryKey(t => t.CodPaciente)
                .ForeignKey("dbo.PessoasFisicas", t => t.CodPaciente)
                .ForeignKey("dbo.Indicacoes", t => t.CodIndicacao)
                .Index(t => t.CodPaciente)
                .Index(t => t.CodIndicacao);
            
            CreateTable(
                "dbo.PessoasJuridicas",
                c => new
                    {
                        CodPessoaJuridica = c.Int(nullable: false),
                        CNPJ = c.String(nullable: false, maxLength: 18, unicode: false),
                        NomeFantasia = c.String(nullable: false, maxLength: 60, unicode: false),
                        InscricaoEstadual = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.CodPessoaJuridica)
                .ForeignKey("dbo.Pessoas", t => t.CodPessoaJuridica)
                .Index(t => t.CodPessoaJuridica);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PessoasJuridicas", "CodPessoaJuridica", "dbo.Pessoas");
            DropForeignKey("dbo.Pacientes", "CodIndicacao", "dbo.Indicacoes");
            DropForeignKey("dbo.Pacientes", "CodPaciente", "dbo.PessoasFisicas");
            DropForeignKey("dbo.Profissionais", "CodConselho", "dbo.Conselhos");
            DropForeignKey("dbo.Profissionais", "CodProfissional", "dbo.PessoasFisicas");
            DropForeignKey("dbo.PessoasFisicas", "CodSexo", "dbo.Sexos");
            DropForeignKey("dbo.PessoasFisicas", "CodEstadoCivil", "dbo.EstadosCivis");
            DropForeignKey("dbo.PessoasFisicas", "CodPessoaFisica", "dbo.Pessoas");
            DropForeignKey("dbo.Pessoas", "CodCidade", "dbo.Cidades");
            DropForeignKey("dbo.Indicacoes", "CodPessoaFisica", "dbo.PessoasFisicas");
            DropForeignKey("dbo.PacientesConvenios", "CodPlano", "dbo.Planos");
            DropForeignKey("dbo.Planos", "CodEmpresa", "dbo.PessoasJuridicas");
            DropForeignKey("dbo.PacientesConvenios", "CodPaciente", "dbo.Pacientes");
            DropForeignKey("dbo.Cidades", "CodEstado", "dbo.Estados");
            DropForeignKey("dbo.Estados", "CodPais", "dbo.Paises");
            DropIndex("dbo.PessoasJuridicas", new[] { "CodPessoaJuridica" });
            DropIndex("dbo.Pacientes", new[] { "CodIndicacao" });
            DropIndex("dbo.Pacientes", new[] { "CodPaciente" });
            DropIndex("dbo.Profissionais", new[] { "CodConselho" });
            DropIndex("dbo.Profissionais", new[] { "CodProfissional" });
            DropIndex("dbo.PessoasFisicas", new[] { "CodSexo" });
            DropIndex("dbo.PessoasFisicas", new[] { "CodEstadoCivil" });
            DropIndex("dbo.PessoasFisicas", new[] { "CodPessoaFisica" });
            DropIndex("dbo.Planos", new[] { "CodEmpresa" });
            DropIndex("dbo.PacientesConvenios", new[] { "CodPlano" });
            DropIndex("dbo.PacientesConvenios", new[] { "CodPaciente" });
            DropIndex("dbo.Indicacoes", new[] { "CodPessoaFisica" });
            DropIndex("dbo.Estados", new[] { "CodPais" });
            DropIndex("dbo.Cidades", new[] { "CodEstado" });
            DropIndex("dbo.Pessoas", new[] { "CodCidade" });
            DropTable("dbo.PessoasJuridicas");
            DropTable("dbo.Pacientes");
            DropTable("dbo.Profissionais");
            DropTable("dbo.PessoasFisicas");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Planos");
            DropTable("dbo.PacientesConvenios");
            DropTable("dbo.Sexos");
            DropTable("dbo.Indicacoes");
            DropTable("dbo.EstadosCivis");
            DropTable("dbo.Paises");
            DropTable("dbo.Estados");
            DropTable("dbo.Cidades");
            DropTable("dbo.Pessoas");
            DropTable("dbo.Conselhos");
        }
    }
}
