namespace Gp.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncludeProfissao : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Profissoes",
                c => new
                    {
                        CodProfissao = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.CodProfissao);
            
            AddColumn("dbo.PessoasFisicas", "CodProfissao", c => c.Int());
            CreateIndex("dbo.PessoasFisicas", "CodProfissao");
            AddForeignKey("dbo.PessoasFisicas", "CodProfissao", "dbo.Profissoes", "CodProfissao");
            DropColumn("dbo.PessoasFisicas", "Profissao");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PessoasFisicas", "Profissao", c => c.String(maxLength: 100, unicode: false));
            DropForeignKey("dbo.PessoasFisicas", "CodProfissao", "dbo.Profissoes");
            DropIndex("dbo.PessoasFisicas", new[] { "CodProfissao" });
            DropColumn("dbo.PessoasFisicas", "CodProfissao");
            DropTable("dbo.Profissoes");
        }
    }
}
