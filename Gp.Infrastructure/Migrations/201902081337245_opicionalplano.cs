namespace Gp.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class opicionalplano : DbMigration
    {
        public override void Up()
        {
            //DropIndex("dbo.Pacientes", new[] { "CodPlano" });
            AlterColumn("dbo.Pacientes", "CodPlano", c => c.Int());
            //CreateIndex("dbo.Pacientes", "CodPlano");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Pacientes", new[] { "CodPlano" });
            AlterColumn("dbo.Pacientes", "CodPlano", c => c.Int(nullable: false));
            CreateIndex("dbo.Pacientes", "CodPlano");
        }
    }
}
