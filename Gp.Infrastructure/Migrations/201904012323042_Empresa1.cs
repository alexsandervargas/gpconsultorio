namespace Gp.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Empresa1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Pacientes", "Empresa", c => c.String(maxLength: 200, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Pacientes", "Empresa", c => c.String(maxLength: 300, unicode: false));
        }
    }
}
