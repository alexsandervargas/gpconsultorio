namespace Gp.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class opicional1 : DbMigration
    {
        public override void Up()
        {
           // DropIndex("dbo.Pessoas", new[] { "CodCidade" });
            AlterColumn("dbo.Pessoas", "CodCidade", c => c.Int());
          //  CreateIndex("dbo.Pessoas", "CodCidade");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Pessoas", new[] { "CodCidade" });
            AlterColumn("dbo.Pessoas", "CodCidade", c => c.Int(nullable: false));
            CreateIndex("dbo.Pessoas", "CodCidade");
        }
    }
}
