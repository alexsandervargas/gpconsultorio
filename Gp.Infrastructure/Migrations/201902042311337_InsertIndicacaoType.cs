namespace Gp.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InsertIndicacaoType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Indicacoes", "Tipo", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Indicacoes", "Tipo");
        }
    }
}
