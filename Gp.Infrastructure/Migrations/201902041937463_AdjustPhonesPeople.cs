namespace Gp.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdjustPhonesPeople : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Pessoas", "TelefoneResidencial", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Pessoas", "TelefoneCelular", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Pessoas", "TelefoneComercial", c => c.String(maxLength: 50, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Pessoas", "TelefoneComercial", c => c.String(maxLength: 10, unicode: false));
            AlterColumn("dbo.Pessoas", "TelefoneCelular", c => c.String(maxLength: 10, unicode: false));
            AlterColumn("dbo.Pessoas", "TelefoneResidencial", c => c.String(maxLength: 10, unicode: false));
        }
    }
}
