namespace Gp.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Opcionais : DbMigration
    {
        public override void Up()
        {
            //DropIndex("dbo.PessoasFisicas", new[] { "CodEstadoCivil" });
           // DropIndex("dbo.PessoasFisicas", new[] { "CodSexo" });
            AlterColumn("dbo.PessoasFisicas", "CodEstadoCivil", c => c.Int());
            AlterColumn("dbo.PessoasFisicas", "CodSexo", c => c.Int());
            AlterColumn("dbo.PessoasFisicas", "RG", c => c.String(maxLength: 20, unicode: false));
            AlterColumn("dbo.PessoasFisicas", "CPF", c => c.String(maxLength: 14, unicode: false));
            AlterColumn("dbo.Pessoas", "Logradouro", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.Pessoas", "Numero", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.Pessoas", "Bairro", c => c.String(maxLength: 100, unicode: false));
          //  CreateIndex("dbo.PessoasFisicas", "CodEstadoCivil");
          //  CreateIndex("dbo.PessoasFisicas", "CodSexo");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PessoasFisicas", new[] { "CodSexo" });
            DropIndex("dbo.PessoasFisicas", new[] { "CodEstadoCivil" });
            AlterColumn("dbo.Pessoas", "Bairro", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("dbo.Pessoas", "Numero", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("dbo.Pessoas", "Logradouro", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("dbo.PessoasFisicas", "CPF", c => c.String(nullable: false, maxLength: 14, unicode: false));
            AlterColumn("dbo.PessoasFisicas", "RG", c => c.String(nullable: false, maxLength: 20, unicode: false));
            AlterColumn("dbo.PessoasFisicas", "CodSexo", c => c.Int(nullable: false));
            AlterColumn("dbo.PessoasFisicas", "CodEstadoCivil", c => c.Int(nullable: false));
            CreateIndex("dbo.PessoasFisicas", "CodSexo");
            CreateIndex("dbo.PessoasFisicas", "CodEstadoCivil");
        }
    }
}
