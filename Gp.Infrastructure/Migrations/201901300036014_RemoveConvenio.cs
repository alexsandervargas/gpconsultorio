namespace Gp.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveConvenio : DbMigration
    {
        public override void Up()
        {
           
            //DropIndex("dbo.PacientesConvenios", new[] { "CodPaciente" });
            //DropIndex("dbo.PacientesConvenios", new[] { "CodPlano" });
            //DropForeignKey("dbo.PacientesConvenios", "CodPaciente", "dbo.Pacientes");
            //DropForeignKey("dbo.PacientesConvenios", "CodPlano", "dbo.Planos");
            AddColumn("dbo.Pacientes", "CodPlano", c => c.Int(nullable: false));
            AddColumn("dbo.Pacientes", "Matricula", c => c.String(maxLength: 100, unicode: false));
            CreateIndex("dbo.Pacientes", "CodPlano");
            AddForeignKey("dbo.Pacientes", "CodPlano", "dbo.Planos", "CodPlano");
            //DropTable("dbo.PacientesConvenios");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PacientesConvenios",
                c => new
                    {
                        CodConvenio = c.Int(nullable: false, identity: true),
                        Matricula = c.String(nullable: false, maxLength: 100, unicode: false),
                        DataCadastro = c.DateTime(nullable: false, precision: 0),
                        DataAtualizacao = c.DateTime(nullable: false, precision: 0),
                        CodPaciente = c.Int(nullable: false),
                        CodPlano = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CodConvenio);
            
            DropForeignKey("dbo.Pacientes", "CodPlano", "dbo.Planos");
            DropIndex("dbo.Pacientes", new[] { "CodPlano" });
            DropColumn("dbo.Pacientes", "Matricula");
            DropColumn("dbo.Pacientes", "CodPlano");
            CreateIndex("dbo.PacientesConvenios", "CodPlano");
            CreateIndex("dbo.PacientesConvenios", "CodPaciente");
            AddForeignKey("dbo.PacientesConvenios", "CodPlano", "dbo.Planos", "CodPlano");
            AddForeignKey("dbo.PacientesConvenios", "CodPaciente", "dbo.Pacientes", "CodPaciente");
        }
    }
}
