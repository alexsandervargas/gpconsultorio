namespace Gp.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InsertFichaPaciente : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pacientes", "Ficha", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pacientes", "Ficha");
        }
    }
}
