namespace Gp.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InsertDataCadastro : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Conselhos", "DataCadastro", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Conselhos", "DataAtualizacao", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Pessoas", "DataCadastro", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Pessoas", "DataAtualizacao", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Cidades", "DataCadastro", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Cidades", "DataAtualizacao", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Estados", "DataCadastro", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Estados", "DataAtualizacao", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Paises", "DataCadastro", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Paises", "DataAtualizacao", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.EstadosCivis", "DataCadastro", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.EstadosCivis", "DataAtualizacao", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Indicacoes", "DataCadastro", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Indicacoes", "DataAtualizacao", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Profissoes", "DataCadastro", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Profissoes", "DataAtualizacao", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Sexos", "DataCadastro", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Sexos", "DataAtualizacao", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.PacientesConvenios", "DataCadastro", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.PacientesConvenios", "DataAtualizacao", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Planos", "DataCadastro", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Planos", "DataAtualizacao", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Usuarios", "DataCadastro", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Usuarios", "DataAtualizacao", c => c.DateTime(nullable: false, precision: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Usuarios", "DataAtualizacao");
            DropColumn("dbo.Usuarios", "DataCadastro");
            DropColumn("dbo.Planos", "DataAtualizacao");
            DropColumn("dbo.Planos", "DataCadastro");
            DropColumn("dbo.PacientesConvenios", "DataAtualizacao");
            DropColumn("dbo.PacientesConvenios", "DataCadastro");
            DropColumn("dbo.Sexos", "DataAtualizacao");
            DropColumn("dbo.Sexos", "DataCadastro");
            DropColumn("dbo.Profissoes", "DataAtualizacao");
            DropColumn("dbo.Profissoes", "DataCadastro");
            DropColumn("dbo.Indicacoes", "DataAtualizacao");
            DropColumn("dbo.Indicacoes", "DataCadastro");
            DropColumn("dbo.EstadosCivis", "DataAtualizacao");
            DropColumn("dbo.EstadosCivis", "DataCadastro");
            DropColumn("dbo.Paises", "DataAtualizacao");
            DropColumn("dbo.Paises", "DataCadastro");
            DropColumn("dbo.Estados", "DataAtualizacao");
            DropColumn("dbo.Estados", "DataCadastro");
            DropColumn("dbo.Cidades", "DataAtualizacao");
            DropColumn("dbo.Cidades", "DataCadastro");
            DropColumn("dbo.Pessoas", "DataAtualizacao");
            DropColumn("dbo.Pessoas", "DataCadastro");
            DropColumn("dbo.Conselhos", "DataAtualizacao");
            DropColumn("dbo.Conselhos", "DataCadastro");
        }
    }
}
