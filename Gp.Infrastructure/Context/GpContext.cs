﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Gp.Infrastructure.Mapping;
using System.Configuration;
using System.Data.SqlClient;
using MySql.Data;
using System.Linq;
using System;

namespace Gp.Infrastructure.Context
{

    public class GpContext : DbContext
    {

        public GpContext() : base("name=Dbase1")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties().Where(p => p.Name == p.ReflectedType.Name + "Id")
                                     .Configure(p => p.IsKey());

            modelBuilder.Properties<string>()
                        .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                        .Configure(p => p.HasMaxLength(100));


            //Builders   
            modelBuilder.Configurations.Add(new ConselhoMapping());
            modelBuilder.Configurations.Add(new ProfissionalMapping());
            modelBuilder.Configurations.Add(new PaisMapping());
            modelBuilder.Configurations.Add(new EstadoMapping());
            modelBuilder.Configurations.Add(new CidadeMapping());
            modelBuilder.Configurations.Add(new PessoaMapping());
            modelBuilder.Configurations.Add(new PessoaFisicaMapping());
            modelBuilder.Configurations.Add(new ProfissaoMapping());
            modelBuilder.Configurations.Add(new IndicacaoMapping());
            modelBuilder.Configurations.Add(new PacienteMapping());
            modelBuilder.Configurations.Add(new EstadoCivilMapping());
            modelBuilder.Configurations.Add(new SexoMapping());
            modelBuilder.Configurations.Add(new PessoaJuridicaMapping());
            modelBuilder.Configurations.Add(new PlanoMapping());
            modelBuilder.Configurations.Add(new UsuarioMapping());




        }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("DataCadastro") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("DataCadastro").CurrentValue = DateTime.Now;
                }
                if (entry.State == EntityState.Modified)
                {
                    entry.Property("DataCadastro").IsModified = false;
                }
            }

            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("DataAtualizacao") != null))
            {

                entry.Property("DataAtualizacao").CurrentValue = DateTime.Now;

            }

            return base.SaveChanges();
        }


    }


}
